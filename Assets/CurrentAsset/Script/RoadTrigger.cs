﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadTrigger : MonoBehaviour
{
    [SerializeField] Transform leftEndTrans;
    [SerializeField] Transform rightEndTrans;
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.GetComponent<Character>().Rotate(transform);
            GameManager.Instance.ChoosedDirection();
            GameManager.Instance.SpawnRoad(leftEndTrans, rightEndTrans);
        }
    }
}
