﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    [SerializeField] Animator anim;
    public float TurnRotation = -90f;
    private void OnCollisionExit(Collision collision)
    {
        if (collision.transform.CompareTag("Ground"))
        {
            anim.SetTrigger("IsFalling");
        }
        
    }

    public void Restart()
    {
        anim.SetBool("IsRunning", true);
    }
    
    
    public void Rotate(Transform triggerTrans)
    {
        
        transform.position = triggerTrans.position;
        transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y + TurnRotation, transform.eulerAngles.z);
    }
}
