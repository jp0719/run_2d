﻿using PhatRobit;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : Singleton<GameManager>
{
    [SerializeField] GameObject player;
    [SerializeField] SimpleRpgCamera simpleRPGCamera;
    Character playerCharacter;
    [SerializeField] GameObject buttonMoveLeft;
    [SerializeField] GameObject buttonMoveRight;
    [Header("Road Setting")]
    [SerializeField] GameObject startMainRoad;
    [SerializeField] GameObject mainRoadPrefab;
    [SerializeField] List<GameObject> previousRoads;
    [Header("GameOver")]
    [SerializeField] SlidingInOut gameOverSlider;
    [Header("UI")]
    [SerializeField] List<Text> CoinText;
    public GameObject Player { get => player; set => player = value; }
    public int WinDirection { get => winDirection; private set => winDirection = value; }
    public int TotalCoin {
        get
        {
            return PlayerPrefs.GetInt("Coin");
        } set
        {
            
            int V = PlayerPrefs.GetInt("Coin") + value;
            PlayerPrefs.SetInt("Coin", V );
            totalCoin = V;
            foreach(Text t in CoinText)
            {
                t.text = "Coin: " + V;
            }
        }
    }

    private void Start()
    {
       
        playerCharacter = player.GetComponent<Character>();
        simpleRPGCamera.target = Player.transform;
        buttonMoveLeft.GetComponent<Button>().onClick.AddListener(delegate { playerCharacter.TurnRotation = -90; });
        buttonMoveRight.GetComponent<Button>().onClick.AddListener(delegate { playerCharacter.TurnRotation = 90; });
    }

    public void Restart()
    {
        foreach (Text t in CoinText)
        {
            t.text = "Coin: " + TotalCoin;
        }
        startMainRoad.SetActive(true);
        player.transform.position = new Vector3(0, 0, 0);
        player.transform.eulerAngles = new Vector3(0, 0, 0);
        playerCharacter.Restart();
        foreach(GameObject g in previousRoads)
        {
            if(g == startMainRoad)
            {

            }
            else
            {
                Destroy(g);
            }
            

        }
        previousRoads.Clear();
        if (!previousRoads.Contains(startMainRoad))
        {
            previousRoads.Add(startMainRoad);
        }
        
    }

    
    int totalCoin;

    public void AddCoin(int coinCount)
    {
        TotalCoin = coinCount;
    }

    public void GameOver()
    {
        gameOverSlider.OnClickSlider();
    }

    // win direction 0 mean left is win, 1 mean right is win
    int winDirection;

    public void GetDirection()
    {
        WinDirection = Random.Range(0, 2);
        buttonMoveLeft.SetActive(true);
        buttonMoveRight.SetActive(true);

    }

    public void SpawnRoad(Transform leftEndTrans, Transform rightEndTrans)
    {
        
        if(previousRoads.Count > 1)
        {
            for(int i = 0; i< (previousRoads.Count - 1); i++)
            {
                previousRoads[i].SetActive(false);
            }
        }
        Transform pickedRoad;
        float rotationPicked;
        if(WinDirection == 0)
        {
            pickedRoad = leftEndTrans;
            rotationPicked = -90f;
        }
        else
        {
            pickedRoad = rightEndTrans;
            rotationPicked = 90f;
        }
        GameObject road = (GameObject)Instantiate(mainRoadPrefab);

        road.transform.position = pickedRoad.transform.position;
        road.transform.eulerAngles = new Vector3(pickedRoad.transform.eulerAngles.x, pickedRoad.transform.eulerAngles.y + rotationPicked, pickedRoad.transform.eulerAngles.z);
        previousRoads.Add(road);
    }

    public void ChoosedDirection()
    {
        
        buttonMoveLeft.SetActive(false);
        buttonMoveRight.SetActive(false);
    }
}
