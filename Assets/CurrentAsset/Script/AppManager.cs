﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppManager : Singleton<AppManager> {

	[Range(5f, 50f)]
	public float slidingInOutSpeed = 20f;
	private List<SlidingInOut> slidingInOutList = new List<SlidingInOut>();
	private List<SlidingInOut> currentActiveScreenList = new List<SlidingInOut> ();

	private bool isStartSavingActiveScreen = false;
	private SlidingInOut startSavingActiveScreenSlidingInOut;
    [SerializeField] RectTransform canvasRect;
    public float screenHeight;
    public float screenWidth;

    protected override void Awake()
    {
        base.Awake();

        screenHeight = canvasRect.sizeDelta.y;

        screenWidth = canvasRect.sizeDelta.x;
    }

    public void SubscribeSlidingInOut (SlidingInOut slidingInOut){
		if (!slidingInOutList.Contains (slidingInOut)) {
			slidingInOutList.Add (slidingInOut);
		}
	}

	public void UnSubscribeSlidingInOut (SlidingInOut slidingInOut){
		if (slidingInOutList.Contains (slidingInOut)) {
			slidingInOutList.Remove (slidingInOut);
		}
	}

	public void SubscribeCurrentActiveScreenList (SlidingInOut slidingInOut){
		if (!currentActiveScreenList.Contains (slidingInOut)) {
			if (isStartSavingActiveScreen) {
				startSavingActiveScreenSlidingInOut = slidingInOut;
			}
			currentActiveScreenList.Add (slidingInOut);
		}
	}

	public void UnSubscribeCurrentActiveScreenList (SlidingInOut slidingInOut){
		if (currentActiveScreenList.Contains (slidingInOut)) {
			if (isStartSavingActiveScreen) {
				if (slidingInOut == startSavingActiveScreenSlidingInOut) {
					CloseAllSavingActiveScreen ();
					startSavingActiveScreenSlidingInOut = null;
					isStartSavingActiveScreen = false;
				}
			}
			currentActiveScreenList.Remove (slidingInOut);
		}
	}



	public void StartSavingActiveScreen(){
		if (isStartSavingActiveScreen) {
			CloseAllSavingActiveScreen ();
		}
		isStartSavingActiveScreen = true;
	}

	public void CloseAllSavingActiveScreen(){
		bool startClosingScreen = false;
		for (int i = 0; i < currentActiveScreenList.Count; i++) {
			if (startClosingScreen) {
				currentActiveScreenList [i].OnClickSlider ();
			}
			if (currentActiveScreenList [i] == startSavingActiveScreenSlidingInOut) {
				startClosingScreen = true;
			}


		}
	}

	void Update(){
		if (Input.GetKeyDown (KeyCode.Escape)) {
			if (currentActiveScreenList.Count > 0) {
				currentActiveScreenList [currentActiveScreenList.Count - 1].OnClickSlider ();
            }
            else
            {
                Application.Quit();
            }
			
		}
			
	}


}
