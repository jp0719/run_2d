﻿using LitJson;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class ServerManager : Singleton<ServerManager>
{
    [Header("Save File Setting")]
    [SerializeField] private string generalFolderName = "/Server Data Run";
    [SerializeField] private string serverResultPath = "/ServerResult/ServerResult.json";
    [SerializeField] private string playerProfilePath = "/PlayerProfile/PlayerProfile.json";
    [SerializeField] private string ticketsPath = "/Tickets/Tickets.json";
    [SerializeField] private string saveFavoritePath = "/SaveFavorite/SaveFavorite.json";
    [Header("Server setting")]
    [SerializeField] private List<ServerResult> storeServerResult = new List<ServerResult>();
    [SerializeField] private bool showDebugResult = true;
    [SerializeField] private long currentDateTick;
    [SerializeField] private int currentRoundPickId;
    int lastResult;

    [Header("Number Draw setting")]
    [SerializeField] private int numberOfPickPerDay = 960;
    [SerializeField] private int minPickNumber = 0;
    [SerializeField] private int maxPickNumber = 9;
    private int secondPerPick;

    [Header("Betting setting")]
    [SerializeField] List<WinningPayoff> winningSetting = new List<WinningPayoff>();
    

    [Header("Player Setting")]
    [SerializeField] private string playerUsername = "Guest";
    [SerializeField] private string playerPassword = "Guest";
    [SerializeField] private int playerCoin = 10000;
    [SerializeField] List<PlayerProfile> players = new List<PlayerProfile>();
    [SerializeField] List<PlayerBetTickets> allTickets = new List<PlayerBetTickets>();
    [SerializeField] List<SaveFavorite> allSavedFavorites = new List<SaveFavorite>();
    private void Start()
    {
        Init();
    }
    bool hasFinishInitServer;
    bool hasInit;

    public bool HasFinishInitServer { get => hasFinishInitServer; set => hasFinishInitServer = value; }
    public long CurrentDateTick { get => currentDateTick; set => currentDateTick = value; }
    public int CurrentRoundPickId { get => currentRoundPickId; set => currentRoundPickId = value; }
    public int SecondPerPick { get => secondPerPick; set => secondPerPick = value; }
    public int LastResult { get => lastResult; private set => lastResult = value; }
    public int NumberOfPickPerDay { get => numberOfPickPerDay; private set => numberOfPickPerDay = value; }
    public List<WinningPayoff> WinningSetting { get => winningSetting; set => winningSetting = value; }
    public List<ServerResult> StoreServerResult { get => storeServerResult; set => storeServerResult = value; }

    private void Init()
    {
        StoreServerResult = LoadServerResult();
        SecondPerPick = (3600 * 24) / NumberOfPickPerDay;

        if (StoreServerResult.Count == 0)
        {

            DateTime t = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            t = t.AddDays(-1);
            int counter = 1;
            bool startAdding = false;
            
            for (int i = 1 ; i<= NumberOfPickPerDay; i++)
            {
                
                t = t.AddSeconds(SecondPerPick);
                
                if (t.Hour == 0 || startAdding)
                {
                    startAdding = true;
                    CurrentDateTick = t.Ticks;
                    CurrentRoundPickId = counter;
                    LastResult = UnityEngine.Random.Range(minPickNumber, maxPickNumber + 1);
                    StoreServerResult.Add(
                        new ServerResult(counter, t.Ticks, counter, LastResult));
                    counter++;
                }

            }

        }
        else
        {
            ServerResult last = StoreServerResult[StoreServerResult.Count - 1];
            CurrentDateTick = last.DatePick;
            CurrentRoundPickId = last.PickRoundId;
            LastResult = last.Result;
        }
        HasFinishInitServer = false;
        

        players = LoadPlayer();

        if(players.Count == 0)
        {
            players.Add(new PlayerProfile(0,playerCoin, playerUsername, playerPassword));
            SavePlayer(players.ToArray());
        }

        allTickets = LoadTickets();
        allSavedFavorites = LoadSavedFavorites();
        StartCoroutine(GenerateResult());
        hasInit = true;
    }

    public PlayerProfile Login(string username, string password)
    {
        foreach(PlayerProfile p in players)
        {
            if(p.PlayerUsername == username && p.PlayerPassword == password)
            {
                
                return p;
            }
        }

        return null;
    }
    IEnumerator GenerateResult()
    {
        bool hasUpdate = false;
        while(new DateTime( StoreServerResult[StoreServerResult.Count -1].DatePick).AddSeconds(SecondPerPick).Ticks < DateTime.Now.Ticks)
        {
            hasUpdate = true;
            int counter = StoreServerResult[StoreServerResult.Count - 1].Id + 1;
            DateTime t = new DateTime(StoreServerResult[StoreServerResult.Count - 1].DatePick).AddSeconds(SecondPerPick);
            int pickCounter = StoreServerResult[StoreServerResult.Count - 1].PickRoundId + 1;
            int result = UnityEngine.Random.Range(minPickNumber, maxPickNumber + 1);

            if (t.Hour == 0 && pickCounter >= NumberOfPickPerDay -1)
            {
                pickCounter = 0;
            }

            CurrentDateTick = t.Ticks;
            CurrentRoundPickId = pickCounter;
            LastResult = result;
            StoreServerResult.Add(
                        new ServerResult(counter, t.Ticks, pickCounter, result));

            if (showDebugResult) Debug.Log(counter + ") "+ new DateTime(t.Ticks) +" round ("+ pickCounter+") Result >>> "+ result);
            
            
        }
        if (hasUpdate)
        {
            SaveServerResult(StoreServerResult.ToArray());

            CheckWin(0);
        }
            

        HasFinishInitServer = true;

        yield return null;
    }

    private void Update()
    {
        if (!hasInit) return;

        StartCoroutine(GenerateResult());
    }

    public List<PlayerBetTickets> GetTickets(int playerId, int numberOfTicket, int startIndex)
    {

        

        if ((allTickets.Count - startIndex) < numberOfTicket)
        {
            numberOfTicket = allTickets.Count - startIndex;
        }
        return allTickets.GetRange(startIndex, numberOfTicket);

        
        
    }

    public void CheckWin(int playerId)
    {
        foreach(PlayerBetTickets t in allTickets)
        {
            if(t.PlayerId == playerId && t.Status == TicketStatus.Pending)
            {
                bool hasWin = true;
                int counterResult = 0;
                foreach (Bet b in t.BetGroups)
                {
                    foreach (ServerResult s in StoreServerResult)
                    {
                        if (new DateTime(b.DateTick).ToString("yyyyMMdd") == new DateTime(s.DatePick).ToString("yyyyMMdd") && b.PickRoundId == s.PickRoundId)
                        {
                            
                            b.SetResult(s.Result);
                            counterResult++;
                            if (hasWin)
                            {
                                switch (b.TypeOfBetting)
                                {
                                    case (TypeOfBet.Number):
                                        if (b.PickNumber != s.Result)
                                        {
                                            hasWin = false;
                                        }

                                        break;
                                    case (TypeOfBet.B):
                                        if(s.Result < 5)
                                        {
                                            hasWin = false;
                                        }
                                        break;
                                    case (TypeOfBet.S):
                                        if (s.Result >= 5)
                                        {
                                            hasWin = false;
                                        }
                                        break;
                                    case (TypeOfBet.OD):
                                        if (s.Result % 2 == 0)
                                        {
                                            hasWin = false;
                                        }
                                        
                                        break;
                                    case (TypeOfBet.EV):
                                        if (s.Result % 2 != 0)
                                        {
                                            hasWin = false;
                                        }
                                        
                                        break;
                                }
                            }
                            

                            break;

                        }
                    }
                }

                
                
                if (counterResult != t.BetGroups.Count)
                {
                    t.Status = TicketStatus.Pending;
                }
                else if (!hasWin)
                {
                    t.Status = TicketStatus.Lose;
                }
                else
                {
                    t.Status = TicketStatus.Win;
                }

                
            }
        }
        SaveTickets(allTickets.ToArray());
    }

    public PlayerProfile ClaimWin(int playerId, int ticketId)
    {
        foreach (PlayerProfile p in players)
        {
            if (p.PlayerId == playerId)
            {
                foreach(PlayerBetTickets t in allTickets)
                {
                    if(t.TicketId == ticketId && t.Status == TicketStatus.Win && t.PlayerId == playerId &&t.HasClaimed == false)
                    {
                        p.PlayerCoin +=  (float)Math.Round( t.BetAmount * t.WinningPercentage,2);
                        t.HasClaimed = true;
                        SavePlayer(players.ToArray());
                        SaveTickets(allTickets.ToArray());
                        return p;
                    }
                }
            }
        }

       

        return null;
    }

    public float GetPlayerCoin(int playerId)
    {
        foreach(PlayerProfile p in players)
        {
            if(p.PlayerId == playerId)
            {
                return (float)p.PlayerCoin;
            }
        }

        return 0;
    }

    public List<PlayerBetTickets> BuyTicket(int playerId, List<Betting> bets, int amount, int repeat = 1)
    {
        List<PlayerBetTickets> ticket = new List<PlayerBetTickets>();

        for (int i =0; i< players.Count; i++)
        {
            
            if(players[i].PlayerId == playerId)
            {
                for(int k = 0; k<= repeat - 1; k++)
                {
                    float winningPercent = 1f;
                    if (players[i].PlayerCoin < amount)
                    {
                        Debug.Log("Not Enough money");
                        return null;
                    }
                    else
                    {
                        players[i].Pay(amount);
                    }
                    List<Bet> tempBetGroup = new List<Bet>();
                    int counter = k;
                    int buyRoundId = k + CurrentRoundPickId;
                    foreach (Betting b in bets)
                    {
                        buyRoundId++;
                        if (buyRoundId > NumberOfPickPerDay)
                        {
                            buyRoundId = buyRoundId - NumberOfPickPerDay;
                        }
                        foreach (WinningPayoff pay in WinningSetting)
                        {
                            if (pay.typeOf == b.TypeBet)
                            {
                                winningPercent = winningPercent * pay.percentage;

                            }
                        }

                        tempBetGroup.Add(new Bet(new DateTime(CurrentDateTick).AddSeconds(counter * SecondPerPick).Ticks, buyRoundId, b.TypeBet, b.Number));
                        counter++;
                    }
                    PlayerBetTickets t = new PlayerBetTickets(players[i].PlayerId, allTickets.Count, amount, CurrentDateTick, Math.Round(winningPercent, 2), tempBetGroup);
                    ticket.Add(t);
                    allTickets.Insert(0, t);
                }
                
                break;
            }
        }
        
        SavePlayer(players.ToArray());
        
        SaveTickets(allTickets.ToArray());
        return ticket;


    }

    public List<SaveFavorite> LoadAllSavedFavorite(int playerId)
    {
        allSavedFavorites = LoadSavedFavorites();
        List<SaveFavorite> temp = new List<SaveFavorite>();
        foreach(SaveFavorite f in allSavedFavorites)
        {
            if(f.PlayerId == playerId)
            {
                temp.Add(f);
            }
        }

        return temp;
    }

    public void RemoveSavedFavorite(int saveFavoriteId)
    {
        SaveFavorite tempS = null;
        foreach(SaveFavorite s in allSavedFavorites)
        {
            if(s.SaveFavoriteId == saveFavoriteId)
            {
                tempS = s;
                break;
            }
        }

        if(tempS != null)
        {
            allSavedFavorites.Remove(tempS);
            SaveSavedFavorites(allSavedFavorites.ToArray());
        }
           
    }

    public void SaveSavedFavorite(int playerId, List<Betting> bettingResult)
    {
        bool hasSame = false;
        foreach(SaveFavorite s in allSavedFavorites)
        {
            if(s.PlayerId == playerId)
            {
                int counter = 0;
                if (bettingResult.Count == s.SavedBettings.Count)
                {
                    for (int i = 0; i < s.SavedBettings.Count; i++)
                    {

                        if (s.SavedBettings[i].Number == bettingResult[i].Number && s.SavedBettings[i].TypeBet == bettingResult[i].TypeBet)
                        {

                            counter++;
                        }


                    }
                    if(bettingResult.Count == counter)
                    {
                        
                        hasSame = true;
                    }
                   
                    

                    
                   
                }

            }
        }
        if (allSavedFavorites.Count > 0)
        {
            if (!hasSame)
            {
                int i = allSavedFavorites[allSavedFavorites.Count - 1].SaveFavoriteId + 1;
                allSavedFavorites.Add(new SaveFavorite(playerId, i, DateTime.Now.Ticks, bettingResult));
            }
            
        }
        else
        {
            allSavedFavorites.Add(new SaveFavorite(playerId, 0, DateTime.Now.Ticks, bettingResult));
        }

        SaveSavedFavorites(allSavedFavorites.ToArray());
    }


    public List<ServerResult> LoadServerResult()
    {

        return DeserializeServerResultFromString(LoadFile(generalFolderName + serverResultPath));
    }

    private void SaveServerResult(ServerResult[] result)
    {
        
        SaveFile(generalFolderName + serverResultPath, result);

    }

    public List<ServerResult> DeserializeServerResultFromString(string jsonString)
    {
        JsonData jsonData = JsonMapper.ToObject(jsonString);

        List<ServerResult> tempList = new List<ServerResult>();
        for (int i = 0; i < jsonData.Count; i++)
        {


            tempList.Add(new ServerResult((int)jsonData[i]["Id"], (long)jsonData[i]["DatePick"], (int)jsonData[i]["PickRoundId"], (int)jsonData[i]["Result"]));
        }

        return tempList.OrderBy(x => x.Id).ToList();

    }

    public List<PlayerProfile> LoadPlayer()
    {

        return DeserializePlayerFromString(LoadFile(generalFolderName + playerProfilePath));
    }

    private void SavePlayer(PlayerProfile[] result)
    {
        
        SaveFile(generalFolderName + playerProfilePath, result);

    }

    public List<PlayerProfile> DeserializePlayerFromString(string jsonString)
    {
        JsonData jsonData = JsonMapper.ToObject(jsonString);

        List<PlayerProfile> tempList = new List<PlayerProfile>();
        for (int i = 0; i < jsonData.Count; i++)
        {
            

            tempList.Add(new PlayerProfile((int)jsonData[i]["PlayerId"], (double)jsonData[i]["PlayerCoin"], jsonData[i]["PlayerUsername"].ToString(), jsonData[i]["PlayerPassword"].ToString()));
        }

        return tempList;

    }

    public List<PlayerBetTickets> LoadTickets()
    {

        return DeserializeTicketsFromString(LoadFile(generalFolderName + ticketsPath));
    }

    private void SaveTickets(PlayerBetTickets[] result)
    {

        SaveFile(generalFolderName + ticketsPath, result);

    }

    public List<PlayerBetTickets> DeserializeTicketsFromString(string jsonString)
    {
        JsonData jsonData = JsonMapper.ToObject(jsonString);

        List<PlayerBetTickets> tickets = new List<PlayerBetTickets>();
        foreach (JsonData ticket in jsonData)
        {


            TicketStatus status = TicketStatus.Pending;
            switch (ticket["Status"].ToString())
            {
                case "0":
                    status = TicketStatus.Win;
                    break;
                case "1":
                    status = TicketStatus.Lose;
                    break;


            }

            List<Bet> bets = new List<Bet>();
            foreach (JsonData bet in ticket["BetGroups"])
            {
                TypeOfBet typeBet = TypeOfBet.EV;
                switch (bet["TypeOfBetting"].ToString())
                {
                    case "0":
                        typeBet = TypeOfBet.Number;
                        break;
                    case "1":
                        typeBet = TypeOfBet.B;
                        break;
                    case "2":
                        typeBet = TypeOfBet.S;
                        break;
                    case "3":
                        typeBet = TypeOfBet.OD;
                        break;


                }
                bets.Add(new Bet((long)bet["DateTick"], (int)bet["PickRoundId"], typeBet, (int)bet["PickNumber"], (int)bet["Result"]));
            }

            tickets.Add(new PlayerBetTickets ((int)ticket["PlayerId"],(int)ticket["TicketId"], (int)ticket["BetAmount"], (long)ticket["DateTick"], (double)ticket["WinningPercentage"],
                bets, status, (bool)ticket["HasClaimed"]));
        }

        return tickets;

    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    public List<SaveFavorite> LoadSavedFavorites()
    {

        return DeserializeSavedFavoriteFromString(LoadFile(generalFolderName + saveFavoritePath));
    }

    private void SaveSavedFavorites(SaveFavorite[] result)
    {

        SaveFile(generalFolderName + saveFavoritePath, result);

    }

    public List<SaveFavorite> DeserializeSavedFavoriteFromString(string jsonString)
    {
        JsonData jsonData = JsonMapper.ToObject(jsonString);

        List<SaveFavorite> tempSavedFavorites = new List<SaveFavorite>();
        foreach (JsonData f in jsonData)
        {
            List<Betting> tempBettings = new List<Betting>();
            foreach(JsonData b in f["SavedBettings"])
            {
                TypeOfBet typeBet = TypeOfBet.EV;
                switch (b["TypeBet"].ToString())
                {
                    case "0":
                        typeBet = TypeOfBet.Number;
                        break;
                    case "1":
                        typeBet = TypeOfBet.B;
                        break;
                    case "2":
                        typeBet = TypeOfBet.S;
                        break;
                    case "3":
                        typeBet = TypeOfBet.OD;
                        break;


                }
                tempBettings.Add(new Betting(typeBet, (int)b["Number"]));
            }

            tempSavedFavorites.Add(new SaveFavorite((int)f["PlayerId"],(int)f["SaveFavoriteId"], (long)f["DateTick"], tempBettings));
        }

        return tempSavedFavorites;

    }

    public string LoadFile(string path)
    {

        FileInfo file = CheckAndCreateFile(path);
        string content = "[]";
        try
        {
            content = File.ReadAllText(file.FullName);
        }
        catch (Exception e)
        {
            //Debug.LogException(e, this);
        }
        return content;
    }

    void SaveFile(string path, object[] content)
    {
      
        FileInfo file = CheckAndCreateFile(path);
        
        JsonData serializeJson = JsonMapper.ToJson(content);
        
        File.WriteAllText(file.FullName, serializeJson.ToString());
    }

    FileInfo CheckAndCreateFile(string path)
    {
#if (!UNITY_ANDROID && !UNITY_IPHONE || UNITY_EDITOR)
		FileInfo file = new FileInfo(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)+ generalFolderName + path);
		
#else
        FileInfo file = new FileInfo(Application.persistentDataPath + generalFolderName + path);
        
#endif
        file.Directory.Create();
        return file;
    }
}



[Serializable]
public class ServerResult
{
    [SerializeField]
    private int id;
    [SerializeField]
    private long datePick;
    [SerializeField]
    private int pickRoundId;
    [SerializeField]
    private int result;

    public int Id { get => id; set => id = value; }
    public long DatePick { get => datePick; set => datePick = value; }
    public int Result { get => result; set => result = value; }
    public int PickRoundId { get => pickRoundId; set => pickRoundId = value; }

    public ServerResult(int id, long timePick, int pickRoundId, int result)
    {
        Id = id;
        DatePick = timePick;
        Result = result;
        PickRoundId = pickRoundId;
    }
}
[Serializable]
public class PlayerProfile
{
    [SerializeField]
    int playerId;
    [SerializeField]
    double playerCoin;
    [SerializeField]
    string playerUsername;

    [SerializeField]
    string playerPassword;
    [SerializeField]
    List<PlayerBetTickets> tickets = new List<PlayerBetTickets>();


    public int PlayerId { get => playerId; private set => playerId = value; }
    public double PlayerCoin { get => playerCoin; set => playerCoin = value; }
    public string PlayerUsername { get => playerUsername; private set => playerUsername = value; }
    public string PlayerPassword { get => playerPassword; private set => playerPassword = value; }



    public PlayerProfile(int playerId, double playerCoin, string playerUsername, string playerPassword)
    {
        PlayerId = playerId;
        PlayerCoin = playerCoin;
        PlayerUsername = playerUsername;
        PlayerPassword = playerPassword;
    }







    public void Pay(int amount)
    {
        PlayerCoin -= amount;
    }

}
[Serializable]
public class PlayerBetTickets
{
    [SerializeField]
    int playerId;
    [SerializeField]
    int ticketId;
    [SerializeField]
    int betAmount;
    [SerializeField]
    long dateTick;
    [SerializeField]
    List<Bet> betGroups = new List<Bet>();
    [SerializeField]
    TicketStatus status = TicketStatus.Pending;
    [SerializeField]
    double winningPercentage;
    [SerializeField]
    bool hasClaimed = false;

    public int PlayerId { get => playerId; set => playerId = value; }
    public int TicketId { get => ticketId; set => ticketId = value; }
    public int BetAmount { get => betAmount; set => betAmount = value; }
    public long DateTick { get => dateTick; set => dateTick = value; }
    public List<Bet> BetGroups { get => betGroups; set => betGroups = value; }
    public TicketStatus Status { get => status; set => status = value; }
    public bool HasClaimed { get => hasClaimed; set => hasClaimed = value; }
    public double WinningPercentage { get => winningPercentage; set => winningPercentage = value; }



    public PlayerBetTickets(int playerId, int ticketId, int amount, long dateTick, double winningPercentage, List<Bet> betGroup)
    {
        PlayerId = playerId;
        TicketId = ticketId;
        BetAmount = amount;
        DateTick = dateTick;
        WinningPercentage = winningPercentage;
        BetGroups = betGroup;
    }

    public PlayerBetTickets(int playerId, int ticketId, int amount, long dateTick, double winningPercentage, List<Bet> betGroup, TicketStatus status, bool hasClaimed)
    {
        PlayerId = playerId;
        TicketId = ticketId;
        BetAmount = amount;
        DateTick = dateTick;
        WinningPercentage = winningPercentage;
        BetGroups = betGroup;
        Status = status;
        HasClaimed = hasClaimed;
    }

    public int ClaimWin()
    {
        if (status == TicketStatus.Win && !hasClaimed)
        {
            return (int)(BetAmount * WinningPercentage);
        }
        else
        {
            return 0;
        }
    }
}
[Serializable]
public class Bet
{
    [SerializeField]
    long dateTick;
    [SerializeField]
    int pickRoundId;
    [SerializeField]
    TypeOfBet typeOfBetting;
    [SerializeField]
    int pickNumber;
    [SerializeField]
    int result = -1;


    public long DateTick { get => dateTick; private set => dateTick = value; }
    public int PickRoundId { get => pickRoundId; private set => pickRoundId = value; }
    public TypeOfBet TypeOfBetting { get => typeOfBetting; private set => typeOfBetting = value; }
    public int PickNumber { get => pickNumber; private set => pickNumber = value; }
    public int Result { get => result; private set => result = value; }

    public Bet(long dateTick, int pickRoundId, TypeOfBet typeBet, int pickNumber)
    {
        DateTick = dateTick;
        PickRoundId = pickRoundId;
        TypeOfBetting = typeBet;
        PickNumber = pickNumber;
    }

    public Bet(long dateTick, int pickRoundId, TypeOfBet typeBet, int pickNumber, int result)
    {
        DateTick = dateTick;
        PickRoundId = pickRoundId;
        TypeOfBetting = typeBet;
        PickNumber = pickNumber;
        Result = result;
    }

    public void SetResult(int result)
    {
        Result = result;

    }



}
[Serializable]
public class Betting
{
    [SerializeField]
    TypeOfBet typeBet;
    [SerializeField]
    int number;


    public TypeOfBet TypeBet { get => typeBet; private set => typeBet = value; }
    public int Number { get => number; private set => number = value; }

    public Betting(TypeOfBet typeBet, int number)
    {
        TypeBet = typeBet;
        Number = number;
    }
}

[Serializable]
public class SaveFavorite
{
    [SerializeField] int playerId;
    [SerializeField] int saveFavoriteId;
    [SerializeField] long dateTick;
    [SerializeField] List<Betting> savedBettings = new List<Betting>();

    public int PlayerId { get => playerId; set => playerId = value; }
    public long DateTick { get => dateTick; set => dateTick = value; }
    public List<Betting> SavedBettings { get => savedBettings; set => savedBettings = value; }
    public int SaveFavoriteId { get => saveFavoriteId; set => saveFavoriteId = value; }

    public SaveFavorite(int playerId, int saveFavoriteId, long datePick, List<Betting> savedBettings)
    {
        PlayerId = playerId;
        SaveFavoriteId = saveFavoriteId;
        DateTick = datePick;
        SavedBettings = savedBettings;
    }
}

[Serializable]
public class WinningPayoff
{
    public TypeOfBet typeOf;
    public float percentage;
}
public enum TicketStatus
{
    Win,
    Lose,
    Pending,
}

public enum TypeOfBet
{
    Number,
    B,
    S,
    OD,
    EV,
}



