﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SlidingInOut : MonoBehaviour {

	public bool isMainScreen = false;
    
	[SerializeField] private bool isSlidingLeft;
	[SerializeField] private bool isSlidingRight;
	[SerializeField] private bool isSlidingBottom;
	[SerializeField] private bool isSlidingTop;
	[SerializeField] private float slidingSpeed = 20f;
	private RectTransform rectTrans;
	[SerializeField] private bool offScreen = true;
	private float slidingDirection = -1f;
	private bool isSliding = false;
	private float currentScreen;


	void Awake() {
		rectTrans = GetComponent<RectTransform> ();
	}

	void Start () {
        
		AppManager.Instance.SubscribeSlidingInOut (this);
		if (isSlidingTop || isSlidingBottom) {
			currentScreen = AppManager.Instance.screenHeight;
		} else {
			currentScreen = AppManager.Instance.screenWidth;
        }
        OnClickSlider();

    }

	void SlideIn(){
		offScreen = false;
	}

	void SlideOut(){
		offScreen = true;
	}

	public void SlidingSpeed (float speed){
		slidingSpeed = speed;
	}

	public void OnClickSlider(){
		isSliding = true;
		if (isSlidingLeft || isSlidingBottom) {
			slidingDirection = -1f;
		} else {
			slidingDirection = 1f;
		}

		if (offScreen) {
			currentScreen = 0f;
			if(!isMainScreen)
				AppManager.Instance.SubscribeCurrentActiveScreenList (this);
		} else {
			if (isSlidingTop || isSlidingBottom) {
				currentScreen = Screen.height;
			} else {
				currentScreen = Screen.width;
			}
			if(!isMainScreen)
				AppManager.Instance.UnSubscribeCurrentActiveScreenList (this);
		}

		offScreen = !offScreen;
	}



	// Update is called once per frame
	void Update () {
		if (isSliding) {
			if (isSlidingTop || isSlidingBottom) {
				rectTrans.offsetMin = new Vector2 
					(rectTrans.offsetMin.x, Mathf.Lerp (rectTrans.offsetMin.y, slidingDirection * currentScreen * (-rectTrans.anchorMin.y + 1f), slidingSpeed * Time.deltaTime));
				rectTrans.offsetMax = new Vector2 
					(rectTrans.offsetMax.x, Mathf.Lerp (rectTrans.offsetMax.y, slidingDirection * currentScreen * (rectTrans.anchorMax.y), slidingSpeed * Time.deltaTime));
			} else {
				rectTrans.offsetMin = new Vector2 (Mathf.Lerp (rectTrans.offsetMin.x, slidingDirection * currentScreen * (-rectTrans.anchorMin.x + 1f), slidingSpeed * Time.deltaTime), rectTrans.offsetMin.y);
				rectTrans.offsetMax = new Vector2 (Mathf.Lerp (rectTrans.offsetMax.x, slidingDirection * currentScreen * (rectTrans.anchorMax.x), slidingSpeed * Time.deltaTime), rectTrans.offsetMax.y);
			}

		}


	}
}
