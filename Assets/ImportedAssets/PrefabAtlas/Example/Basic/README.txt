How to use prefab atlas:

For a video on how to use the Prefab Atlas, please follow this link: https://www.youtube.com/watch?v=kxDwXxrw_do

For any questions please post them in the forums: http://forum.unity3d.com/threads/released-prefab-atlas-easiest-pooling-solution.334363/


The example (PrefabAtlas/Example/BasicSpawning) needs an extra step in order to work, due to the nature of this tool, the prefab itself cannot be added to the tool
or it will override your changes when updating.

Steps for testing the example:

1 - Open the BasicSpawning scene located in the Example folder.
2 - Open the PrefabAtlas window (Tools - > Prefab Atlas Editor), this will automatically create a prefab atlas in your project if one doesnt exist.
3 - In the editor window, locate at your right the button "Create Scene Object", click this button and an scene object will be created in your scene.
4 - On your left use the + button on the bottom to add a group, you can then select the newly created entry to rename it.
5 - In the example folder, there is a prefab called "Cube", drag this prefab on top of your newly created group to add it. You will notice information will appear in your Scene Object (right window).
6 - Commit your changes by clicking the commit button located at the top right of the window. this will force a recompilation.
7 - Make sure to save your project and save your scene in order to keep the changes.
8 - In your hierarchy, locate the "PrefabSpawner" game object, in there you will see a field named "SpawnID" followed by "Group:", select the group you just created, immediatly a new drop down menu will
	show called "ID", select the ID of the prefab you wish to instantiate.
9 - Press play, if you followed everything without issues you should see Cubes being spawned on the scene. you can modify the spawn rate in the "PrefabSpawner" to check the differences.

Please note that the "PrefabSpawner" is just an example, it is not a class meant to be used outside of the of the example itself, the purpose of the PrefabSpawner is to show you in code how you can
use a "PAT (Prefab Atlas Tag)" value to allow designers to customize what to spawn in the inspector. It is encouraged that you learn how to use the Prefab Atlas in code. 

Thank you for using the prefab atlas!