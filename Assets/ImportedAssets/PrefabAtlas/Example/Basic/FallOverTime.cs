﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class FallOverTime : MonoBehaviour
{
    #region Fields
    float ElapsedTime;
    float TargetTime;
    float FallSpeed;
    Vector3 Rotation = Vector3.zero;
    #endregion

    void OnEnable() //on enabled gets called everytime the prefab atlas gets an instance of this game object.
    {
        ElapsedTime = 0.0f;
        TargetTime = UnityEngine.Random.Range(3.0f, 4.0f);
        FallSpeed = 2.0f;
        Rotation = new Vector3(UnityEngine.Random.Range(-100.0f, 100.0f), UnityEngine.Random.Range(-100.0f, 100.0f), UnityEngine.Random.Range(-100.0f, 100.0f));
    }

    void Update()
    {
        Vector3 Cached = transform.position;
        Cached.y -= FallSpeed * Time.deltaTime;
        transform.position = Cached;
        transform.Rotate(Rotation * Time.deltaTime);
        ElapsedTime += Time.deltaTime;
        if(ElapsedTime >= TargetTime)
        {
            gameObject.SetActive(false); //Set active is all what we need to return this game object to the prefab atlas to be re used.
        }
    }
}

