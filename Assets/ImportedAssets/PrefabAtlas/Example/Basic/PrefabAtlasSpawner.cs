﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class PrefabAtlasSpawner : MonoBehaviour
{
    #region Fields
    public Vector3 SpawnPosition = Vector3.zero;
    public PAT SpawnID;
    public float SpawnRate = 1.0f;
    float ElapsedTime;
    #endregion

    PrefabContainer Container;
    void OnEnable()
    {
        ElapsedTime = 0.0f;
        Container = PrefabAtlas.Get(SpawnID);
    }

    void Update()
    {
        ElapsedTime += Time.deltaTime;
        if (ElapsedTime >= SpawnRate)
        {
            if (Container != null)
            {
                Container.Create(SpawnPosition);
            }
            else
            {
                Debug.Log("This prefab container is null, you probably using the 'None' tag or you havent defined any prefabs in this group");
            }
            ElapsedTime -= SpawnRate;
        }
    }

}

