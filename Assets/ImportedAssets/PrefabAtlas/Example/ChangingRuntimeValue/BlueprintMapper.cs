﻿using System;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;

/// <summary>
/// In this example we are going to be changing a prefab print from a pool (Determined by the PAT value) every 5 seconds, and then we will change it back again. this is
/// to illustrate how to change the prefab print in a pool during runtime.
/// </summary>
public class BlueprintMapper : MonoBehaviour
{
    public PAT ValueToChange; //this is the value we need to set in the editor in order to affect a pool
    public GameObject NewElement; //this is the prefab we directly set into the game object inspector. Very important that it is a prefab asset and not a common game object, if
                                  //it is a game object it could be destroyed and the pool will end up with a null print.
    GameObject OldPrint; //here we will store the print that was previously set inside the pool
    float ElapsedTime; //to keep track of the time to wait for 5 seconds before attempting to change the print once again.

    void OnEnable()
    {
        PrefabContainer Container = PrefabAtlas.Get(ValueToChange); //we first get the value from the atlas using the PAT
        OldPrint = Container.PrefabPrint; //inmediatly we cache the value of the print
        Container.SetNewPrefab(NewElement); //and finally change it, this is the important line since this is how you can change the print of the prefab
    }

    void Update()
    {
        ElapsedTime += Time.deltaTime; //once the time has gone up
        if(ElapsedTime >= 5.0f) //up to 5, we can switch back and forth
        {
            NewElement = OldPrint; //cache the current old print as the new print, we will use this to set the new element
            PrefabContainer Container = PrefabAtlas.Get(ValueToChange); //get the pool we want to modify
            OldPrint = Container.PrefabPrint; //cache the current element in the pool
            Container.SetNewPrefab(NewElement); //and finally set the new element as the print, the cycle will repeat constantly.
            ElapsedTime -= 5.0f;
        }
    }
    //This example is to be used along with the basic spawning, it is recomended you use the Cube prefab since it contains
    //an script that will deactive its gameobject after 2 seconds, sucessfully return it to the pool
}

