﻿using System;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;

public class PAModHolder : Attribute
{
    public string _ID;
    public PAModHolder(string ID = "")
    {
        _ID = ID;
    }
}

public class PAGroupModHolder : Attribute
{
    public string _ID;
    public PAGroupModHolder(string ID = "")
    {
        _ID = ID;
    }
}

