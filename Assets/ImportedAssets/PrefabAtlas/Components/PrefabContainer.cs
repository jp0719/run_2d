﻿using System;
using System.Collections.Generic;
using UnityEngine;
using PrefabAtlasElements;

/// <summary>
/// The prefab container will hold all the instance of a single prefab in run time. it will also take care of pooling them when they get created.
/// </summary>
public class PrefabContainer
{
    public GameObject PrefabPrint;
    public Stack<PrefabContainerNode> Head = new Stack<PrefabContainerNode>();
    public List<PrefabContainerNode> RecycledStack = new List<PrefabContainerNode>(); 
    public int Count;
    public int InternalHashPrint = 0;

    public int Alive;
    public int Capacity = -1;
    public int MaxInstance = -1;
    public int PrefabAtlasHashSetNum = 0;

    /// <summary>
    /// This will replace the current prefab held by this pool, all the current pooled elements will be destroyed and all the spawned elements that try to come back to the pool
    /// from the previous print will also be destroyed. This is to be used in runtime along with Asset bundles or the new UNET system to properly pool elements created from a server.
    /// Please take a look at the examples provided by unity on the subject since they tend to evolve and information here might be obsolete at some point in time.
    /// </summary>
    /// <param name="NewBlueprint"></param>
    public void SetNewPrefab(GameObject NewBlueprint)
    {
        PrefabPrint = NewBlueprint;
        InternalHashPrint = PrefabPrint.name.GetHashCode();
        UniqueID = 0;
        Alive = 0;
        Count = 0;
        //empty the pool
       
        while(Head.Count > 0)
        {
            PrefabContainerNode Container = Head.Pop();
            GameObject.Destroy(Container.Target);
        }
    }
    int UniqueID;
#if UNITY_EDITOR
    Transform Bag;

    public PrefabContainer(int HashSetNum, GameObject Print, Transform bag, int preloadCount, int capacity = -1, int maxInstance = -1)
    {
        PrefabAtlasHashSetNum = HashSetNum;
        UniqueID = 0;
        PrefabPrint = Print;
        InternalHashPrint = Print.name.GetHashCode(); //used to know if the prefab container has changed its value
        Bag = bag;
        for (int i = 0; i < preloadCount; i++)
        {
            Preload();
        }
        Alive = 0;
        Count = preloadCount;
        Capacity = capacity;
        MaxInstance = maxInstance;
        
    }
#else
    public PrefabContainer(int HashSetNum, GameObject Print, int preloadCount, int capacity = -1, int maxInstance = -1)
    {
        PrefabAtlasHashSetNum = HashSetNum;
        UniqueID = 0;
        PrefabPrint = Print;
        InternalHashPrint = Print.name.GetHashCode(); //used to know if the prefab container has changed its value
        for(int i = 0; i < preloadCount; i++)
        {
            Preload();
        }
        Capacity = capacity;
        MaxInstance = maxInstance;
    }
#endif

    public void CleanUp()
    {
        for (int i = 0; i < RecycledStack.Count; i++)
        {
            if(RecycledStack[i])//they could have been destroyed
            {
                Head.Push(RecycledStack[i]);
            }
        }
        RecycledStack.Clear();
    }

    public GameObject Preload()
    {
        GameObject TargetObject = null;
        Count++;
        TargetObject = GameObject.Instantiate(PrefabPrint) as GameObject;
        PrefabContainerNode NewNode = TargetObject.AddComponent<PrefabContainerNode>();
        NewNode.Target = TargetObject;
        NewNode.Parent = this;
        NewNode.PrintID = InternalHashPrint;
        if (TargetObject.activeSelf)
        {
            TargetObject.SetActive(false);
        }
#if UNITY_EDITOR
        TargetObject.name += UniqueID.ToString("000");
        UniqueID++;
        TargetObject.transform.SetParent(Bag);
#endif
        return TargetObject;
    }

    #region Generic Component
    public T Create<T>() where T : Component
    {
        GameObject TargetObject = null;
        T CachedElement = null;
        if (Head.Count == 0)
        {
            if (MaxInstance != -1)
            {
                if (Count >= MaxInstance)
                {
                    return null;
                }
            }
            Count++;
            TargetObject = GameObject.Instantiate(PrefabPrint, Vector3.zero, Quaternion.identity) as GameObject;
            PrefabContainerNode NewNode = TargetObject.AddComponent<PrefabContainerNode>();
            NewNode.Target = TargetObject;
            NewNode.CachedComponent = TargetObject.GetComponent<T>();
            NewNode.PrintID = InternalHashPrint;
            if (!NewNode.CachedComponent)
            {
                Debug.LogError("This prefab doesnt contain a component of type " + typeof(T).ToString());
            }
            CachedElement = (T)NewNode.CachedComponent;
            NewNode.Parent = this;
            if (!TargetObject.activeSelf)
            {
                TargetObject.SetActive(true);
            }
#if UNITY_EDITOR
            TargetObject.name += UniqueID.ToString("000");
            UniqueID++;
            TargetObject.transform.SetParent(Bag);
#endif
        }
        else
        {
            PrefabContainerNode Found = Head.Pop();
            TargetObject = Found.Target;
            CachedElement = (T)Found.CachedComponent;
            if (!CachedElement)
            {
                //you attempting to get another type from the previously asked to this container
                Found.CachedComponent = TargetObject.GetComponent<T>();
                CachedElement = (T)Found.CachedComponent;
                if (!CachedElement)
                {
                    Debug.LogError("This prefab doesnt contain a component of type " + typeof(T).ToString());
                }
            }
            Transform transform = TargetObject.transform;
            transform.rotation = Quaternion.identity;
            transform.position = Vector3.zero;
            TargetObject.SetActive(true);
        }
        Alive++;
        return CachedElement;
    }

    public T Create<T>(Quaternion Rotation) where T : Component
    {
        GameObject TargetObject = null;
        T CachedElement = null;
        if (Head.Count == 0)
        {
            if (MaxInstance != -1)
            {
                if (Count >= MaxInstance)
                {
                    return null;
                }
            }
            Count++;
            TargetObject = GameObject.Instantiate(PrefabPrint, Vector3.zero, Rotation) as GameObject;
            PrefabContainerNode NewNode = TargetObject.AddComponent<PrefabContainerNode>();
            NewNode.Target = TargetObject;
            NewNode.CachedComponent = TargetObject.GetComponent<T>();
            NewNode.PrintID = InternalHashPrint;
            if (!NewNode.CachedComponent)
            {
                Debug.LogError("This prefab doesnt contain a component of type " + typeof(T).ToString());
            }
            CachedElement = (T)NewNode.CachedComponent;
            NewNode.Parent = this;
            if (!TargetObject.activeSelf)
            {
                TargetObject.SetActive(true);
            }
#if UNITY_EDITOR
            TargetObject.name += UniqueID.ToString("000");
            UniqueID++;
            TargetObject.transform.SetParent(Bag);
#endif
        }
        else
        {
            PrefabContainerNode Found = Head.Pop();
            TargetObject = Found.Target;
            CachedElement = (T)Found.CachedComponent;
            if (!CachedElement)
            {
                //you attempting to get another type from the previously asked to this container
                Found.CachedComponent = TargetObject.GetComponent<T>();
                CachedElement = (T)Found.CachedComponent;
                if (!CachedElement)
                {
                    Debug.LogError("This prefab doesnt contain a component of type " + typeof(T).ToString());
                }
            }
            Transform transform = TargetObject.transform;
            transform.rotation = Rotation;
            transform.position = Vector3.zero;
            TargetObject.SetActive(true);
         
        }
        Alive++;
        return CachedElement;

    }

    public T Create<T>(Vector3 Pos) where T : Component
    {
        GameObject TargetObject = null;
        T CachedElement = null;
        if (Head.Count == 0)
        {
            if (MaxInstance != -1)
            {
                if (Count >= MaxInstance)
                {
                    return null;
                }
            }
            Count++;
            TargetObject = GameObject.Instantiate(PrefabPrint, Pos, Quaternion.identity) as GameObject;
            PrefabContainerNode NewNode = TargetObject.AddComponent<PrefabContainerNode>();
            NewNode.Target = TargetObject;
            NewNode.CachedComponent = TargetObject.GetComponent<T>();
            NewNode.PrintID = InternalHashPrint;
            if (!NewNode.CachedComponent)
            {
                Debug.LogError("This prefab doesnt contain a component of type " + typeof(T).ToString());
            }
            CachedElement = (T)NewNode.CachedComponent;
            NewNode.Parent = this;
            if (!TargetObject.activeSelf)
            {
                TargetObject.SetActive(true);
            }
#if UNITY_EDITOR
            TargetObject.name += UniqueID.ToString("000");
            UniqueID++;
            TargetObject.transform.SetParent(Bag);
#endif
        }
        else
        {
            PrefabContainerNode Found = Head.Pop();
            TargetObject = Found.Target;
            CachedElement = (T)Found.CachedComponent;
            if (!CachedElement)
            {
                //you attempting to get another type from the previously asked to this container
                Found.CachedComponent = TargetObject.GetComponent<T>();
                CachedElement = (T)Found.CachedComponent;
                if (!CachedElement)
                {
                    Debug.LogError("This prefab doesnt contain a component of type " + typeof(T).ToString());
                }
            }
            Transform transform = TargetObject.transform;
            transform.rotation = Quaternion.identity;
            transform.position = Pos;
            TargetObject.SetActive(true);
          
        }
        Alive++;
        return CachedElement;

    }

    public T Create<T>(Vector3 Pos, Quaternion Rotation) where T : Component
    {
        GameObject TargetObject = null;
        T CachedElement = null;
        if (Head.Count == 0)
        {
            if (MaxInstance != -1)
            {
                if (Count >= MaxInstance)
                {
                    return null;
                }
            }
            Count++;
            TargetObject = GameObject.Instantiate(PrefabPrint, Pos, Rotation) as GameObject;
            PrefabContainerNode NewNode = TargetObject.AddComponent<PrefabContainerNode>();
            NewNode.Target = TargetObject;
            NewNode.CachedComponent = TargetObject.GetComponent<T>();
            NewNode.PrintID = InternalHashPrint;
            if (!NewNode.CachedComponent)
            {
                Debug.LogError("This prefab doesnt contain a component of type " + typeof(T).ToString());
            }
            CachedElement = (T)NewNode.CachedComponent;
            NewNode.Parent = this;
            if (!TargetObject.activeSelf)
            {
                TargetObject.SetActive(true);
            }
#if UNITY_EDITOR
            TargetObject.name += UniqueID.ToString("000");
            UniqueID++;
            TargetObject.transform.SetParent( Bag);
#endif
        }
        else
        {
            PrefabContainerNode Found = Head.Pop();
            TargetObject = Found.Target;
            CachedElement = (T)Found.CachedComponent;
            if (!CachedElement)
            {
                //you attempting to get another type from the previously asked to this container
                Found.CachedComponent = TargetObject.GetComponent<T>();
                CachedElement = (T)Found.CachedComponent;
                if (!CachedElement)
                {
                    Debug.LogError("This prefab doesnt contain a component of type " + typeof(T).ToString());
                }
            }
            Transform transform = TargetObject.transform;
            transform.rotation = Rotation;
            transform.position = Pos;
            TargetObject.SetActive(true);
           
        }
        Alive++;
        return CachedElement;

    }

    public T Create<T>(float X, float Y, float Z) where T : Component
    {
        GameObject TargetObject = null;
        T CachedElement = null;
        if (Head.Count == 0)
        {
            if (MaxInstance != -1)
            {
                if (Count >= MaxInstance)
                {
                    return null;
                }
            }
            Count++;
            TargetObject = GameObject.Instantiate(PrefabPrint, new Vector3(X, Y, Z), Quaternion.identity) as GameObject;
            PrefabContainerNode NewNode = TargetObject.AddComponent<PrefabContainerNode>();
            NewNode.Target = TargetObject;
            NewNode.CachedComponent = TargetObject.GetComponent<T>();
            NewNode.PrintID = InternalHashPrint;
            if (!NewNode.CachedComponent)
            {
                Debug.LogError("This prefab doesnt contain a component of type " + typeof(T).ToString());
            }
            CachedElement = (T)NewNode.CachedComponent;
            NewNode.Parent = this;
            if (!TargetObject.activeSelf)
            {
                TargetObject.SetActive(true);
            }
#if UNITY_EDITOR
            TargetObject.name += UniqueID.ToString("000");
            UniqueID++;
            TargetObject.transform.SetParent(Bag);
#endif
        }
        else
        {
            PrefabContainerNode Found = Head.Pop();
            TargetObject = Found.Target;
            CachedElement = (T)Found.CachedComponent;
            if (!CachedElement)
            {
                //you attempting to get another type from the previously asked to this container
                Found.CachedComponent = TargetObject.GetComponent<T>();
                CachedElement = (T)Found.CachedComponent;
                if (!CachedElement)
                {
                    Debug.LogError("This prefab doesnt contain a component of type " + typeof(T).ToString());
                }
            }
            Transform transform = TargetObject.transform;
            transform.rotation = Quaternion.identity;
            transform.position = new Vector3(X, Y, Z);
            TargetObject.SetActive(true);
         
        }
        Alive++;
        return CachedElement;

    }

    public T Create<T>(float X, float Y, float Z, Quaternion Rotation) where T : Component
    {
        GameObject TargetObject = null;
        T CachedElement = null;
        if (Head.Count == 0)
        {
            if (MaxInstance != -1)
            {
                if (Count >= MaxInstance)
                {
                    return null;
                }
            }
            Count++;
            TargetObject = GameObject.Instantiate(PrefabPrint, new Vector3(X, Y, Z), Rotation) as GameObject;
            PrefabContainerNode NewNode = TargetObject.AddComponent<PrefabContainerNode>();
            NewNode.Target = TargetObject;
            NewNode.CachedComponent = TargetObject.GetComponent<T>();
            NewNode.PrintID = InternalHashPrint;
            if (!NewNode.CachedComponent)
            {
                Debug.LogError("This prefab doesnt contain a component of type " + typeof(T).ToString());
            }
            CachedElement = (T)NewNode.CachedComponent;
            NewNode.Parent = this;
            if (!TargetObject.activeSelf)
            {
                TargetObject.SetActive(true);
            }
#if UNITY_EDITOR
            TargetObject.name += UniqueID.ToString("000");
            UniqueID++;
            TargetObject.transform.SetParent(Bag);
#endif
        }
        else
        {
            PrefabContainerNode Found = Head.Pop();
            TargetObject = Found.Target;
            CachedElement = (T)Found.CachedComponent;
            if (!CachedElement)
            {
                //you attempting to get another type from the previously asked to this container
                Found.CachedComponent = TargetObject.GetComponent<T>();
                CachedElement = (T)Found.CachedComponent;
                if (!CachedElement)
                {
                    Debug.LogError("This prefab doesnt contain a component of type " + typeof(T).ToString());
                }
            }
            Transform transform = TargetObject.transform;
            transform.rotation = Rotation;
            transform.position = new Vector3(X, Y, Z);
            TargetObject.SetActive(true);
         
        }
        Alive++;
        return CachedElement;

    }
    #endregion

    public GameObject Create()
    {
        GameObject TargetObject = null;
        if (Head.Count == 0)
        {
            if (MaxInstance != -1)
            {
                if (Count >= MaxInstance)
                {
                    return null;
                }
            }
            Count++;
            TargetObject = GameObject.Instantiate(PrefabPrint, Vector3.zero, Quaternion.identity) as GameObject;
            PrefabContainerNode NewNode = TargetObject.AddComponent<PrefabContainerNode>();
            NewNode.Target = TargetObject;
            NewNode.Parent = this;
            NewNode.PrintID = InternalHashPrint;
            if (!TargetObject.activeSelf)
            {
                TargetObject.SetActive(true);
            }
#if UNITY_EDITOR
            TargetObject.name += UniqueID.ToString("000");
            UniqueID++;
            TargetObject.transform.SetParent(Bag);
#endif
        }
        else
        {
            PrefabContainerNode Found = Head.Pop();
            TargetObject = Found.Target;
            Transform transform = TargetObject.transform;
            transform.rotation = Quaternion.identity;
            transform.position = Vector3.zero;
            TargetObject.SetActive(true);
          
        }
        Alive++;
        return TargetObject;
    }

   

    public GameObject Create(Quaternion Rotation)
    {
        GameObject TargetObject = null;
        if (Head.Count == 0)
        {
            if (MaxInstance != -1)
            {
                if (Count >= MaxInstance)
                {
                    return null;
                }
            }
            Count++;
            TargetObject = GameObject.Instantiate(PrefabPrint, Vector3.zero, Rotation) as GameObject;
            PrefabContainerNode NewNode = TargetObject.AddComponent<PrefabContainerNode>();
            NewNode.Target = TargetObject;
            NewNode.Parent = this;
            NewNode.PrintID = InternalHashPrint;
            if (!TargetObject.activeSelf)
            {
                TargetObject.SetActive(true);
            }
#if UNITY_EDITOR
            TargetObject.name += UniqueID.ToString("000");
            UniqueID++;
            TargetObject.transform.SetParent(Bag);
#endif
        }
        else
        {
            PrefabContainerNode Found = Head.Pop();
            TargetObject = Found.Target;
            Transform transform = TargetObject.transform;
            transform.rotation = Rotation;
            transform.position = Vector3.zero;
            TargetObject.SetActive(true);
          
        }
        Alive++;
        return TargetObject;
    }

    public GameObject Create(Vector3 Pos)
    {
        GameObject TargetObject = null;
        if (Head.Count == 0)
        {
            if (MaxInstance != -1)
            {
                if (Count >= MaxInstance)
                {
                    return null;
                }
            }
            Count++;
            TargetObject = GameObject.Instantiate(PrefabPrint, Pos, Quaternion.identity) as GameObject;
            PrefabContainerNode NewNode = TargetObject.AddComponent<PrefabContainerNode>();
            NewNode.Target = TargetObject;
            NewNode.Parent = this;
            NewNode.PrintID = InternalHashPrint;
            if (!TargetObject.activeSelf)
            {
                TargetObject.SetActive(true);
            }
#if UNITY_EDITOR
            TargetObject.name += UniqueID.ToString("000");
            UniqueID++;
            TargetObject.transform.SetParent(Bag);
#endif
        }
        else
        {
            PrefabContainerNode Found = Head.Pop();
            TargetObject = Found.Target;
            Transform transform = TargetObject.transform;
            transform.rotation = Quaternion.identity;
            transform.position = Pos;
            TargetObject.SetActive(true);
            
        }
        Alive++;
        return TargetObject;
    }

    public GameObject Create(Vector3 Pos, Quaternion Rotation)
    {
        GameObject TargetObject = null;
        if (Head.Count == 0)
        {
            if (MaxInstance != -1)
            {
                if (Count >= MaxInstance)
                {
                    return null;
                }
            }
            Count++;
            TargetObject = GameObject.Instantiate(PrefabPrint, Pos, Rotation) as GameObject;
            PrefabContainerNode NewNode = TargetObject.AddComponent<PrefabContainerNode>();
            NewNode.Target = TargetObject;
            NewNode.Parent = this;
            NewNode.PrintID = InternalHashPrint;
            if (!TargetObject.activeSelf)
            {
                TargetObject.SetActive(true);
            }
#if UNITY_EDITOR
            TargetObject.name += UniqueID.ToString("000");
            UniqueID++;
            TargetObject.transform.SetParent(Bag);
#endif
        }
        else
        {
            PrefabContainerNode Found = Head.Pop();
            TargetObject = Found.Target;
            Transform transform = TargetObject.transform;
            transform.rotation = Rotation;
            transform.position = Pos;
            TargetObject.SetActive(true);
           
        }
        Alive++;
        return TargetObject;
    }

    public GameObject Create(float X, float Y, float Z)
    {
        GameObject TargetObject = null;
        if (Head.Count == 0)
        {
            if (MaxInstance != -1)
            {
                if (Count >= MaxInstance)
                {
                    return null;
                }
            }
            Count++;
            TargetObject = GameObject.Instantiate(PrefabPrint, new Vector3(X, Y, Z), Quaternion.identity) as GameObject;
            PrefabContainerNode NewNode = TargetObject.AddComponent<PrefabContainerNode>();
            NewNode.Target = TargetObject;
            NewNode.Parent = this;
            NewNode.PrintID = InternalHashPrint;
            if (!TargetObject.activeSelf)
            {
                TargetObject.SetActive(true);
            }
#if UNITY_EDITOR
            TargetObject.name += UniqueID.ToString("000");
            UniqueID++;
            TargetObject.transform.SetParent(Bag);
#endif
        }
        else
        {
            PrefabContainerNode Found = Head.Pop();
            TargetObject = Found.Target;
            Transform transform = TargetObject.transform;
            transform.rotation = Quaternion.identity;
            transform.position = new Vector3(X, Y, Z);
            TargetObject.SetActive(true);
          
        }
        Alive++;
        return TargetObject;
    }

    public GameObject Create(float X, float Y, float Z, Quaternion Rotation)
    {
        GameObject TargetObject = null;
        if (Head.Count == 0)
        {
            if (MaxInstance != -1)
            {
                if (Count >= MaxInstance)
                {
                    return null;
                }
            }
            Count++;
            TargetObject = GameObject.Instantiate(PrefabPrint, new Vector3(X, Y, Z), Rotation) as GameObject;
            PrefabContainerNode NewNode = TargetObject.AddComponent<PrefabContainerNode>();
            NewNode.Target = TargetObject;
            NewNode.Parent = this;
            NewNode.PrintID = InternalHashPrint;
            if (!TargetObject.activeSelf)
            {
                TargetObject.SetActive(true);
            }
#if UNITY_EDITOR
            TargetObject.name += UniqueID.ToString("000");
            UniqueID++;
            TargetObject.transform.SetParent(Bag);
#endif
        }
        else
        {
            PrefabContainerNode Found = Head.Pop();
            TargetObject = Found.Target;
            Transform transform = TargetObject.transform;
            transform.rotation = Rotation;
            transform.position = new Vector3(X, Y, Z);
            TargetObject.SetActive(true);
          
        }
        Alive++;
        return TargetObject;
    }

    #region Parenting options
    public GameObject CreateWithParent(Transform Parent)
    {
        GameObject TargetObject = null;
        if (Head.Count == 0)
        {
            if (MaxInstance != -1)
            {
                if (Count >= MaxInstance)
                {
                    return null;
                }
            }
            Count++;
            TargetObject = GameObject.Instantiate(PrefabPrint) as GameObject;
            PrefabContainerNode NewNode = TargetObject.AddComponent<PrefabContainerNode>();
            NewNode.Target = TargetObject;
            NewNode.Parent = this;
            NewNode.PrintID = InternalHashPrint;
            if (!TargetObject.activeSelf)
            {
                TargetObject.SetActive(true);
            }
#if UNITY_EDITOR
            TargetObject.name += UniqueID.ToString("000");
            UniqueID++;
#endif
            TargetObject.transform.SetParent(Parent);
        }
        else
        {
            PrefabContainerNode Found = Head.Pop();
            TargetObject = Found.Target;
            Transform transform = TargetObject.transform;
            transform.rotation = Quaternion.identity;
            transform.position = Vector3.zero;
            TargetObject.SetActive(true);
            transform.SetParent(Parent);
          
           
        }
        Alive++;
        return TargetObject;
    }

    public GameObject CreateWithParent(Transform Parent, Quaternion Rotation)
    {
        GameObject TargetObject = null;
        if (Head.Count == 0)
        {
            if (MaxInstance != -1)
            {
                if (Count >= MaxInstance)
                {
                    return null;
                }
            }
            Count++;
            TargetObject = GameObject.Instantiate(PrefabPrint, Vector3.zero, Rotation) as GameObject;
            PrefabContainerNode NewNode = TargetObject.AddComponent<PrefabContainerNode>();
            NewNode.Target = TargetObject;
            NewNode.Parent = this;
            NewNode.PrintID = InternalHashPrint;
            if (!TargetObject.activeSelf)
            {
                TargetObject.SetActive(true);
            }
#if UNITY_EDITOR
            TargetObject.name += UniqueID.ToString("000");
            UniqueID++;
#endif
            TargetObject.transform.SetParent(Parent);
        }
        else
        {
            PrefabContainerNode Found = Head.Pop();
            TargetObject = Found.Target;
            Transform transform = TargetObject.transform;
            transform.rotation = Rotation;
            transform.position = Vector3.zero;
            TargetObject.SetActive(true);
            transform.SetParent(Parent);
        }
        Alive++;
        return TargetObject;
    }

    public GameObject CreateWithParent(Transform Parent, Vector3 Pos)
    {
        GameObject TargetObject = null;
        if (Head.Count == 0)
        {
            if (MaxInstance != -1)
            {
                if (Count >= MaxInstance)
                {
                    return null;
                }
            }
            Count++;
            TargetObject = GameObject.Instantiate(PrefabPrint, Pos, Quaternion.identity) as GameObject;
            PrefabContainerNode NewNode = TargetObject.AddComponent<PrefabContainerNode>();
            NewNode.Target = TargetObject;
            NewNode.Parent = this;
            NewNode.PrintID = InternalHashPrint;
            if (!TargetObject.activeSelf)
            {
                TargetObject.SetActive(true);
            }
#if UNITY_EDITOR
            TargetObject.name += UniqueID.ToString("000");
            UniqueID++;
#endif
            TargetObject.transform.SetParent(Parent);
        }
        else
        {
            PrefabContainerNode Found = Head.Pop();
            TargetObject = Found.Target;
            Transform transform = TargetObject.transform;
            transform.rotation = Quaternion.identity;
            transform.position = Pos;
            TargetObject.SetActive(true);
            transform.SetParent(Parent);

        }
        Alive++;
        return TargetObject;
    }

    public GameObject CreateWithParent(Transform Parent, Vector3 Pos, Quaternion Rotation)
    {
        GameObject TargetObject = null;
        if (Head.Count == 0)
        {
            if (MaxInstance != -1)
            {
                if (Count >= MaxInstance)
                {
                    return null;
                }
            }
            Count++;
            TargetObject = GameObject.Instantiate(PrefabPrint, Pos, Rotation) as GameObject;
            PrefabContainerNode NewNode = TargetObject.AddComponent<PrefabContainerNode>();
            NewNode.Target = TargetObject;
            NewNode.Parent = this;
            NewNode.PrintID = InternalHashPrint;
            if (!TargetObject.activeSelf)
            {
                TargetObject.SetActive(true);
            }
#if UNITY_EDITOR
            TargetObject.name += UniqueID.ToString("000");
            UniqueID++;
#endif
            TargetObject.transform.SetParent(Parent);
        }
        else
        {
            PrefabContainerNode Found = Head.Pop();
            TargetObject = Found.Target;
            Transform transform = TargetObject.transform;
            transform.rotation = Rotation;
            transform.position = Pos;
            TargetObject.SetActive(true);
            transform.SetParent(Parent);

        }
        Alive++;
        return TargetObject;
    }

    public GameObject CreateWithParent(Transform Parent, float X, float Y, float Z)
    {
        GameObject TargetObject = null;
        if (Head.Count == 0)
        {
            if (MaxInstance != -1)
            {
                if (Count >= MaxInstance)
                {
                    return null;
                }
            }
            Count++;
            TargetObject = GameObject.Instantiate(PrefabPrint, new Vector3(X, Y, Z), Quaternion.identity) as GameObject;
            PrefabContainerNode NewNode = TargetObject.AddComponent<PrefabContainerNode>();
            NewNode.Target = TargetObject;
            NewNode.Parent = this;
            NewNode.PrintID = InternalHashPrint;
            if (!TargetObject.activeSelf)
            {
                TargetObject.SetActive(true);
            }
#if UNITY_EDITOR
            TargetObject.name += UniqueID.ToString("000");
            UniqueID++;
#endif
            TargetObject.transform.SetParent(Parent);
        }
        else
        {
            PrefabContainerNode Found = Head.Pop();
            TargetObject = Found.Target;
            Transform transform = TargetObject.transform;
            transform.rotation = Quaternion.identity;
            transform.position = new Vector3(X, Y, Z);
            TargetObject.SetActive(true);
            transform.SetParent(Parent);

        }
        Alive++;
        return TargetObject;
    }

    public GameObject CreateWithParent(Transform Parent, float X, float Y, float Z, Quaternion Rotation)
    {
        GameObject TargetObject = null;
        if (Head.Count == 0)
        {
            if (MaxInstance != -1)
            {
                if (Count >= MaxInstance)
                {
                    return null;
                }
            }
            Count++;
            TargetObject = GameObject.Instantiate(PrefabPrint, new Vector3(X, Y, Z), Rotation) as GameObject;
            PrefabContainerNode NewNode = TargetObject.AddComponent<PrefabContainerNode>();
            NewNode.Target = TargetObject;
            NewNode.Parent = this;
            NewNode.PrintID = InternalHashPrint;
            if (!TargetObject.activeSelf)
            {
                TargetObject.SetActive(true);
            }
#if UNITY_EDITOR
            TargetObject.name += UniqueID.ToString("000");
            UniqueID++;
#endif
            TargetObject.transform.SetParent(Parent);
        }
        else
        {
            PrefabContainerNode Found = Head.Pop();
            TargetObject = Found.Target;
            Transform transform = TargetObject.transform;
            transform.rotation = Rotation;
            transform.position = new Vector3(X, Y, Z);
            TargetObject.SetActive(true);
            transform.SetParent(Parent);

        }
        Alive++;
        return TargetObject;
    }
    #endregion

}
namespace PrefabAtlasElements
{

    public class PrefabContainerNode : MonoBehaviour
    {
        [HideInInspector]
        public PrefabContainer Parent = null;
        [HideInInspector]
        public GameObject Target = null;
        [HideInInspector]
        public Component CachedComponent = null;
        [HideInInspector]
        public bool Destroyed = false;
        public int PrintID = 0;

#if UNITY_EDITOR
        void Awake()
        {
            hideFlags = HideFlags.HideInInspector;
        }
#endif
        void OnDisable()
        {
            if (Parent.InternalHashPrint != PrintID)
            {
                //your prefab container has changed blue prints, this would mean that you are no longer needed inside the container, instead of disabling yourself you should destroy.
                Destroyed = true;
                Destroy(Target);
                return;
            }
            Parent.Alive--;
            if (Parent.Capacity != -1)
            {
                if (Parent.Count > Parent.Capacity)
                {
                    Destroyed = true;
                    Destroy(gameObject);
                    return;
                }
            }
            if (!Destroyed) //since Unity tends to be hectic about their execution order, this is a fail safe check. (also if we using the pool destruction method, will make it all faster.)
            {
                Parent.RecycledStack.Add(this);
            }
        }

        void OnDestroy()
        {
            if (Parent.InternalHashPrint != PrintID)
            {
                return;
            }
            Parent.Count--;
            //Elements that are not in the pool nor in the recycle stack will have their previous and next set to null, if this is the case then we need to check against the heads.
            Destroyed = true;
            Target = null;
            CachedComponent = null;
            Parent.RecycledStack.Remove(this);
            Parent = null;
           
        }

    }
}

