﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PrefabAtlasHelpers
{
    public class LabelHelper
    {
        public static GUIStyle FontStyle = null;
        public static void BoldTextAt(Rect Area, int Size, String Text)
        {
            GUI.skin.label.richText = true;
            GUI.Label(Area, new GUIContent("<b><size=" + Size.ToString() + ">" + Text + "</size></b>"));
        }

        public static void TextAt(Rect Area, int Size, String Text, string FontName, Color color, TextAnchor Alignment)
        {
            //if (FontStyle == null)
            //{
            //    FontStyle = new GUIStyle();
            //}
            FontStyle = new GUIStyle();
            FontStyle.font = (Font)Resources.Load(FontName);
            FontStyle.fontSize = Size;
            FontStyle.normal.textColor = color;
            FontStyle.stretchHeight = false;
            FontStyle.stretchWidth = false;
            FontStyle.alignment = Alignment; 
            GUI.Label(Area, new GUIContent(Text), FontStyle);
        }

        public static void TextAt(Rect Area, int Size, String Text, string FontName, Color color)
        {
            FontStyle = new GUIStyle();
            FontStyle.font = (Font)Resources.Load(FontName);
            FontStyle.fontSize = Size;
            FontStyle.normal.textColor = color;
            FontStyle.stretchHeight = false;
            FontStyle.stretchWidth = false;
            GUI.Label(Area, new GUIContent(Text), FontStyle);
        }


        public static void TextAt(Rect Area, int Size, String Text, string FontName)
        {
            FontStyle = new GUIStyle();
            FontStyle.font = (Font)Resources.Load(FontName);
            FontStyle.fontSize = Size;
            FontStyle.stretchHeight = false;
            FontStyle.stretchWidth = false;
            GUI.Label(Area, new GUIContent(Text), FontStyle);
        }
        public static void TextAtCentered(Rect Area, int Size, String Text, string FontName)
        {
            FontStyle = new GUIStyle();
            FontStyle.font = (Font)Resources.Load(FontName);
            FontStyle.fontSize = Size;
            FontStyle.stretchHeight = false;
            FontStyle.stretchWidth = false;
            FontStyle.alignment = TextAnchor.MiddleCenter;
            GUI.Label(Area, new GUIContent(Text), FontStyle);
        }

        public static void TextAtCentered(Rect Area, int Size, String Text, string FontName, Color color)
        {
            FontStyle = new GUIStyle();
            FontStyle.font = (Font)Resources.Load(FontName);
            FontStyle.fontSize = Size;
            FontStyle.stretchHeight = false;
            FontStyle.stretchWidth = false;
            FontStyle.alignment = TextAnchor.MiddleCenter;
            FontStyle.normal.textColor = color;
            GUI.Label(Area, new GUIContent(Text), FontStyle);
        }


        public static string TextFieldAt(Rect Area, int Size, String Text, string FontName)
        {
            FontStyle = new GUIStyle();
            FontStyle.font = (Font)Resources.Load(FontName);
            FontStyle.fontSize = Size;
            FontStyle.stretchHeight = false;
            return GUI.TextField(Area, Text, FontStyle);
        }

        public static string TextFieldAt(Rect Area, int Size, String Text, string FontName, Color color, TextAnchor Alignment)
        {
            FontStyle = new GUIStyle();
            FontStyle.font = (Font)Resources.Load(FontName);
            FontStyle.fontSize = Size;
            FontStyle.normal.textColor = color;
            FontStyle.stretchHeight = false;
            FontStyle.stretchWidth = false;
            FontStyle.alignment = Alignment;
            return GUI.TextField(Area, Text, FontStyle);
        }

        public static string TextFieldAt(Rect Area, int Size, String Text, string FontName, Color color, TextAnchor Alignment, float offset)
        {
            FontStyle = new GUIStyle();
            FontStyle.font = (Font)Resources.Load(FontName);
            FontStyle.fontSize = Size;
            FontStyle.normal.textColor = color;
            FontStyle.stretchHeight = false;
            FontStyle.stretchWidth = false;
            FontStyle.alignment = Alignment;
            FontStyle.contentOffset = new Vector2(offset, 0.0f);
            return GUI.TextField(Area, Text, FontStyle);
        }

        public static string TextFieldAt(Rect Area, int Size, String Text, string FontName, int MaxCharacters)
        {
            FontStyle = new GUIStyle();
            FontStyle.font = (Font)Resources.Load(FontName);
            FontStyle.fontSize = Size;
            if (Text.Length > MaxCharacters)
            {
                Text = Text.Remove(Text.Length - 1);
            }
            return GUI.TextField(Area, Text, FontStyle);
        }

        public static void TextAtWithBG(Rect Area, int Size, String Text, Color BG)
        {
            GUIStyle TheStyle = StyleHelpers.CreateWithBGColor(BG);
            TheStyle.wordWrap = true;
            TheStyle.fontSize = Size;
            //TheStyle.fontStyle = FontStyle.Bold;
            GUI.Label(Area, new GUIContent(Text), TheStyle);
        }

        public static void TextAtWithBG(Rect Area, int Size, String Text, Color BG, Vector2 Offset)
        {
            GUIStyle TheStyle = StyleHelpers.CreateWithBGColor(BG);
            TheStyle.wordWrap = true;
            TheStyle.fontSize = Size;
            //TheStyle.fontStyle = FontStyle.Bold;
            TheStyle.contentOffset = Offset;
            GUI.Label(Area, new GUIContent(Text), TheStyle);
        }

        public static void ShowTooltipQuestionMark(Rect Area, String Text)
        {
            GUI.Label(Area, new GUIContent("[?]", Text));
        }

        public static void TypeOnScreen(Rect Area, String Text)
        {
            GUI.Label(Area, new GUIContent(Text));
        }

        public static void HelpArea(Rect Area, String Text)
        {
            GUI.Label(Area, new GUIContent("", Text));
        }


        #region LAYOUT CHICK LABELS

        //public static void ChickLabel(string Text)
        //{
        //    GUIStyle TheStyle = new GUIStyle();
        //    TheStyle.wordWrap = true;
        //    TheStyle.fontStyle = FontStyle.Bold;
        //    GUILayout.Label(Text, TheStyle);
        //}
        //
        //public static void ChickLabel(string Text, params GUILayoutOption[] options)
        //{
        //    GUIStyle TheStyle = new GUIStyle();
        //    TheStyle.wordWrap = true;
        //    TheStyle.fontStyle = FontStyle.Bold;
        //    GUILayout.Label(Text, TheStyle, options);
        //}
        //
        //public static void ChickLabel(string Text, int Size)
        //{
        //    GUIStyle TheStyle = new GUIStyle();
        //    TheStyle.wordWrap = true;
        //    TheStyle.fontStyle = FontStyle.Bold;
        //    TheStyle.fontSize = Size;
        //    GUILayout.Label(Text, TheStyle);
        //}
        //
        //public static void ChickLabel(string Text, int Size, GUIStyle Style)
        //{
        //    GUIStyle TheStyle = Style;
        //    TheStyle.wordWrap = true;
        //    TheStyle.fontStyle = FontStyle.Bold;
        //    TheStyle.fontSize = Size;
        //    GUILayout.Label(Text, TheStyle);
        //}
        //
        //public static void ChickLabel(string Text, int Size, Color color)
        //{
        //    GUIStyle TheStyle = StyleHelpers.CreateWithBGColor(color);
        //    TheStyle.wordWrap = true;
        //    TheStyle.fontStyle = FontStyle.Bold;
        //    TheStyle.fontSize = Size;
        //    GUILayout.Label(Text, TheStyle);
        //}
        //
        //public static void ChickLabel(string Text, int Size, Color color, params GUILayoutOption[] options)
        //{
        //    GUIStyle TheStyle = StyleHelpers.CreateWithBGColor(color);
        //    TheStyle.wordWrap = true;
        //    TheStyle.fontStyle = FontStyle.Bold;
        //    TheStyle.fontSize = Size;
        //    GUILayout.Label(Text, TheStyle, options);
        //}

        #endregion
    }
}

