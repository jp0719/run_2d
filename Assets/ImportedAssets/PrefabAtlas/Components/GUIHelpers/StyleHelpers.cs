﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PrefabAtlasHelpers
{
    public class StyleHelpers : ScriptableObject
    {
        private static Texture2D Text;
        public static GUIStyle CreateWithBGColor(Color color)
        {
            GUIStyle TheStyle = new GUIStyle();
            if (Text == null)
            {
                Text = new Texture2D(1, 1);
                Text.wrapMode = TextureWrapMode.Repeat;
                Text.hideFlags = HideFlags.DontSave;
            }
            Color[] Colors = new Color[1];
            Colors[0] = color;

            Text.SetPixels(Colors);
            Text.Apply();
            TheStyle.normal.background = Text;

            return TheStyle;
        }
        public static GUIStyle GiveColor(GUIStyle ToMe, Color color)
        {
            if (Text == null)
            {
                Text = new Texture2D(1, 1);
                Text.wrapMode = TextureWrapMode.Repeat;
                Text.hideFlags = HideFlags.DontSave;
            }
            Color[] Colors = new Color[1];
            Colors[0] = color;

            Text.SetPixels(Colors);
            Text.Apply();
            ToMe.normal.background = Text;

            return ToMe;
        }

        public static GUIStyle CreateForBoxlessButtonBold()
        {
            GUIStyle Style = new GUIStyle();
            Style.fontStyle = FontStyle.Bold;
            Style.normal.background = null;
            return Style;
        }

        public static void CleanUpTexture()
        {
            if (Text != null)
            {
                GameObject.DestroyImmediate(Text);
                Text = null;
            }
        }
    }
}

