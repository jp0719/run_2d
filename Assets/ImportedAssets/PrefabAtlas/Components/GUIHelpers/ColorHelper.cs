﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PrefabAtlasHelpers
{
    public class ColorHelper
    {
        public static Color _Black_0 = GetRGB(0.0f, 0.0f, 0.0f, 1.0f);
        public static Color _Black_10 = GetRGB(30.0f, 30.0f, 30.0f, 1.0f);
        public static Color _Black_15 = GetRGB(35.0f, 35.0f, 35.0f, 1.0f);
        public static Color _Black_20 = GetRGB(50.0f, 50.0f, 50.0f, 1.0f);
        public static Color _Black_25 = GetRGB(55.0f, 55.0f, 55.0f, 1.0f);
        public static Color _Black_50 = GetRGB(70, 70, 70);
        public static Color _Grey_100 = GetRGB(120, 120, 120);
        public static Color _Grey_120 = GetRGB(140, 140, 140);
        public static Color _Grey_150 = GetRGB(150, 150, 150);
        public static Color _White_200 = GetRGB(200, 200, 200);

        public static Color _Blue_BhvrTool = GetRGB(0.0f, 110.0f, 242.0f);
        public static Color _Blue_BhvrTool_A1 = GetRGB(0.0f, 110.0f, 242.0f, 0.05f);

        public static Color _Orange_Neon_10 = GetRGB(230.0f, 112.0f, 12.0f, 1.0f);

        public static Color _Green_BhvrTool = GetRGB(0.0f, 255.0f, 100.0f, 1.0f);
        public static Color _Green_BhvrTool_A1 = GetRGB(0.0f, 255.0f, 100.0f, 0.2f);


        public static Color GetRGB(float R, float G, float B, float Alpha = 1.0f)
        {
            //R *= 2.0f;
            //G *= 2.0f;
            //B *= 2.0f;
            return new Color(R / 255.0f, G / 255.0f, B / 255.0f, Alpha);
        }
    }
}

