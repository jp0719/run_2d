﻿using System;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;


public class LayoutHelpers 
{
    public static void Horizontal(Action Do)
    {
        GUILayout.BeginHorizontal();
        Do();
        GUILayout.EndHorizontal();
    }
}

