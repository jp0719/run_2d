﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using PrefabAtlasElements;

#if UNITY_EDITOR
using UnityEditor;
#endif


[AddComponentMenu("")]
public partial class PrefabAtlas : MonoBehaviour
{
    public int _Version = 0;
    public static void CleanUpForSceneObject()
    {
        if (GameObject.FindObjectsOfType<PrefabAtlas>().Length > 1)
        {
            if (UniqueInstance != null)
            {
                Destroy(UniqueInstance.gameObject);
            }
        }
        UniqueInstance = null;
    }
    private static PrefabAtlas UniqueInstance = null;
    private PrefabAtlasSceneObject Mask;
    public static PrefabAtlas Instance
    {
        get
        {
            if (UniqueInstance == null)
            {
                PrefabAtlasSceneObject PrefabSceneManager = PrefabAtlasSceneObject.DirectAccess;
                if (PrefabSceneManager == null)
                {
                    PrefabSceneManager = GameObject.FindObjectOfType<PrefabAtlasSceneObject>();
                    if (PrefabSceneManager == null)
                    {
                        Debug.LogError("There is no prefab scene manager in this scene! prefab atlas needs a prefab scene object, otherwise it will not know what to load");
                        return null;
                    }
                }
                if (PrefabSceneManager.TheAtlas != null)
                {
                    UniqueInstance = (GameObject.Instantiate(PrefabSceneManager.TheAtlas.gameObject) as GameObject).GetComponent<PrefabAtlas>();
                    UniqueInstance.Mask = PrefabSceneManager;
                    UniqueInstance.OnAwake();
                }
                else
                {
#if UNITY_EDITOR
                    Selection.activeGameObject = PrefabSceneManager.gameObject;
#endif
                    Debug.LogError("No prefab atlas found, please create and link it to the PrefabAtlasSceneObject");
                }
            }

            return UniqueInstance;
        }
    }

    void OnAwake()
    {
        Debug.Log("=ATLAS INITIALIZING=");
        //create the dictionary
        AllPrefabContainers = new Dictionary<int, PrefabContainer>();
        //populate the dictionary
        int count = Groups.Count;
        for (int i = 0; i < count; i++)
        {
            PrefabAtlasGroup Current = Groups[i];
            int indexG = Mask.CurrentGroups.IndexOf(Current.GroupName);
            if (indexG != -1 && Mask.PreloadCounts.Count > indexG)
            {
                Current.IsActiveInThisScene = Mask.CurrentValues[indexG];
                GroupCountsData Preloading = Mask.PreloadCounts[indexG];
                if (Current.IsActiveInThisScene)
                {
                    if (Preloading != null && Preloading.Validate())
                    {
                        Current.Preload.AddRange(Preloading.PreloadCounts);
                        Current.Capacity.AddRange(Preloading.Capacities);
                        Current.MaxInstancing.AddRange(Preloading.MaxInstances);
                    }
                    else
                    {
                        //default values
                        Current.IsActiveInThisScene = true;
                        int elementcount = Current.Prefabs.Count;
                        for (int c = 0; c < elementcount; c++)
                        {
                            Current.Preload.Add(0);
                            Current.Capacity.Add(-1);
                            Current.MaxInstancing.Add(-1);
                        }
                    }
                }
                else
                {
                    Current.RuntimeNull();
                }
            }
            else
            {
                //default values
                Groups[i].IsActiveInThisScene = true;
                int elementcount = Groups[i].Prefabs.Count;
                for(int c = 0; c < elementcount; c++)
                {
                    Groups[i].Preload.Add(0);
                    Groups[i].Capacity.Add(-1);
                    Groups[i].MaxInstancing.Add(-1);
                }
            }
        }
        Mask.PreloadCounts = null;
        for (int i = 0; i < count; i++)
        {
            PrefabAtlasGroup CurrentGroup = Groups[i];
            if (CurrentGroup.IsActiveInThisScene)
            {
#if UNITY_EDITOR
                CurrentGroup.name = CurrentGroup.GroupName;
#endif
                Debug.Log("=GROUP CREATED!: " + CurrentGroup.GroupName + "=");
                int GroupElements = CurrentGroup.Prefabs.Count;
                for (int a = 0; a < GroupElements; a++)
                {
                    if(CurrentGroup.Hashes.Count <= a) { break; }
                    int Hash = CurrentGroup.Hashes[a];
                    if (!AllPrefabContainers.ContainsKey(Hash)) //we dont need to add the same element twice, even if a group defines the same element than another group.
                    {
#if UNITY_EDITOR
                        GameObject GO = new GameObject(CurrentGroup.Prefabs[a].name + "Bag"); //this is used for debugging only.
                        GO.transform.parent = CurrentGroup.transform;

                        GameObject Print = CurrentGroup.Prefabs[a];
                       
                        //When going through a scene to another scene with another configuration or if the scene object hasnt been updated, it could contain less preloads configurations
                        int Preload = CurrentGroup.GetPreload(a);
                        int Cap = CurrentGroup.GetCapacity(a);
                        int MaxInst = CurrentGroup.GetMaxInstancing(a);

                        AllPrefabContainers.Add(Hash, new PrefabContainer(Hash, Print, GO.transform, Preload, Cap, MaxInst));
#else
                        int Preload = CurrentGroup.GetPreload(a);
                        int Cap = CurrentGroup.GetCapacity(a);
                        int MaxInst = CurrentGroup.GetMaxInstancing(a);
                        AllPrefabContainers.Add(Hash, new PrefabContainer(Hash, CurrentGroup.Prefabs[a], Preload, Cap, MaxInst));
#endif
                    }
                }
            }
            else
            {
                Debug.Log("=GROUP IGNORED: "+ CurrentGroup.GroupName + "=");
#if UNITY_EDITOR
                Debug.Log("Group ignored: " + CurrentGroup.GroupName);
#endif
                //release the prefabs from this group
                CurrentGroup.Prefabs = null;
            }
        }
        int atlasCount = AllPrefabContainers.Count;
        InternalList = new PrefabContainer[atlasCount];
        int index = 0;
        foreach (PrefabContainer obj in AllPrefabContainers.Values)
        {
            InternalList[index] = obj;
            index++;
        }
        if(Application.isPlaying)
        Destroy(Mask.gameObject);
    }
    /// <summary>
    /// Returns all the groups in the prefab atlas.
    /// </summary>
    /// <returns></returns>
    public static List<PrefabAtlasGroup> GetGroups()
    {
        return Instance.Groups;
    }
    public List<PrefabAtlasGroup> Groups = new List<PrefabAtlasGroup>();
    public List<PABunch> Bunches = new List<PABunch>();
    public Dictionary<int, PrefabContainer> AllPrefabContainers = new Dictionary<int, PrefabContainer>();
    private PrefabContainer[] InternalList;

    void LateUpdate()
    {
        int count = InternalList.Length;
        for (int i = 0; i < count; i++)
        {
            InternalList[i].CleanUp();
        }
    }
    public static PrefabContainer Get(object PATValue)
    {
        PAT CastedVal = (PAT)PATValue;
        if(CastedVal != null)
        {
            return Instance.AllPrefabContainers[CastedVal.HashVal];
        }
        try
        {
            //int Val = (int)PATValue;

            return Instance.AllPrefabContainers[CastedVal.HashVal];
        }
        catch(Exception E)
        {
            Debug.LogError("The value passed to the atlas doesnt exist or is invalid|| " + E.Message);
        }
        return null;
    }
    public static PrefabContainer Spawn(object PATValue = null, object sec = null, object th = null, object fo = null)
    {
        PAT CastedVal = (PAT)PATValue;
        if (CastedVal != null)
        {
            return Instance.AllPrefabContainers[CastedVal.HashVal];
        }
        try
        {
            //int Val = (int)PATValue;

            return Instance.AllPrefabContainers[CastedVal.HashVal];
        }
        catch (Exception E)
        {
            Debug.LogError("The value passed to the atlas doesnt exist or is invalid|| " + E.Message);
        }
        return null;
    }
   

#if UNITY_EDITOR
    /// <summary>
    /// EDITOR ONLY
    /// </summary>
    /// <param name="Prefab"></param>
    /// <param name="Sce"></param>
    /// <returns></returns>
    public static PrefabAtlasGroup GetGroupOf(GameObject Prefab, PrefabAtlasSceneObject Sce)
    {
        if(Sce == null)
        {
            Debug.LogError("There is no Prefab Scene Object in this scene, please create one");
            return null;
        }
        PrefabAtlasGroup G = null;
        for(int i  = 0; i < Sce.CurrentGroups.Count; i++)
        {
            G = GetPrefabGroup(Sce.CurrentGroups[i], Sce.TheAtlas);
            if (G.Prefabs.Contains(Prefab))
            {
                break; 
            }
        }

        return G;
    }
    static PrefabAtlasGroup GetPrefabGroup(string Name, PrefabAtlas TheAtlas)
    {
        int count = TheAtlas.Groups.Count;
        for (int i = 0; i < count; i++)
        {
            if (TheAtlas.Groups[i].GroupName == Name)
            {
                return TheAtlas.Groups[i];
            }
        }
        return null;
    }
    void OnDestroy()
    {
        UniqueInstance = null;
    }
#endif
}


//property drawer
//Prefab Atlas Tag
[Serializable]
public class PAT
{
    [SerializeField]
    public PrefabAtlasGroup enumType;
    [SerializeField]
    public int HashVal;
    [SerializeField]
    public string SearchHelper = "";

    public bool IsValid()
    {
        if(HashVal == 0) { return false; }
        if(enumType == null) { return false; }
        if (!enumType.Hashes.Contains(HashVal)) { return false; }
        int Index = enumType.Hashes.IndexOf(HashVal);
        if (Index == -1) { return false; }
        if(enumType.Prefabs.Count <= Index) { return false; } //this is dangerous though, an element could have an index of 0 while it refers to another one.
        
        return true;
    }
    public GameObject SelfInstantiate(Vector3 pos, Quaternion rot)
    {
        if(enumType != null)
        {
            return (GameObject)GameObject.Instantiate(enumType.Prefabs[enumType.Hashes.IndexOf(HashVal)], pos, rot);
        }
        return null;
    }
}