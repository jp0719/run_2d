﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace PrefabAtlasElements
{
    [AddComponentMenu("")]
    public class PrefabAtlasSceneObject : MonoBehaviour
    {
        [HideInInspector]
        public int _Version = 0;
        public static PrefabAtlasSceneObject DirectAccess = null;
        [HideInInspector]
        public PrefabAtlas TheAtlas = null;
        [HideInInspector]
        public List<string> CurrentGroups = new List<string>();
        [HideInInspector]
        public List<bool> CurrentValues = new List<bool>();
        [HideInInspector]
        public List<bool> MinimizedValues = new List<bool>();
        [HideInInspector]
        public List<GroupCountsData> PreloadCounts = new List<GroupCountsData>();

        void Awake()
        {
            PrefabAtlas.CleanUpForSceneObject();
            DirectAccess = this;
            TheAtlas = PrefabAtlas.Instance;
        }
    }

    [Serializable]
    public class GroupCountsData
    {
        public string GroupName;
        public List<int> PreloadCounts = new List<int>();
        public List<int> Capacities = new List<int>();
        public List<int> MaxInstances = new List<int>();
        public List<GameObject> Prefabs = new List<GameObject>();

        public int DefaultPreloadCount = 0;
        public int DefaultCapacity = -1;
        public int DefaultMaxInstances = -1;

        public GroupCountsData(string name, List<GameObject> prefabs)
        {
            GroupName = name;
            Prefabs = prefabs;
            int count = Prefabs.Count;
            PreloadCounts = new List<int>(count);
            for (int i = 0; i < count; i++)
            {
                PreloadCounts.Add(0);
                Capacities.Add(-1);
                MaxInstances.Add(-1);
            }
        }

        public void SetValues(List<int> values, List<GameObject> ids, List<int> instancing, List<int> Maxs, int defpre, int defcap, int defmax)
        {
            if (values.Count != ids.Count)
            {
                return;
            }
            int valIndex = 0;
            for (int i = 0; i < ids.Count; i++)
            {
                GameObject current = ids[i];
                if (current == null)
                {
                    continue;
                }

                int indexOf = Prefabs.IndexOf(ids[i]);
                if (indexOf != -1)
                {
                    PreloadCounts[indexOf] = values[i];
                    Capacities[indexOf] = instancing[i];
                    MaxInstances[indexOf] = Maxs[i];
                }
                else
                {
                    valIndex--;
                }
                valIndex++;
            }
            DefaultPreloadCount = defpre;
            DefaultMaxInstances = defmax;
            DefaultCapacity = defcap;
        }
        public bool Validate()
        {
            if(Prefabs.Count == 0)
            {
                return false;
            }
            if(Prefabs.Count != PreloadCounts.Count)
            {
                return false;
            }
            if (Prefabs.Count != Capacities.Count)
            {
                return false;
            }
            if (Prefabs.Count != MaxInstances.Count)
            {
                return false;
            }
            return true;
        }
    }
}
