﻿using System;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;

[AddComponentMenu("")]
public partial class PABunch: MonoBehaviour
{
    public string _ID = "BunchName";
    public List<PAT> _SelectedItems = new List<PAT>();
}

