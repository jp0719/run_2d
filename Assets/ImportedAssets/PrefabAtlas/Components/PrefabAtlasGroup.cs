﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace PrefabAtlasElements
{
    /// <summary>
    /// The atlas group will define a single atlas group with a name, when creating this group, and adding prefabs to it, the main atlas class will extend its definitions to include 
    /// a tagging system that you will be able to access in code and in the inspector.
    /// </summary>
    [AddComponentMenu("")]
    [Serializable]
    public class PrefabAtlasGroup : MonoBehaviour
    {
        public bool IsActiveInThisScene = true;
        //[HideInInspector]
        public List<GameObject> Prefabs = new List<GameObject>();
        [HideInInspector]
        public List<int> Preload = new List<int>();
        [HideInInspector]
        public List<int> Capacity = new List<int>();
        [HideInInspector]
        public List<int> MaxInstancing = new List<int>();
        [HideInInspector]
        public List<int> Hashes = new List<int>();
        [HideInInspector]
        public string GroupName = "GroupName";
        [HideInInspector]
        public bool ShowInInspector = true;

        public int GetPreload(int index)
        {
            //return 0;
            if(Preload.Count <= index)
            {
                return 0;
            }
            return Preload[index];
        }

        public int GetCapacity(int index)
        {
            //return -1;
            if (Capacity.Count <= index)
            {
                return -1;
            }
            return Capacity[index];
        }

        public int GetMaxInstancing(int index)
        {
            //return -1;
            if (MaxInstancing.Count <= index)
            {
                return -1;
            }
            return MaxInstancing[index];
        }

        public string GetPrefabNameWithHash(int Hash)
        {
            int Index = Hashes.IndexOf(Hash);
            if(Index != -1)
            {
                return Prefabs[Index].name;
            }
            return string.Empty;
        }

        public void RuntimeNull()
        {
            Prefabs = null;
            Preload = null;
            Capacity = null;
            MaxInstancing = null;
            Hashes = null;
        }
    }
}

