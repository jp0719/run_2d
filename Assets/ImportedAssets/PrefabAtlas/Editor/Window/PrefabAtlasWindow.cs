﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using PrefabAtlasHelpers;
using PrefabAtlasElements;
using System.Diagnostics;

public class PrefabAtlasWindow : EditorWindow
{
    public enum PAWindowMode
    {
        NORMAL,
        BUNCHES,
        MASS_APPLY,
    }
    private static PrefabAtlasWindow _Window;
    public PrefabAtlas _Data;
    public PAElements _Elements;
    public Rect _CanvasArea;
    public Rect _GroupsRect;
    public Rect _EntriesRect;
    public Rect _InspectorRect;

    public PAWindowMode _ModeOn = PAWindowMode.NORMAL;

    [MenuItem("Tools/Prefab Atlas Editor")]
    public static void OpenWindow()
    {
        if (_Window == null)
        {
            _Window = EditorWindow.GetWindow<PrefabAtlasWindow>("PrefabAtlasWindow");
            PrefabAtlas Results = GetFirstPrefabOfType<PrefabAtlas>();
            if (Results == null) //If no prefab atlas is found, we will create one.
            {
                GameObject NewPrefabAtlas = new GameObject("PrefabAtlas");
                NewPrefabAtlas.AddComponent<PrefabAtlas>();
                string Path = "Assets" + FindTypeDirectory(typeof(PrefabAtlas)) + "PrefabAtlas.prefab";
                PrefabAtlas RetVal = PrefabUtility.CreatePrefab(Path, NewPrefabAtlas).GetComponent<PrefabAtlas>();
                DestroyImmediate(NewPrefabAtlas);
                Results = RetVal;
            }
            _Window._Data = Results;
            _Window._Elements = new PAElements();
        }
        _Window.Show();
    }

    void OnInspectorUpdate()
    {
        if (_Data == null)
        {
            return;
        }

        if (_Window == null)
        {
            _Window = this;
        }
        if (Application.isPlaying)
        {
            Repaint();
        }
    }
    static GUIStyle StyleSelected;
    void OnGUI()
    {
        IntegrityCheck();
        
            RenderNormalView();
       

        if (GUI.Button(new Rect(Screen.width - 120.0f, 0.0f, 120.0f, 15.0f), new GUIContent("Commit", "This will change the settings to reflect the current prefab atlas, remember to save your project to keep the changes done to your prefab atlas and save your scene if you made any changes to your prefab scene object setup")))
        {
            CommitChanges();
            EditorUtility.SetDirty(_Data);
        }

            StyleSelected = new GUIStyle(GUI.skin.button);
            StyleSelected.normal.background = new Texture2D(2, 2, TextureFormat.RGBA32, false);
            Color R = Color.green;
            StyleSelected.normal.background.SetPixels(new Color[4] { R, R, R, R });
        

       
            _ModeOn = PAWindowMode.NORMAL;
    
    }

    void RenderNormalView()
    {
        PAElements.DefaultSkin = GUI.skin;
        _CanvasArea = new Rect(0.0f, 0.0f, Screen.width, Screen.height);
        float EntriesAndGroupsWidth = Mathf.Round(Screen.width * 0.3f > 200.0f ? 200.0f : Screen.width * 0.3f);
        float Separation = 5.0f;
        _GroupsRect = new Rect(0.0f, 17.0f, EntriesAndGroupsWidth, Screen.height - 17.0f);
        _EntriesRect = new Rect(EntriesAndGroupsWidth + Separation, 17.0f, EntriesAndGroupsWidth, Screen.height - 17.0f);
        _InspectorRect = new Rect(EntriesAndGroupsWidth * 2.0f + Separation * 2.0f, 17.0f, Screen.width - (EntriesAndGroupsWidth * 2.0f + Separation * 2.0f), Screen.height - 17.0f);
        //Render the BG.
        RenderHelpers.RenderBox(_CanvasArea, ColorHelper._Black_10);
        //Render the Entries.
        _Elements.RenderEntries(this, _EntriesRect);
        //Render the Groups.
        _Elements.RenderGroups(this, _GroupsRect);
        //Render the Entry data
        _Elements.RenderInspector(this, _InspectorRect);
        GUI.skin = PAElements.DefaultSkin;
    }

    void RenderBunches()
    {
        PAElements.DefaultSkin = GUI.skin;
        _CanvasArea = new Rect(0.0f, 0.0f, Screen.width, Screen.height);
        RenderHelpers.RenderBox(_CanvasArea, ColorHelper._Black_10);
        RenderHelpers.RenderBox(new Rect(_CanvasArea.x, _CanvasArea.y + 15.0f, _CanvasArea.width, 2.0f), ColorHelper._Blue_BhvrTool);
        _CanvasArea.y += 18;
        Rect ListRect = new Rect(_CanvasArea.x, _CanvasArea.y, Mathf.Round(_CanvasArea.width * 0.4f), _CanvasArea.height);
        Rect ViewerRect = new Rect(Mathf.Round(_CanvasArea.x + _CanvasArea.width * 0.4f), _CanvasArea.y, Mathf.Round(_CanvasArea.width * 0.6f), _CanvasArea.height);
        _Elements.RenderBunches(this, ListRect);
        _Elements.BunchesInspector(this, ViewerRect);

        GUI.skin = PAElements.DefaultSkin;
    }

    void RenderMassApply()
    {
        PAElements.DefaultSkin = GUI.skin;
        _CanvasArea = new Rect(0.0f, 0.0f, Screen.width, Screen.height);
        float EntriesAndGroupsWidth = Mathf.Round(Screen.width * 0.45f);
        float Separation = 5.0f;
        _GroupsRect = new Rect(0.0f, 17.0f, EntriesAndGroupsWidth, Screen.height - 17.0f);
        _EntriesRect = new Rect(EntriesAndGroupsWidth + Separation, 17.0f, Screen.width - EntriesAndGroupsWidth - Separation, Screen.height - 17.0f);
        //Render the BG.
        RenderHelpers.RenderBox(_CanvasArea, ColorHelper._Black_10);
        _Elements.RenderGroups(this, _GroupsRect);
        _Elements.RenderGroupsOptions(this, _EntriesRect);
        GUI.skin = PAElements.DefaultSkin;
    }

    void IntegrityCheck()
    {
        _Window = this;
        if (_Data == null)
        {
            PrefabAtlas Results = GetFirstPrefabOfType<PrefabAtlas>();
            if (Results == null) //If no prefab atlas is found, we will create one.
            {
                GameObject NewPrefabAtlas = new GameObject("PrefabAtlas");
                NewPrefabAtlas.AddComponent<PrefabAtlas>();
                string Path = "Assets" + FindTypeDirectory(typeof(PrefabAtlas)) + "PrefabAtlas.prefab";
                PrefabAtlas RetVal = PrefabUtility.CreatePrefab(Path, NewPrefabAtlas).GetComponent<PrefabAtlas>();
                DestroyImmediate(NewPrefabAtlas);
                Results = RetVal;
            }
            _Window._Data = Results;
            Repaint();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    private void CommitChanges()
    {
        PrefabAtlas Atlas = _Data;
        if (Atlas == null) { return; }
        #region NAME CHECKING
        //check if all the elements in the atlas have unique names
        List<PrefabAtlasGroup> groups = Atlas.Groups;
        for (int i = 0; i < groups.Count - 1; i++)
        {
            PrefabAtlasGroup Group = groups[i];
            string toCompare = Group.GroupName;
            toCompare = toCompare.Replace(' ', '_');
            for (int a = 1 + i; a < groups.Count; a++)
            {
                PrefabAtlasGroup Compare = groups[a];
                string target = Compare.GroupName;
                target = target.Replace(' ', '_');
                if (toCompare == target)
                {
                    UnityEngine.Debug.LogError("Cant apply changes (automatically or manually) because two groups share the same name, please rename group: " + Compare.GroupName);
                    return;
                }
            }

            //Hash set
            Group.Hashes = new List<int>();
            for (int x = 0; x < Group.Prefabs.Count; x++)
            {
                Group.Hashes.Add(Group.Prefabs[x].name.GetHashCode());
            }
        }

        List<PABunch> Bunches = Atlas.Bunches;
        for (int i = 0; i < Bunches.Count - 1; i++)
        {
            PABunch Group = Bunches[i];
            string toCompare = Group._ID;
            toCompare = toCompare.Replace(' ', '_');
            for (int a = 1 + i; a < Bunches.Count; a++)
            {
                PABunch Compare = Bunches[a];
                string target = Compare._ID;
                target = target.Replace(' ', '_');
                if (toCompare == target)
                {
                    UnityEngine.Debug.LogError("Cant apply changes (automatically or manually) because two bunches share the same name, please rename Bunch: " + Compare._ID);
                    return;
                }
            }
        }

        //None check
        bool ContainsNone = false;

        #endregion
        #region CREATING TAGS AND EXTENSIONS
        if (Application.isPlaying)
        {
            UnityEngine.Debug.LogError("Cant do modifications while playing");
            return;
        }
        string DataPath = Application.dataPath + "/" + FindTypeDirectory(typeof(PrefabAtlas)) + "PrefabAtlasExtension.cs";
        string final = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), DataPath);


        using (StreamWriter W = new StreamWriter(final, false))
        {
            W.WriteLine("using UnityEngine;");
            W.WriteLine("using System;");
            W.WriteLine("using System.Collections.Generic;");
            W.WriteLine("using PrefabAtlasElements;");
            #region create the enums using the group names + tag
            for (int i = 0; i < groups.Count; i++)
            {
                List<GameObject> prefabs = groups[i].Prefabs;
                int prefabCount = prefabs.Count;
                //none check
                for (int a = 0; a < prefabCount; a++)
                {
                    GameObject CurrentPrefab = prefabs[a];
                    string prefabName = CurrentPrefab.name;
                    prefabName = prefabName.Replace(' ', '_');
                    if (prefabName == "None")
                    {
                        ContainsNone = true;
                    }
                }

                string GroupNameFixed = groups[i].GroupName;
                GroupNameFixed = GroupNameFixed.Replace(' ', '_');

                if (prefabCount > 0)
                {


                    //create random access
                    W.WriteLine("public partial class PARandom");
                    W.WriteLine("{");
                    W.WriteLine("    public static " + GroupNameFixed + "Tag " + GroupNameFixed + "()");
                    W.WriteLine("    {");
                    W.WriteLine("        int M = PrefabAtlas.RandomRange(0," + prefabCount + ");");
                    W.WriteLine("        switch(M)");
                    W.WriteLine("        {");
                    for (int a = 0; a < prefabCount; a++)
                    {
                        W.WriteLine("            case " + a + ":");
                        GameObject CurrentPrefab = prefabs[a];
                        string prefabName = CurrentPrefab.name;
                        prefabName = prefabName.Replace(' ', '_');
                        W.WriteLine("            return " + GroupNameFixed + "Tag." + prefabName + ";");
                    }
                    W.WriteLine("        }");
                    W.WriteLine("        return default(" + GroupNameFixed + "Tag);");
                    W.WriteLine("    }");
                    W.WriteLine("}");
                }

                W.WriteLine("public enum " + GroupNameFixed + "Tag");
                W.WriteLine("{");
                if (!ContainsNone)
                {
                    W.WriteLine("    None = 0,");
                }


                for (int a = 0; a < prefabCount; a++)
                {
                    GameObject CurrentPrefab = prefabs[a];
                    int Hash = CurrentPrefab.name.GetHashCode();
                    string prefabName = CurrentPrefab.name;
                    prefabName = prefabName.Replace(' ', '_');
                    W.WriteLine("    " + prefabName + " = " + Hash.ToString() + ",");
                }
                W.WriteLine("}");
                W.WriteLine("");
            }
            #endregion
            #region create the class
            W.WriteLine("public partial class PrefabAtlas");
            W.WriteLine("{");
            W.WriteLine("    static System.Random Randomizer = null;");
            W.WriteLine("    public static int RandomRange(int min, int max){ if(Randomizer == null){ Randomizer = new System.Random((int)System.DateTime.Now.Ticks); } return Randomizer.Next(min, max); }");
            List<string> _GroupsIDS = new List<string>();
            List<string> _UnmodifiedGroupsID = new List<string>();
            for (int i = 0; i < groups.Count; i++)
            {
                string GroupNameFixed = groups[i].GroupName;
                _UnmodifiedGroupsID.Add(GroupNameFixed);
                GroupNameFixed = GroupNameFixed.Replace(' ', '_');
                _GroupsIDS.Add(GroupNameFixed);
                int PrefabCount = groups[i].Prefabs.Count;
                for (int a = 0; a < PrefabCount; a++)
                {
                    //Add the GroupName tags fields for each container.
                    GameObject CurrentPrefab = groups[i].Prefabs[a];
                    string prefabName = CurrentPrefab.name;
                    prefabName = prefabName.Replace(' ', '_');
                    W.WriteLine("    PrefabContainer " + GroupNameFixed + "_" + prefabName + "_Container;");
                }
                W.WriteLine("    ///<summary>");
                W.WriteLine("    /// Gets the container from the atlas of type " + GroupNameFixed);
                W.WriteLine("    ///</summary>");
                W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
                W.WriteLine("    public static PrefabContainer Get(" + GroupNameFixed + "Tag ID)");
                W.WriteLine("    {");
                W.WriteLine("        if(UniqueInstance == null){ UniqueInstance = Instance; }");
                //if (PrefabCount > 0)
                //{
                W.WriteLine("        switch(ID)");
                W.WriteLine("        {");
                if (!ContainsNone)
                {
                    W.WriteLine("            case " + GroupNameFixed + "Tag.None:");
                    W.WriteLine("               return null;");
                }
                for (int a = 0; a < PrefabCount; a++)
                {
                    GameObject CurrentPrefab = groups[i].Prefabs[a];
                    string prefabName = CurrentPrefab.name;
                    prefabName = prefabName.Replace(' ', '_');

                    W.WriteLine("            case " + GroupNameFixed + "Tag." + prefabName + ":");
                    W.WriteLine("               if(UniqueInstance." + GroupNameFixed + "_" + prefabName + "_Container == null){ UniqueInstance." + GroupNameFixed + "_" + prefabName + "_Container = UniqueInstance.AllPrefabContainers[(int)ID];}");
                    W.WriteLine("               return UniqueInstance." + GroupNameFixed + "_" + prefabName + "_Container;");
                }
                W.WriteLine("        }");
                //}
                W.WriteLine("        return Instance.AllPrefabContainers[(int)ID];");
                W.WriteLine("    }");
                W.WriteLine("");

                ///////NEW SPAWN METHODS (Tags)
                //parameterless
                W.WriteLine("    ///<summary>");
                W.WriteLine("    /// Spawns an element from the pool that belongs to the " + GroupNameFixed + " group");
                W.WriteLine("    ///</summary>");
                W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
                W.WriteLine("    public static GameObject Spawn(" + GroupNameFixed + "Tag ID)");
                W.WriteLine("    {");
                W.WriteLine("        PrefabContainer Got = Get(ID);");
                W.WriteLine("        if(Got == null) { return null; }");
                W.WriteLine("        return Got.Create();");
                W.WriteLine("    }");
                W.WriteLine("");
                //with position
                W.WriteLine("    ///<summary>");
                W.WriteLine("    /// Spawns an element from the pool that belongs to the " + GroupNameFixed + " group at the given location");
                W.WriteLine("    ///</summary>");
                W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
                W.WriteLine("    /// <param name=\"Position\">The given location in the world where this instance will be placed at</param>");
                W.WriteLine("    public static GameObject Spawn(" + GroupNameFixed + "Tag ID, Vector3 Position)");
                W.WriteLine("    {");
                W.WriteLine("        PrefabContainer Got = Get(ID);");
                W.WriteLine("        if(Got == null) { return null; }");
                W.WriteLine("        return Got.Create(Position);");
                W.WriteLine("    }");
                W.WriteLine("");
                //with rotation
                W.WriteLine("    ///<summary>");
                W.WriteLine("    /// Spawns an element from the pool that belongs to the " + GroupNameFixed + " group with the given rotation");
                W.WriteLine("    ///</summary>");
                W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
                W.WriteLine("    /// <param name=\"Rotation\">The given rotation which this instance will have when spawned</param>");
                W.WriteLine("    public static GameObject Spawn(" + GroupNameFixed + "Tag ID, Quaternion Rotation)");
                W.WriteLine("    {");
                W.WriteLine("        PrefabContainer Got = Get(ID);");
                W.WriteLine("        if(Got == null) { return null; }");
                W.WriteLine("        return Got.Create(Rotation);");
                W.WriteLine("    }");
                W.WriteLine("");
                //with rotation and position
                W.WriteLine("    ///<summary>");
                W.WriteLine("    /// Spawns an element from the pool that belongs to the " + GroupNameFixed + " group with the given rotation");
                W.WriteLine("    ///</summary>");
                W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
                W.WriteLine("    /// <param name=\"Position\">The given location in the world where this instance will be placed at</param>");
                W.WriteLine("    /// <param name=\"Rotation\">The given rotation which this instance will have when spawned</param>");
                W.WriteLine("    public static GameObject Spawn(" + GroupNameFixed + "Tag ID, Vector3 Position, Quaternion Rotation)");
                W.WriteLine("    {");
                W.WriteLine("        PrefabContainer Got = Get(ID);");
                W.WriteLine("        if(Got == null) { return null; }");
                W.WriteLine("        return Got.Create(Position, Rotation);");
                W.WriteLine("    }");
                W.WriteLine("");

                //WITH PARENT
                ///////NEW SPAWN METHODS (Tags)
                //parameterless
                W.WriteLine("    ///<summary>");
                W.WriteLine("    /// Spawns an element from the pool that belongs to the " + GroupNameFixed + " group as a Child");
                W.WriteLine("    ///</summary>");
                W.WriteLine("    /// <param name=\"Parent\">The Transform that will contain the gameobject</param>");
                W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
                W.WriteLine("    public static GameObject SpawnWithParent(Transform Parent, " + GroupNameFixed + "Tag ID)");
                W.WriteLine("    {");
                W.WriteLine("        PrefabContainer Got = Get(ID);");
                W.WriteLine("        if(Got == null) { return null; }");
                W.WriteLine("        GameObject GO = Got.Create();");
                W.WriteLine("        GO.transform.parent = Parent;");
                W.WriteLine("        return GO;");
                W.WriteLine("    }");
                W.WriteLine("");
                //with position
                W.WriteLine("    ///<summary>");
                W.WriteLine("    /// Spawns an element from the pool that belongs to the " + GroupNameFixed + " group at the given location as a Child");
                W.WriteLine("    ///</summary>");
                W.WriteLine("    /// <param name=\"Parent\">The Transform that will contain the gameobject</param>");
                W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
                W.WriteLine("    /// <param name=\"Position\">The given location in the world where this instance will be placed at</param>");
                W.WriteLine("    public static GameObject SpawnWithParent(Transform Parent, " + GroupNameFixed + "Tag ID, Vector3 Position)");
                W.WriteLine("    {");
                W.WriteLine("        PrefabContainer Got = Get(ID);");
                W.WriteLine("        if(Got == null) { return null; }");
                W.WriteLine("        GameObject GO = Got.Create(Position);");
                W.WriteLine("        GO.transform.parent = Parent;");
                W.WriteLine("        return GO;");
                W.WriteLine("    }");
                W.WriteLine("");
                //with rotation
                W.WriteLine("    ///<summary>");
                W.WriteLine("    /// Spawns an element from the pool that belongs to the " + GroupNameFixed + " group with the given rotation as a Child");
                W.WriteLine("    ///</summary>");
                W.WriteLine("    /// <param name=\"Parent\">The Transform that will contain the gameobject</param>");
                W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
                W.WriteLine("    /// <param name=\"Rotation\">The given rotation which this instance will have when spawned</param>");
                W.WriteLine("    public static GameObject SpawnWithParent(Transform Parent, " + GroupNameFixed + "Tag ID, Quaternion Rotation)");
                W.WriteLine("    {");
                W.WriteLine("        PrefabContainer Got = Get(ID);");
                W.WriteLine("        if(Got == null) { return null; }");
                W.WriteLine("        GameObject GO = Got.Create(Rotation);");
                W.WriteLine("        GO.transform.parent = Parent;");
                W.WriteLine("        return GO;");
                W.WriteLine("    }");
                W.WriteLine("");
                //with rotation and position
                W.WriteLine("    ///<summary>");
                W.WriteLine("    /// Spawns an element from the pool that belongs to the " + GroupNameFixed + " group with the given rotation as a Child");
                W.WriteLine("    ///</summary>");
                W.WriteLine("    /// <param name=\"Parent\">The Transform that will contain the gameobject</param>");
                W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
                W.WriteLine("    /// <param name=\"Position\">The given location in the world where this instance will be placed at</param>");
                W.WriteLine("    /// <param name=\"Rotation\">The given rotation which this instance will have when spawned</param>");
                W.WriteLine("    public static GameObject SpawnWithParent(Transform Parent, " + GroupNameFixed + "Tag ID, Vector3 Position, Quaternion Rotation)");
                W.WriteLine("    {");
                W.WriteLine("        PrefabContainer Got = Get(ID);");
                W.WriteLine("        if(Got == null) { return null; }");
                W.WriteLine("        GameObject GO = Got.Create(Position, Rotation);");
                W.WriteLine("        GO.transform.parent = Parent;");
                W.WriteLine("        return GO;");
                W.WriteLine("    }");
                W.WriteLine("");


                //GENERICS
                W.WriteLine("    ///<summary>");
                W.WriteLine("    /// Spawns an element from the pool that belongs to the " + GroupNameFixed + " group and inmediatly gets a component of type T, this component gets cached meaning that if you ask the pool for an element with the same component it will avoid calling GetComponent on it, if you instead ask for another type of component the system will cache that type of component and erase the previously cached.");
                W.WriteLine("    ///</summary>");
                W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
                W.WriteLine("    public static T Spawn<T>(" + GroupNameFixed + "Tag ID) where T: Component");
                W.WriteLine("    {");
                W.WriteLine("        PrefabContainer Got = Get(ID);");
                W.WriteLine("        if(Got == null) { return null; }");
                W.WriteLine("        return Got.Create<T>();");
                W.WriteLine("    }");
                W.WriteLine("");
                //with position
                W.WriteLine("    ///<summary>");
                W.WriteLine("    /// Spawns an element from the pool that belongs to the " + GroupNameFixed + " group and inmediatly gets a component of type T, this component gets cached meaning that if you ask the pool for an element with the same component it will avoid calling GetComponent on it, if you instead ask for another type of component the system will cache that type of component and erase the previously cached.");
                W.WriteLine("    ///</summary>");
                W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
                W.WriteLine("    /// <param name=\"Position\">The given location in the world where this instance will be placed at</param>");
                W.WriteLine("    public static T Spawn<T>(" + GroupNameFixed + "Tag ID, Vector3 Position) where T: Component");
                W.WriteLine("    {");
                W.WriteLine("        PrefabContainer Got = Get(ID);");
                W.WriteLine("        if(Got == null) { return null; }");
                W.WriteLine("        return Got.Create<T>(Position);");
                W.WriteLine("    }");
                W.WriteLine("");
                //with rotation
                W.WriteLine("    ///<summary>");
                W.WriteLine("    /// Spawns an element from the pool that belongs to the " + GroupNameFixed + " group and inmediatly gets a component of type T, this component gets cached meaning that if you ask the pool for an element with the same component it will avoid calling GetComponent on it, if you instead ask for another type of component the system will cache that type of component and erase the previously cached.");
                W.WriteLine("    ///</summary>");
                W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
                W.WriteLine("    /// <param name=\"Rotation\">The given rotation which this instance will have when spawned</param>");
                W.WriteLine("    public static T Spawn<T>(" + GroupNameFixed + "Tag ID, Quaternion Rotation) where T: Component");
                W.WriteLine("    {");
                W.WriteLine("        PrefabContainer Got = Get(ID);");
                W.WriteLine("        if(Got == null) { return null; }");
                W.WriteLine("        return Got.Create<T>(Rotation);");
                W.WriteLine("    }");
                W.WriteLine("");
                //with rotation and position
                W.WriteLine("    ///<summary>");
                W.WriteLine("    /// Spawns an element from the pool that belongs to the " + GroupNameFixed + " group and inmediatly gets a component of type T, this component gets cached meaning that if you ask the pool for an element with the same component it will avoid calling GetComponent on it, if you instead ask for another type of component the system will cache that type of component and erase the previously cached.");
                W.WriteLine("    ///</summary>");
                W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
                W.WriteLine("    /// <param name=\"Position\">The given location in the world where this instance will be placed at</param>");
                W.WriteLine("    /// <param name=\"Rotation\">The given rotation which this instance will have when spawned</param>");
                W.WriteLine("    public static T Spawn<T>(" + GroupNameFixed + "Tag ID, Vector3 Position, Quaternion Rotation) where T: Component");
                W.WriteLine("    {");
                W.WriteLine("        PrefabContainer Got = Get(ID);");
                W.WriteLine("        if(Got == null) { return null; }");
                W.WriteLine("        return Got.Create<T>(Position, Rotation);");
                W.WriteLine("    }");
                W.WriteLine("");

                //WITH PARENTS GENERICS
                //GENERICS
                W.WriteLine("    ///<summary>");
                W.WriteLine("    /// Spawns an element from the pool that belongs to the " + GroupNameFixed + " group and inmediatly gets a component of type T, this component gets cached meaning that if you ask the pool for an element with the same component it will avoid calling GetComponent on it, if you instead ask for another type of component the system will cache that type of component and erase the previously cached.");
                W.WriteLine("    ///</summary>");
                W.WriteLine("    /// <param name=\"Parent\">The Transform that will contain the gameobject</param>");
                W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
                W.WriteLine("    public static T SpawnWithParent<T>(Transform Parent, " + GroupNameFixed + "Tag ID) where T: Component");
                W.WriteLine("    {");
                W.WriteLine("        PrefabContainer Got = Get(ID);");
                W.WriteLine("        if(Got == null) { return null; }");
                W.WriteLine("        T GO = Got.Create<T>();");
                W.WriteLine("        GO.transform.parent = Parent;");
                W.WriteLine("        return GO;");
                W.WriteLine("    }");
                W.WriteLine("");
                //with position
                W.WriteLine("    ///<summary>");
                W.WriteLine("    /// Spawns an element from the pool that belongs to the " + GroupNameFixed + " group and inmediatly gets a component of type T, this component gets cached meaning that if you ask the pool for an element with the same component it will avoid calling GetComponent on it, if you instead ask for another type of component the system will cache that type of component and erase the previously cached.");
                W.WriteLine("    ///</summary>");
                W.WriteLine("    /// <param name=\"Parent\">The Transform that will contain the gameobject</param>");
                W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
                W.WriteLine("    /// <param name=\"Position\">The given location in the world where this instance will be placed at</param>");
                W.WriteLine("    public static T SpawnWithParent<T>(Transform Parent, " + GroupNameFixed + "Tag ID, Vector3 Position) where T: Component");
                W.WriteLine("    {");
                W.WriteLine("        PrefabContainer Got = Get(ID);");
                W.WriteLine("        if(Got == null) { return null; }");
                W.WriteLine("        T GO = Got.Create<T>(Position);");
                W.WriteLine("        GO.transform.parent = Parent;");
                W.WriteLine("        return GO;");
                W.WriteLine("    }");
                W.WriteLine("");
                //with rotation
                W.WriteLine("    ///<summary>");
                W.WriteLine("    /// Spawns an element from the pool that belongs to the " + GroupNameFixed + " group and inmediatly gets a component of type T, this component gets cached meaning that if you ask the pool for an element with the same component it will avoid calling GetComponent on it, if you instead ask for another type of component the system will cache that type of component and erase the previously cached.");
                W.WriteLine("    ///</summary>");
                W.WriteLine("    /// <param name=\"Parent\">The Transform that will contain the gameobject</param>");
                W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
                W.WriteLine("    /// <param name=\"Rotation\">The given rotation which this instance will have when spawned</param>");
                W.WriteLine("    public static T SpawnWithParent<T>(Transform Parent, " + GroupNameFixed + "Tag ID, Quaternion Rotation) where T: Component");
                W.WriteLine("    {");
                W.WriteLine("        PrefabContainer Got = Get(ID);");
                W.WriteLine("        if(Got == null) { return null; }");
                W.WriteLine("        T GO = Got.Create<T>(Rotation);");
                W.WriteLine("        GO.transform.parent = Parent;");
                W.WriteLine("        return GO;");
                W.WriteLine("    }");
                W.WriteLine("");
                //with rotation and position
                W.WriteLine("    ///<summary>");
                W.WriteLine("    /// Spawns an element from the pool that belongs to the " + GroupNameFixed + " group and inmediatly gets a component of type T, this component gets cached meaning that if you ask the pool for an element with the same component it will avoid calling GetComponent on it, if you instead ask for another type of component the system will cache that type of component and erase the previously cached.");
                W.WriteLine("    ///</summary>");
                W.WriteLine("    /// <param name=\"Parent\">The Transform that will contain the gameobject</param>");
                W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
                W.WriteLine("    /// <param name=\"Position\">The given location in the world where this instance will be placed at</param>");
                W.WriteLine("    /// <param name=\"Rotation\">The given rotation which this instance will have when spawned</param>");
                W.WriteLine("    public static T SpawnWithParent<T>(Transform Parent, " + GroupNameFixed + "Tag ID, Vector3 Position, Quaternion Rotation) where T: Component");
                W.WriteLine("    {");
                W.WriteLine("        PrefabContainer Got = Get(ID);");
                W.WriteLine("        if(Got == null) { return null; }");
                W.WriteLine("        T GO = Got.Create<T>(Position, Rotation);");
                W.WriteLine("        GO.transform.parent = Parent;");
                W.WriteLine("        return GO;");
                W.WriteLine("    }");
                W.WriteLine("");
                //////END NEW SPAWN METHODS
            }

            #region PAT accessors
            //PAT accessors
            //parameterless
            W.WriteLine("    ///<summary>");
            W.WriteLine("    /// Spawns an element from the pool that belongs to the given PAT group");
            W.WriteLine("    ///</summary>");
            W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
            W.WriteLine("    public static GameObject Spawn(PAT ID)");
            W.WriteLine("    {");
            W.WriteLine("        PrefabContainer Got = Get(ID);");
            W.WriteLine("        if(Got == null) { return null; }");
            W.WriteLine("        return Got.Create();");
            W.WriteLine("    }");
            W.WriteLine("");
            //with position
            W.WriteLine("    ///<summary>");
            W.WriteLine("    /// Spawns an element from the pool that belongs to the given PAT group");
            W.WriteLine("    ///</summary>");
            W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
            W.WriteLine("    /// <param name=\"Position\">The given location in the world where this instance will be placed at</param>");
            W.WriteLine("    public static GameObject Spawn(PAT ID, Vector3 Position)");
            W.WriteLine("    {");
            W.WriteLine("        PrefabContainer Got = Get(ID);");
            W.WriteLine("        if(Got == null) { return null; }");
            W.WriteLine("        return Got.Create(Position);");
            W.WriteLine("    }");
            W.WriteLine("");
            //with rotation
            W.WriteLine("    ///<summary>");
            W.WriteLine("    /// Spawns an element from the pool that belongs to the given PAT group");
            W.WriteLine("    ///</summary>");
            W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
            W.WriteLine("    /// <param name=\"Rotation\">The given rotation which this instance will have when spawned</param>");
            W.WriteLine("    public static GameObject Spawn(PAT ID, Quaternion Rotation)");
            W.WriteLine("    {");
            W.WriteLine("        PrefabContainer Got = Get(ID);");
            W.WriteLine("        if(Got == null) { return null; }");
            W.WriteLine("        return Got.Create(Rotation);");
            W.WriteLine("    }");
            W.WriteLine("");
            //with rotation and position
            W.WriteLine("    ///<summary>");
            W.WriteLine("    /// Spawns an element from the pool that belongs to the given PAT group");
            W.WriteLine("    ///</summary>");
            W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
            W.WriteLine("    /// <param name=\"Position\">The given location in the world where this instance will be placed at</param>");
            W.WriteLine("    /// <param name=\"Rotation\">The given rotation which this instance will have when spawned</param>");
            W.WriteLine("    public static GameObject Spawn(PAT ID, Vector3 Position, Quaternion Rotation)");
            W.WriteLine("    {");
            W.WriteLine("        PrefabContainer Got = Get(ID);");
            W.WriteLine("        if(Got == null) { return null; }");
            W.WriteLine("        return Got.Create(Position, Rotation);");
            W.WriteLine("    }");
            W.WriteLine("");

            //WITH PARENTS
            //PAT accessors
            //parameterless
            W.WriteLine("    ///<summary>");
            W.WriteLine("    /// Spawns an element from the pool that belongs to the given PAT group as a child");
            W.WriteLine("    ///</summary>");
            W.WriteLine("    /// <param name=\"Parent\">The Transform that will contain the gameobject</param>");
            W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
            W.WriteLine("    public static GameObject SpawnWithParent(Transform Parent, PAT ID)");
            W.WriteLine("    {");
            W.WriteLine("        PrefabContainer Got = Get(ID);");
            W.WriteLine("        if(Got == null) { return null; }");
            W.WriteLine("        GameObject GO = Got.Create();");
            W.WriteLine("        GO.transform.parent = Parent;");
            W.WriteLine("        return GO;");
            W.WriteLine("    }");
            W.WriteLine("");
            //with position
            W.WriteLine("    ///<summary>");
            W.WriteLine("    /// Spawns an element from the pool that belongs to the given PAT group as a child");
            W.WriteLine("    ///</summary>");
            W.WriteLine("    /// <param name=\"Parent\">The Transform that will contain the gameobject</param>");
            W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
            W.WriteLine("    /// <param name=\"Position\">The given location in the world where this instance will be placed at</param>");
            W.WriteLine("    public static GameObject SpawnWithParent(Transform Parent, PAT ID, Vector3 Position)");
            W.WriteLine("    {");
            W.WriteLine("        PrefabContainer Got = Get(ID);");
            W.WriteLine("        if(Got == null) { return null; }");
            W.WriteLine("        GameObject GO = Got.Create(Position);");
            W.WriteLine("        GO.transform.parent = Parent;");
            W.WriteLine("        return GO;");
            W.WriteLine("    }");
            W.WriteLine("");
            //with rotation
            W.WriteLine("    ///<summary>");
            W.WriteLine("    /// Spawns an element from the pool that belongs to the given PAT group as a child");
            W.WriteLine("    ///</summary>");
            W.WriteLine("    /// <param name=\"Parent\">The Transform that will contain the gameobject</param>");
            W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
            W.WriteLine("    /// <param name=\"Rotation\">The given rotation which this instance will have when spawned</param>");
            W.WriteLine("    public static GameObject SpawnWithParent(Transform Parent, PAT ID, Quaternion Rotation)");
            W.WriteLine("    {");
            W.WriteLine("        PrefabContainer Got = Get(ID);");
            W.WriteLine("        if(Got == null) { return null; }");
            W.WriteLine("        GameObject GO = Got.Create(Rotation);");
            W.WriteLine("        GO.transform.parent = Parent;");
            W.WriteLine("        return GO;");
            W.WriteLine("    }");
            W.WriteLine("");
            //with rotation and position
            W.WriteLine("    ///<summary>");
            W.WriteLine("    /// Spawns an element from the pool that belongs to the given PAT group as a child");
            W.WriteLine("    ///</summary>");
            W.WriteLine("    /// <param name=\"Parent\">The Transform that will contain the gameobject</param>");
            W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
            W.WriteLine("    /// <param name=\"Position\">The given location in the world where this instance will be placed at</param>");
            W.WriteLine("    /// <param name=\"Rotation\">The given rotation which this instance will have when spawned</param>");
            W.WriteLine("    public static GameObject SpawnWithParent(Transform Parent, PAT ID, Vector3 Position, Quaternion Rotation)");
            W.WriteLine("    {");
            W.WriteLine("        PrefabContainer Got = Get(ID);");
            W.WriteLine("        if(Got == null) { return null; }");
            W.WriteLine("        GameObject GO = Got.Create(Position, Rotation);");
            W.WriteLine("        GO.transform.parent = Parent;");
            W.WriteLine("        return GO;");
            W.WriteLine("    }");
            W.WriteLine("");

            //GENERICS
            W.WriteLine("    ///<summary>");
            W.WriteLine("    /// Spawns an element from the pool that belongs to the given PAT group and inmediatly gets a component of type T, this component gets cached meaning that if you ask the pool for an element with the same component it will avoid calling GetComponent on it, if you instead ask for another type of component the system will cache that type of component and erase the previously cached.");
            W.WriteLine("    ///</summary>");
            W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
            W.WriteLine("    public static T Spawn<T>(PAT ID) where T: Component");
            W.WriteLine("    {");
            W.WriteLine("        PrefabContainer Got = Get(ID);");
            W.WriteLine("        if(Got == null) { return null; }");
            W.WriteLine("        return Got.Create<T>();");
            W.WriteLine("    }");
            W.WriteLine("");
            //with position
            W.WriteLine("    ///<summary>");
            W.WriteLine("    /// Spawns an element from the pool that belongs to the given PAT group and inmediatly gets a component of type T, this component gets cached meaning that if you ask the pool for an element with the same component it will avoid calling GetComponent on it, if you instead ask for another type of component the system will cache that type of component and erase the previously cached.");
            W.WriteLine("    ///</summary>");
            W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
            W.WriteLine("    /// <param name=\"Position\">The given location in the world where this instance will be placed at</param>");
            W.WriteLine("    public static T Spawn<T>(PAT ID, Vector3 Position) where T: Component");
            W.WriteLine("    {");
            W.WriteLine("        PrefabContainer Got = Get(ID);");
            W.WriteLine("        if(Got == null) { return null; }");
            W.WriteLine("        return Got.Create<T>(Position);");
            W.WriteLine("    }");
            W.WriteLine("");
            //with rotation
            W.WriteLine("    ///<summary>");
            W.WriteLine("    /// Spawns an element from the pool that belongs to the given PAT group and inmediatly gets a component of type T, this component gets cached meaning that if you ask the pool for an element with the same component it will avoid calling GetComponent on it, if you instead ask for another type of component the system will cache that type of component and erase the previously cached.");
            W.WriteLine("    ///</summary>");
            W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
            W.WriteLine("    /// <param name=\"Rotation\">The given rotation which this instance will have when spawned</param>");
            W.WriteLine("    public static T Spawn<T>(PAT ID, Quaternion Rotation) where T: Component");
            W.WriteLine("    {");
            W.WriteLine("        PrefabContainer Got = Get(ID);");
            W.WriteLine("        if(Got == null) { return null; }");
            W.WriteLine("        return Got.Create<T>(Rotation);");
            W.WriteLine("    }");
            W.WriteLine("");
            //with rotation and position
            W.WriteLine("    ///<summary>");
            W.WriteLine("    /// Spawns an element from the pool that belongs to the given PAT group and inmediatly gets a component of type T, this component gets cached meaning that if you ask the pool for an element with the same component it will avoid calling GetComponent on it, if you instead ask for another type of component the system will cache that type of component and erase the previously cached.");
            W.WriteLine("    ///</summary>");
            W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
            W.WriteLine("    /// <param name=\"Position\">The given location in the world where this instance will be placed at</param>");
            W.WriteLine("    /// <param name=\"Rotation\">The given rotation which this instance will have when spawned</param>");
            W.WriteLine("    public static T Spawn<T>(PAT ID, Vector3 Position, Quaternion Rotation) where T: Component");
            W.WriteLine("    {");
            W.WriteLine("        PrefabContainer Got = Get(ID);");
            W.WriteLine("        if(Got == null) { return null; }");
            W.WriteLine("        return Got.Create<T>(Position, Rotation);");
            W.WriteLine("    }");
            W.WriteLine("");

            //Generics with parent
            //GENERICS
            W.WriteLine("    ///<summary>");
            W.WriteLine("    /// Spawns an element from the pool that belongs to the given PAT group and inmediatly gets a component of type T, this component gets cached meaning that if you ask the pool for an element with the same component it will avoid calling GetComponent on it, if you instead ask for another type of component the system will cache that type of component and erase the previously cached.");
            W.WriteLine("    ///</summary>");
            W.WriteLine("    /// <param name=\"Parent\">The Transform that will contain the gameobject</param>");
            W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
            W.WriteLine("    public static T SpawnWithParent<T>(Transform Parent, PAT ID) where T: Component");
            W.WriteLine("    {");
            W.WriteLine("        PrefabContainer Got = Get(ID);");
            W.WriteLine("        if(Got == null) { return null; }");
            W.WriteLine("        T Go = Got.Create<T>();");
            W.WriteLine("        Go.transform.parent = Parent;");
            W.WriteLine("        return Go;");
            W.WriteLine("    }");
            W.WriteLine("");
            //with position
            W.WriteLine("    ///<summary>");
            W.WriteLine("    /// Spawns an element from the pool that belongs to the given PAT group and inmediatly gets a component of type T, this component gets cached meaning that if you ask the pool for an element with the same component it will avoid calling GetComponent on it, if you instead ask for another type of component the system will cache that type of component and erase the previously cached.");
            W.WriteLine("    ///</summary>");
            W.WriteLine("    /// <param name=\"Parent\">The Transform that will contain the gameobject</param>");
            W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
            W.WriteLine("    /// <param name=\"Position\">The given location in the world where this instance will be placed at</param>");
            W.WriteLine("    public static T SpawnWithParent<T>(Transform Parent, PAT ID, Vector3 Position) where T: Component");
            W.WriteLine("    {");
            W.WriteLine("        PrefabContainer Got = Get(ID);");
            W.WriteLine("        if(Got == null) { return null; }");
            W.WriteLine("        T Go = Got.Create<T>(Position);");
            W.WriteLine("        Go.transform.parent = Parent;");
            W.WriteLine("        return Go;");
            W.WriteLine("    }");
            W.WriteLine("");
            //with rotation
            W.WriteLine("    ///<summary>");
            W.WriteLine("    /// Spawns an element from the pool that belongs to the given PAT group and inmediatly gets a component of type T, this component gets cached meaning that if you ask the pool for an element with the same component it will avoid calling GetComponent on it, if you instead ask for another type of component the system will cache that type of component and erase the previously cached.");
            W.WriteLine("    ///</summary>");
            W.WriteLine("    /// <param name=\"Parent\">The Transform that will contain the gameobject</param>");
            W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
            W.WriteLine("    /// <param name=\"Rotation\">The given rotation which this instance will have when spawned</param>");
            W.WriteLine("    public static T SpawnWithParent<T>(Transform Parent, PAT ID, Quaternion Rotation) where T: Component");
            W.WriteLine("    {");
            W.WriteLine("        PrefabContainer Got = Get(ID);");
            W.WriteLine("        if(Got == null) { return null; }");
            W.WriteLine("        T Go = Got.Create<T>(Rotation);");
            W.WriteLine("        Go.transform.parent = Parent;");
            W.WriteLine("        return Go;");
            W.WriteLine("    }");
            W.WriteLine("");
            //with rotation and position
            W.WriteLine("    ///<summary>");
            W.WriteLine("    /// Spawns an element from the pool that belongs to the given PAT group and inmediatly gets a component of type T, this component gets cached meaning that if you ask the pool for an element with the same component it will avoid calling GetComponent on it, if you instead ask for another type of component the system will cache that type of component and erase the previously cached.");
            W.WriteLine("    ///</summary>");
            W.WriteLine("    /// <param name=\"Parent\">The Transform that will contain the gameobject</param>");
            W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
            W.WriteLine("    /// <param name=\"Position\">The given location in the world where this instance will be placed at</param>");
            W.WriteLine("    /// <param name=\"Rotation\">The given rotation which this instance will have when spawned</param>");
            W.WriteLine("    public static T SpawnWithParent<T>(Transform Parent, PAT ID, Vector3 Position, Quaternion Rotation) where T: Component");
            W.WriteLine("    {");
            W.WriteLine("        PrefabContainer Got = Get(ID);");
            W.WriteLine("        if(Got == null) { return null; }");
            W.WriteLine("        T Go = Got.Create<T>(Position, Rotation);");
            W.WriteLine("        Go.transform.parent = Parent;");
            W.WriteLine("        return Go;");
            W.WriteLine("    }");
            W.WriteLine("");
            #endregion

            #region INT accessors
            //PAT accessors
            //parameterless
            W.WriteLine("    ///<summary>");
            W.WriteLine("    /// Spawns an element from the pool that belongs to the given group");
            W.WriteLine("    ///</summary>");
            W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
            W.WriteLine("    public static GameObject Spawn(int ID)");
            W.WriteLine("    {");
            W.WriteLine("        PrefabContainer Got = Get(ID);");
            W.WriteLine("        if(Got == null) { return null; }");
            W.WriteLine("        return Got.Create();");
            W.WriteLine("    }");
            W.WriteLine("");
            //with position
            W.WriteLine("    ///<summary>");
            W.WriteLine("    /// Spawns an element from the pool that belongs to the given group");
            W.WriteLine("    ///</summary>");
            W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
            W.WriteLine("    /// <param name=\"Position\">The given location in the world where this instance will be placed at</param>");
            W.WriteLine("    public static GameObject Spawn(int ID, Vector3 Position)");
            W.WriteLine("    {");
            W.WriteLine("        PrefabContainer Got = Get(ID);");
            W.WriteLine("        if(Got == null) { return null; }");
            W.WriteLine("        return Got.Create(Position);");
            W.WriteLine("    }");
            W.WriteLine("");
            //with rotation
            W.WriteLine("    ///<summary>");
            W.WriteLine("    /// Spawns an element from the pool that belongs to the given group");
            W.WriteLine("    ///</summary>");
            W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
            W.WriteLine("    /// <param name=\"Rotation\">The given rotation which this instance will have when spawned</param>");
            W.WriteLine("    public static GameObject Spawn(int ID, Quaternion Rotation)");
            W.WriteLine("    {");
            W.WriteLine("        PrefabContainer Got = Get(ID);");
            W.WriteLine("        if(Got == null) { return null; }");
            W.WriteLine("        return Got.Create(Rotation);");
            W.WriteLine("    }");
            W.WriteLine("");
            //with rotation and position
            W.WriteLine("    ///<summary>");
            W.WriteLine("    /// Spawns an element from the pool that belongs to the given group");
            W.WriteLine("    ///</summary>");
            W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
            W.WriteLine("    /// <param name=\"Position\">The given location in the world where this instance will be placed at</param>");
            W.WriteLine("    /// <param name=\"Rotation\">The given rotation which this instance will have when spawned</param>");
            W.WriteLine("    public static GameObject Spawn(int ID, Vector3 Position, Quaternion Rotation)");
            W.WriteLine("    {");
            W.WriteLine("        PrefabContainer Got = Get(ID);");
            W.WriteLine("        if(Got == null) { return null; }");
            W.WriteLine("        return Got.Create(Position, Rotation);");
            W.WriteLine("    }");
            W.WriteLine("");

            //WITH PARENTS
            //PAT accessors
            //parameterless
            W.WriteLine("    ///<summary>");
            W.WriteLine("    /// Spawns an element from the pool that belongs to the given group as a child");
            W.WriteLine("    ///</summary>");
            W.WriteLine("    /// <param name=\"Parent\">The Transform that will contain the gameobject</param>");
            W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
            W.WriteLine("    public static GameObject SpawnWithParent(Transform Parent, int ID)");
            W.WriteLine("    {");
            W.WriteLine("        PrefabContainer Got = Get(ID);");
            W.WriteLine("        if(Got == null) { return null; }");
            W.WriteLine("        GameObject GO = Got.Create();");
            W.WriteLine("        GO.transform.parent = Parent;");
            W.WriteLine("        return GO;");
            W.WriteLine("    }");
            W.WriteLine("");
            //with position
            W.WriteLine("    ///<summary>");
            W.WriteLine("    /// Spawns an element from the pool that belongs to the given group as a child");
            W.WriteLine("    ///</summary>");
            W.WriteLine("    /// <param name=\"Parent\">The Transform that will contain the gameobject</param>");
            W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
            W.WriteLine("    /// <param name=\"Position\">The given location in the world where this instance will be placed at</param>");
            W.WriteLine("    public static GameObject SpawnWithParent(Transform Parent, int ID, Vector3 Position)");
            W.WriteLine("    {");
            W.WriteLine("        PrefabContainer Got = Get(ID);");
            W.WriteLine("        if(Got == null) { return null; }");
            W.WriteLine("        GameObject GO = Got.Create(Position);");
            W.WriteLine("        GO.transform.parent = Parent;");
            W.WriteLine("        return GO;");
            W.WriteLine("    }");
            W.WriteLine("");
            //with rotation
            W.WriteLine("    ///<summary>");
            W.WriteLine("    /// Spawns an element from the pool that belongs to the given group as a child");
            W.WriteLine("    ///</summary>");
            W.WriteLine("    /// <param name=\"Parent\">The Transform that will contain the gameobject</param>");
            W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
            W.WriteLine("    /// <param name=\"Rotation\">The given rotation which this instance will have when spawned</param>");
            W.WriteLine("    public static GameObject SpawnWithParent(Transform Parent, int ID, Quaternion Rotation)");
            W.WriteLine("    {");
            W.WriteLine("        PrefabContainer Got = Get(ID);");
            W.WriteLine("        if(Got == null) { return null; }");
            W.WriteLine("        GameObject GO = Got.Create(Rotation);");
            W.WriteLine("        GO.transform.parent = Parent;");
            W.WriteLine("        return GO;");
            W.WriteLine("    }");
            W.WriteLine("");
            //with rotation and position
            W.WriteLine("    ///<summary>");
            W.WriteLine("    /// Spawns an element from the pool that belongs to the given group as a child");
            W.WriteLine("    ///</summary>");
            W.WriteLine("    /// <param name=\"Parent\">The Transform that will contain the gameobject</param>");
            W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
            W.WriteLine("    /// <param name=\"Position\">The given location in the world where this instance will be placed at</param>");
            W.WriteLine("    /// <param name=\"Rotation\">The given rotation which this instance will have when spawned</param>");
            W.WriteLine("    public static GameObject SpawnWithParent(Transform Parent, int ID, Vector3 Position, Quaternion Rotation)");
            W.WriteLine("    {");
            W.WriteLine("        PrefabContainer Got = Get(ID);");
            W.WriteLine("        if(Got == null) { return null; }");
            W.WriteLine("        GameObject GO = Got.Create(Position, Rotation);");
            W.WriteLine("        GO.transform.parent = Parent;");
            W.WriteLine("        return GO;");
            W.WriteLine("    }");
            W.WriteLine("");

            //GENERICS
            W.WriteLine("    ///<summary>");
            W.WriteLine("    /// Spawns an element from the pool that belongs to the given group and inmediatly gets a component of type T, this component gets cached meaning that if you ask the pool for an element with the same component it will avoid calling GetComponent on it, if you instead ask for another type of component the system will cache that type of component and erase the previously cached.");
            W.WriteLine("    ///</summary>");
            W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
            W.WriteLine("    public static T Spawn<T>(int ID) where T: Component");
            W.WriteLine("    {");
            W.WriteLine("        PrefabContainer Got = Get(ID);");
            W.WriteLine("        if(Got == null) { return null; }");
            W.WriteLine("        return Got.Create<T>();");
            W.WriteLine("    }");
            W.WriteLine("");
            //with position
            W.WriteLine("    ///<summary>");
            W.WriteLine("    /// Spawns an element from the pool that belongs to the given group and inmediatly gets a component of type T, this component gets cached meaning that if you ask the pool for an element with the same component it will avoid calling GetComponent on it, if you instead ask for another type of component the system will cache that type of component and erase the previously cached.");
            W.WriteLine("    ///</summary>");
            W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
            W.WriteLine("    /// <param name=\"Position\">The given location in the world where this instance will be placed at</param>");
            W.WriteLine("    public static T Spawn<T>(int ID, Vector3 Position) where T: Component");
            W.WriteLine("    {");
            W.WriteLine("        PrefabContainer Got = Get(ID);");
            W.WriteLine("        if(Got == null) { return null; }");
            W.WriteLine("        return Got.Create<T>(Position);");
            W.WriteLine("    }");
            W.WriteLine("");
            //with rotation
            W.WriteLine("    ///<summary>");
            W.WriteLine("    /// Spawns an element from the pool that belongs to the given group and inmediatly gets a component of type T, this component gets cached meaning that if you ask the pool for an element with the same component it will avoid calling GetComponent on it, if you instead ask for another type of component the system will cache that type of component and erase the previously cached.");
            W.WriteLine("    ///</summary>");
            W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
            W.WriteLine("    /// <param name=\"Rotation\">The given rotation which this instance will have when spawned</param>");
            W.WriteLine("    public static T Spawn<T>(int ID, Quaternion Rotation) where T: Component");
            W.WriteLine("    {");
            W.WriteLine("        PrefabContainer Got = Get(ID);");
            W.WriteLine("        if(Got == null) { return null; }");
            W.WriteLine("        return Got.Create<T>(Rotation);");
            W.WriteLine("    }");
            W.WriteLine("");
            //with rotation and position
            W.WriteLine("    ///<summary>");
            W.WriteLine("    /// Spawns an element from the pool that belongs to the given group and inmediatly gets a component of type T, this component gets cached meaning that if you ask the pool for an element with the same component it will avoid calling GetComponent on it, if you instead ask for another type of component the system will cache that type of component and erase the previously cached.");
            W.WriteLine("    ///</summary>");
            W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
            W.WriteLine("    /// <param name=\"Position\">The given location in the world where this instance will be placed at</param>");
            W.WriteLine("    /// <param name=\"Rotation\">The given rotation which this instance will have when spawned</param>");
            W.WriteLine("    public static T Spawn<T>(int ID, Vector3 Position, Quaternion Rotation) where T: Component");
            W.WriteLine("    {");
            W.WriteLine("        PrefabContainer Got = Get(ID);");
            W.WriteLine("        if(Got == null) { return null; }");
            W.WriteLine("        return Got.Create<T>(Position, Rotation);");
            W.WriteLine("    }");
            W.WriteLine("");

            //Generics with parent
            //GENERICS
            W.WriteLine("    ///<summary>");
            W.WriteLine("    /// Spawns an element from the pool that belongs to the given group and inmediatly gets a component of type T, this component gets cached meaning that if you ask the pool for an element with the same component it will avoid calling GetComponent on it, if you instead ask for another type of component the system will cache that type of component and erase the previously cached.");
            W.WriteLine("    ///</summary>");
            W.WriteLine("    /// <param name=\"Parent\">The Transform that will contain the gameobject</param>");
            W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
            W.WriteLine("    public static T SpawnWithParent<T>(Transform Parent, int ID) where T: Component");
            W.WriteLine("    {");
            W.WriteLine("        PrefabContainer Got = Get(ID);");
            W.WriteLine("        if(Got == null) { return null; }");
            W.WriteLine("        T Go = Got.Create<T>();");
            W.WriteLine("        Go.transform.parent = Parent;");
            W.WriteLine("        return Go;");
            W.WriteLine("    }");
            W.WriteLine("");
            //with position
            W.WriteLine("    ///<summary>");
            W.WriteLine("    /// Spawns an element from the pool that belongs to the given group and inmediatly gets a component of type T, this component gets cached meaning that if you ask the pool for an element with the same component it will avoid calling GetComponent on it, if you instead ask for another type of component the system will cache that type of component and erase the previously cached.");
            W.WriteLine("    ///</summary>");
            W.WriteLine("    /// <param name=\"Parent\">The Transform that will contain the gameobject</param>");
            W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
            W.WriteLine("    /// <param name=\"Position\">The given location in the world where this instance will be placed at</param>");
            W.WriteLine("    public static T SpawnWithParent<T>(Transform Parent, int ID, Vector3 Position) where T: Component");
            W.WriteLine("    {");
            W.WriteLine("        PrefabContainer Got = Get(ID);");
            W.WriteLine("        if(Got == null) { return null; }");
            W.WriteLine("        T Go = Got.Create<T>(Position);");
            W.WriteLine("        Go.transform.parent = Parent;");
            W.WriteLine("        return Go;");
            W.WriteLine("    }");
            W.WriteLine("");
            //with rotation
            W.WriteLine("    ///<summary>");
            W.WriteLine("    /// Spawns an element from the pool that belongs to the given group and inmediatly gets a component of type T, this component gets cached meaning that if you ask the pool for an element with the same component it will avoid calling GetComponent on it, if you instead ask for another type of component the system will cache that type of component and erase the previously cached.");
            W.WriteLine("    ///</summary>");
            W.WriteLine("    /// <param name=\"Parent\">The Transform that will contain the gameobject</param>");
            W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
            W.WriteLine("    /// <param name=\"Rotation\">The given rotation which this instance will have when spawned</param>");
            W.WriteLine("    public static T SpawnWithParent<T>(Transform Parent, int ID, Quaternion Rotation) where T: Component");
            W.WriteLine("    {");
            W.WriteLine("        PrefabContainer Got = Get(ID);");
            W.WriteLine("        if(Got == null) { return null; }");
            W.WriteLine("        T Go = Got.Create<T>(Rotation);");
            W.WriteLine("        Go.transform.parent = Parent;");
            W.WriteLine("        return Go;");
            W.WriteLine("    }");
            W.WriteLine("");
            //with rotation and position
            W.WriteLine("    ///<summary>");
            W.WriteLine("    /// Spawns an element from the pool that belongs to the given group and inmediatly gets a component of type T, this component gets cached meaning that if you ask the pool for an element with the same component it will avoid calling GetComponent on it, if you instead ask for another type of component the system will cache that type of component and erase the previously cached.");
            W.WriteLine("    ///</summary>");
            W.WriteLine("    /// <param name=\"Parent\">The Transform that will contain the gameobject</param>");
            W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
            W.WriteLine("    /// <param name=\"Position\">The given location in the world where this instance will be placed at</param>");
            W.WriteLine("    /// <param name=\"Rotation\">The given rotation which this instance will have when spawned</param>");
            W.WriteLine("    public static T SpawnWithParent<T>(Transform Parent, int ID, Vector3 Position, Quaternion Rotation) where T: Component");
            W.WriteLine("    {");
            W.WriteLine("        PrefabContainer Got = Get(ID);");
            W.WriteLine("        if(Got == null) { return null; }");
            W.WriteLine("        T Go = Got.Create<T>(Position, Rotation);");
            W.WriteLine("        Go.transform.parent = Parent;");
            W.WriteLine("        return Go;");
            W.WriteLine("    }");
            W.WriteLine("");
            #endregion


            //Write the global PAT access
            W.WriteLine("    ///<summary>");
            W.WriteLine("    /// Gets the pool that the PAT describes.");
            W.WriteLine("    ///</summary>");
            W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
            W.WriteLine("    public static PrefabContainer Get(PAT ID)");
            W.WriteLine("    {");
            W.WriteLine("        if (UniqueInstance == null) { UniqueInstance = Instance; }");
            W.WriteLine("        int Val = ID.HashVal;");
            W.WriteLine("        switch(Val)");
            W.WriteLine("        {");
            if (!ContainsNone)
            {
                W.WriteLine("            case 0: return null;");
            }
            List<string> _AlreadyDefined = new List<string>();
            for (int i = 0; i < groups.Count; i++)
            {
                List<GameObject> prefabs = groups[i].Prefabs;
                int prefabCount = prefabs.Count;
                string GroupNameFixed = groups[i].GroupName;
                GroupNameFixed = GroupNameFixed.Replace(' ', '_');
                for (int a = 0; a < prefabCount; a++)
                {
                    GameObject CurrentPrefab = prefabs[a];
                    int Hash = CurrentPrefab.name.GetHashCode();
                    string prefabName = CurrentPrefab.name;
                    if (_AlreadyDefined.Contains(prefabName)) { continue; }
                    _AlreadyDefined.Add(prefabName);
                    prefabName = prefabName.Replace(' ', '_');
                    W.WriteLine("            case " + Hash.ToString() + ": ");
                    W.WriteLine("               if(UniqueInstance." + GroupNameFixed + "_" + prefabName + "_Container == null){ UniqueInstance." + GroupNameFixed + "_" + prefabName + "_Container = UniqueInstance.AllPrefabContainers[Val];}");
                    W.WriteLine("               return UniqueInstance." + GroupNameFixed + "_" + prefabName + "_Container;");
                }
            }
            W.WriteLine("        }");
            W.WriteLine("        return Instance.AllPrefabContainers[Val];");
            W.WriteLine("    }");



            //Write int generic global access
            W.WriteLine("    ///<summary>");
            W.WriteLine("    /// Gets the pool that the INT describes.");
            W.WriteLine("    ///</summary>");
            W.WriteLine("    /// <param name=\"ID\">The ID that determines which pool we accessing</param>");
            W.WriteLine("    public static PrefabContainer Get(int ID)");
            W.WriteLine("    {");
            W.WriteLine("        if (UniqueInstance == null) { UniqueInstance = Instance; }");
            W.WriteLine("        int Val = ID;");
            W.WriteLine("        switch(Val)");
            W.WriteLine("        {");
            if (!ContainsNone)
            {
                W.WriteLine("            case 0: return null;");
            }
            _AlreadyDefined.Clear();
            for (int i = 0; i < groups.Count; i++)
            {
                List<GameObject> prefabs = groups[i].Prefabs;
                int prefabCount = prefabs.Count;
                string GroupNameFixed = groups[i].GroupName;
                GroupNameFixed = GroupNameFixed.Replace(' ', '_');
                for (int a = 0; a < prefabCount; a++)
                {
                    GameObject CurrentPrefab = prefabs[a];
                    int Hash = CurrentPrefab.name.GetHashCode();
                    string prefabName = CurrentPrefab.name;
                    if (_AlreadyDefined.Contains(prefabName)) { continue; }
                    _AlreadyDefined.Add(prefabName);
                    prefabName = prefabName.Replace(' ', '_');
                    W.WriteLine("            case " + Hash.ToString() + ": ");
                    W.WriteLine("               if(UniqueInstance." + GroupNameFixed + "_" + prefabName + "_Container == null){ UniqueInstance." + GroupNameFixed + "_" + prefabName + "_Container = UniqueInstance.AllPrefabContainers[Val];}");
                    W.WriteLine("               return UniqueInstance." + GroupNameFixed + "_" + prefabName + "_Container;");
                }
            }
            W.WriteLine("        }");
            W.WriteLine("        return Instance.AllPrefabContainers[Val];");
            W.WriteLine("    }");

            #region GroupFuntions
            //REGULAR
            W.WriteLine("    ///<summary>");
            W.WriteLine("    /// Spawns an element from the group randomly (use the static class \"PAGroups\" to access constant IDs.");
            W.WriteLine("    ///</summary>");
            W.WriteLine("    /// <param name=\"ID\">The ID of the group, use PAGroups static class for easy type safe strings</param>");
            W.WriteLine("    public static GameObject SpawnRandom(string ID)");
            W.WriteLine("    {");
            W.WriteLine("        List<PrefabAtlasGroup> AllGroups = GetGroups();");
            W.WriteLine("        int GroupsTotalCount = AllGroups.Count;");
            W.WriteLine("        for(int i = 0; i < GroupsTotalCount; i++)");
            W.WriteLine("        {");
            W.WriteLine("            if(AllGroups[i].GroupName == ID)");
            W.WriteLine("            {");
            W.WriteLine("                int Random = AllGroups[i].Hashes[(int)UnityEngine.Random.Range(0, AllGroups[i].Hashes.Count - 1)];");
            W.WriteLine("                return PrefabAtlas.Spawn(Random);");
            W.WriteLine("            }");
            W.WriteLine("        }");
            W.WriteLine("        return null;");
            W.WriteLine("    }");

            W.WriteLine("    ///<summary>");
            W.WriteLine("    /// Spawns an element from the group randomly (use the static class \"PAGroups\" to access constant IDs.");
            W.WriteLine("    ///</summary>");
            W.WriteLine("    /// <param name=\"ID\">The ID of the group, use PAGroups static class for easy type safe strings</param>");
            W.WriteLine("    public static GameObject SpawnRandom(string ID, Vector3 Position)");
            W.WriteLine("    {");
            W.WriteLine("        List<PrefabAtlasGroup> AllGroups = GetGroups();");
            W.WriteLine("        int GroupsTotalCount = AllGroups.Count;");
            W.WriteLine("        for(int i = 0; i < GroupsTotalCount; i++)");
            W.WriteLine("        {");
            W.WriteLine("            if(AllGroups[i].GroupName == ID)");
            W.WriteLine("            {");
            W.WriteLine("                int Random = AllGroups[i].Hashes[(int)UnityEngine.Random.Range(0, AllGroups[i].Hashes.Count - 1)];");
            W.WriteLine("                return PrefabAtlas.Spawn(Random, Position);");
            W.WriteLine("            }");
            W.WriteLine("        }");
            W.WriteLine("        return null;");
            W.WriteLine("    }");

            //WITH PARENT
            W.WriteLine("    ///<summary>");
            W.WriteLine("    /// Spawns an element from the group randomly (use the static class \"PAGroups\" to access constant IDs.");
            W.WriteLine("    ///</summary>");
            W.WriteLine("    /// <param name=\"ID\">The ID of the group, use PAGroups static class for easy type safe strings</param>");
            W.WriteLine("    public static GameObject SpawnRandomWithParent(Transform Parent, string ID)");
            W.WriteLine("    {");
            W.WriteLine("        List<PrefabAtlasGroup> AllGroups = GetGroups();");
            W.WriteLine("        int GroupsTotalCount = AllGroups.Count;");
            W.WriteLine("        for(int i = 0; i < GroupsTotalCount; i++)");
            W.WriteLine("        {");
            W.WriteLine("            if(AllGroups[i].GroupName == ID)");
            W.WriteLine("            {");
            W.WriteLine("                int Random = AllGroups[i].Hashes[(int)UnityEngine.Random.Range(0, AllGroups[i].Hashes.Count - 1)];");
            W.WriteLine("                return PrefabAtlas.SpawnWithParent(Parent, Random);");
            W.WriteLine("            }");
            W.WriteLine("        }");
            W.WriteLine("        return null;");
            W.WriteLine("    }");

            W.WriteLine("    ///<summary>");
            W.WriteLine("    /// Spawns an element from the group randomly (use the static class \"PAGroups\" to access constant IDs.");
            W.WriteLine("    ///</summary>");
            W.WriteLine("    /// <param name=\"ID\">The ID of the group, use PAGroups static class for easy type safe strings</param>");
            W.WriteLine("    public static GameObject SpawnRandomWithParent(Transform Parent, string ID, Vector3 Position)");
            W.WriteLine("    {");
            W.WriteLine("        List<PrefabAtlasGroup> AllGroups = GetGroups();");
            W.WriteLine("        int GroupsTotalCount = AllGroups.Count;");
            W.WriteLine("        for(int i = 0; i < GroupsTotalCount; i++)");
            W.WriteLine("        {");
            W.WriteLine("            if(AllGroups[i].GroupName == ID)");
            W.WriteLine("            {");
            W.WriteLine("                int Random = AllGroups[i].Hashes[(int)UnityEngine.Random.Range(0, AllGroups[i].Hashes.Count - 1)];");
            W.WriteLine("                return PrefabAtlas.SpawnWithParent(Parent, Random, Position);");
            W.WriteLine("            }");
            W.WriteLine("        }");
            W.WriteLine("        return null;");
            W.WriteLine("    }");

            #endregion


            W.WriteLine("}");
            #endregion
            #region Groups enums
            W.WriteLine("public static class PAGroups");
            W.WriteLine("{");
            int IndexUnmodified = 0;
            foreach (string IDGroup in _GroupsIDS)
            {
                W.WriteLine("    public const string " + IDGroup + " = \"" + _UnmodifiedGroupsID[IndexUnmodified] + "\";");
                IndexUnmodified++;
            }
            W.WriteLine("}");
            #endregion
            #region Bunches
            List<PABunch> _AllBunches = Atlas.Bunches;
            //W.WriteLine("public partial class PrefabAtlas");
            //W.WriteLine("{");
            W.WriteLine("   public partial class PABunch");
            W.WriteLine("   {");
            for (int i = 0; i < _AllBunches.Count; i++)
            {
                W.WriteLine("       public enum " + _AllBunches[i]._ID);
                W.WriteLine("       {");
                List<int> _alreadyDefinedHashes = new List<int>();
                _alreadyDefinedHashes.Add(0);
                for (int a = 0; a < _AllBunches[i]._SelectedItems.Count; a++)
                {
                    PAT CurrPAT = _AllBunches[i]._SelectedItems[a];
                    if (!CurrPAT.IsValid() || _alreadyDefinedHashes.Contains(CurrPAT.HashVal)) { continue; }
                    _alreadyDefinedHashes.Add(CurrPAT.HashVal);
                    W.WriteLine("           " + CurrPAT.enumType.GetPrefabNameWithHash(CurrPAT.HashVal) + " = " + CurrPAT.HashVal.ToString() + ",");
                }
                W.WriteLine("       }");
                if (_AllBunches[i]._SelectedItems.Count > 0)
                {
                    W.WriteLine("       public static class " + _AllBunches[i]._ID + "Class");
                    W.WriteLine("       {");
                    W.WriteLine("           public static bool ContainsID(int ID) { for (int i = 0; i < Hashes.Length; i++) { if (Hashes[i] == ID) { return true; } } return false; }");
                    W.WriteLine("           private static int[] Hashes = {");
                    int Minus = 0;
                    for (int w = 0; w < _AllBunches[i]._SelectedItems.Count; w++)
                    {
                        if (!_AllBunches[i]._SelectedItems[w].IsValid()) { Minus++; continue; }
                        W.WriteLine("           " + _AllBunches[i]._SelectedItems[w].HashVal.ToString() + ",");
                    }
                    W.WriteLine("           };");
                    if (_AllBunches[i]._SelectedItems.Count - Minus > 0)
                    {
                        W.WriteLine("           public static int GetRandom(){ return Hashes[PrefabAtlas.RandomRange(0," + _AllBunches[i]._SelectedItems.Count.ToString() + ")]; }");
                    }
                    W.WriteLine("       }");
                }

            }
            W.WriteLine("   }");
            //W.WriteLine("}");
            #endregion

            W.Flush();
            W.Close();
            AssetDatabase.Refresh();
        }
        #endregion
    }
    

    #region Getting elements from the user
    public static T GetFirstPrefabOfType<T>() where T : MonoBehaviour
    {
        DirectoryInfo directory = new DirectoryInfo(Application.dataPath);
        FileInfo[] goFileInfo = directory.GetFiles("PrefabAtlas" + ".prefab", SearchOption.AllDirectories);

        int i = 0; int goFileInfoLength = goFileInfo.Length;
        FileInfo tempGoFileInfo; string tempFilePath;
        GameObject tempGO;
        for (; i < goFileInfoLength; i++)
        {
            tempGoFileInfo = goFileInfo[i];
            if (tempGoFileInfo == null)
                continue;

            tempFilePath = tempGoFileInfo.FullName;
            tempFilePath = tempFilePath.Replace(@"\", "/").Replace(Application.dataPath, "Assets");
            tempGO = AssetDatabase.LoadAssetAtPath(tempFilePath, typeof(System.Object)) as GameObject;
            if (tempGO == null)
            {
                continue;
            }
            T Compo = tempGO.GetComponent<T>();
            if (Compo != null)
            {
                return Compo;
            }
            Compo = tempGO.GetComponentInChildren<T>();
            if (Compo != null)
            {
                return Compo;
            }

        }
        return null;

    }

    public static string FindTypeDirectory(Type theType)
    {
        string fileExtension = ".cs";
        DirectoryInfo TheDirectory = new DirectoryInfo(Application.dataPath);
        FileInfo[] TheFileInfo = TheDirectory.GetFiles("*" + fileExtension, SearchOption.AllDirectories);
        int i = 0; int FileInfoLength = TheFileInfo.Length;
        FileInfo TempFileInfo; string tempFilePath;
        for (; i < FileInfoLength; i++)
        {
            TempFileInfo = TheFileInfo[i];
            if (TempFileInfo == null)
                continue;

            tempFilePath = TempFileInfo.FullName;
            tempFilePath = tempFilePath.Replace(@"\", "/").Replace(Application.dataPath, "");

            char[] delimiters = { '.', '/' };
            string[] FindAction = tempFilePath.Split(delimiters);
            int countSplits = FindAction.Length;
            string ClassName = FindAction[countSplits - 2];
            Type cachedType = GetType(ClassName);
            if (cachedType != null)
            {
                if (cachedType == theType)
                {
                    tempFilePath = tempFilePath.Replace(ClassName + ".cs", "");
                    return tempFilePath;
                }
            }
        }

        return string.Empty;
    }

    private static Type GetType(string typeName)
    {
        var type = Type.GetType(typeName);
        if (type != null) return type;
        foreach (var a in AppDomain.CurrentDomain.GetAssemblies())
        {
            type = a.GetType(typeName);
            if (type != null)
                return type;
        }
        return null;
    }
    #endregion
}

