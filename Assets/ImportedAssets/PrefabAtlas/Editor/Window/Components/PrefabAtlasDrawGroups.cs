﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using PrefabAtlasHelpers;
using PrefabAtlasElements;
using UnityEditorInternal;
using System.Linq;
using System.Reflection;

[Serializable]
public class PAElements
{
    private int SelectedGroupIndex = -1;
    private int PreviouslySelectedGroupIndex = -1;
    private int SelectedBunchIndex = -1;

    private List<Rect> GroupSelections = new List<Rect>();
    private List<Rect> PrefabSelections = new List<Rect>();
    private Vector2 GroupScrollViewPos = Vector2.zero;
    private Vector2 BunchesScrollViewPos = Vector2.zero;
    private Vector2 BunchesInspectorScrollViewPos = Vector2.zero;
    private Vector2 EntryScrollViewPos = Vector2.zero;
    private Vector2 SceneObjectScrollViewPos = Vector2.zero;

    private const float HeaderHeight = 24.0f;
    private const float EntryHeight = 14.0f;
    private const float EntrySeparation = 20.0f;
    private const int HeaderFontSize = 14;
    private const int TextFontSize = 14;

    private PrefabAtlasSceneObject SceneObject;

    static GUISkin CachedSkin = null;
    public static GUISkin DefaultSkin;
    private static GUISkin Skin { get { if (CachedSkin == null) { CachedSkin = Resources.Load<GUISkin>("PAtlasSkin"); } return CachedSkin; } }
    private ReorderableList GroupList;
    private ReorderableList PrefabList;
    private ReorderableList BunchList;
    private ReorderableList BunchesInspectorList;

    #region Rendering
    public void RenderGroups(PrefabAtlasWindow Window, Rect Size)
    {
        RenderHelpers.RenderBox(Size, ColorHelper._Black_20);
        Size.y -= 2;
        RenderHelpers.RenderBox(new Rect(Size.x, Size.y, Size.width, 2.0f), ColorHelper._Blue_BhvrTool);
        Size.y += 2;
        GUI.skin = Skin;
        GUI.skin.verticalScrollbar = GUIStyle.none;
        PrefabAtlas Data = Window._Data;
        if (Data == null)
        {
            return;
        }
        //Render the BG
        GroupSelections.Clear();
        PrefabSelections.Clear();
        List<PrefabAtlasGroup> Groups = Data.Groups;
        GUILayout.BeginArea(Size);
        LayoutHelpers.Horizontal(() =>
        {
            GUILayout.Space(5.0f);
            GUILayout.Label(new GUIContent("GROUPS"));
        });
        if (GroupList == null)
        {
            GroupList = new ReorderableList(Groups, typeof(PrefabAtlasGroup), true, false, true, true);
            GroupList.drawHeaderCallback = (x) =>
            {

            };
            GroupList.onAddCallback = (x) =>
            {
                GameObject instantiated = (GameObject)PrefabUtility.InstantiatePrefab(Data.gameObject);
                PrefabUtility.UnpackPrefabInstance(instantiated, PrefabUnpackMode.Completely, InteractionMode.AutomatedAction);
                GameObject NewAtlas = new GameObject("NewAtlas");
                NewAtlas.transform.parent = instantiated.transform;
                instantiated.GetComponent<PrefabAtlas>().Groups.Add(NewAtlas.AddComponent<PrefabAtlasGroup>());
                PrefabUtility.SaveAsPrefabAssetAndConnect(instantiated, "Assets" + PrefabAtlasWindow.FindTypeDirectory(typeof(PrefabAtlas)) + "PrefabAtlas.prefab", InteractionMode.AutomatedAction);
                GameObject.DestroyImmediate(instantiated);
                EditorUtility.SetDirty(Data.gameObject);
                VersionUp();
            };
            GroupList.onRemoveCallback = (x) =>
            {
                int i = x.index;
                if (EditorUtility.DisplayDialog("Deleting group: " + Groups[i].GroupName,
                  "Are you sure you want to delete this group? this action cant be reverted", "Yes", "Cancel"))
                {
                    GameObject instantiated = (GameObject)PrefabUtility.InstantiatePrefab(Data.gameObject);
                    PrefabAtlas temp = instantiated.GetComponent<PrefabAtlas>();
                    PrefabUtility.UnpackPrefabInstance(instantiated, PrefabUnpackMode.Completely, InteractionMode.AutomatedAction);
                    GameObject.DestroyImmediate(temp.Groups[i].gameObject);
                    temp.Groups.RemoveAt(i);
                    GameObject resulting = PrefabUtility.SaveAsPrefabAssetAndConnect(instantiated, "Assets" + PrefabAtlasWindow.FindTypeDirectory(typeof(PrefabAtlas)) + "PrefabAtlas.prefab", InteractionMode.AutomatedAction);
                    EditorUtility.SetDirty(resulting);
                    GameObject.DestroyImmediate(instantiated);
                    Window.Focus();
                    SelectedGroupIndex = -1;
                    VersionUp();
                }
            };
            GroupList.onSelectCallback = (x) =>
            {
                PreviouslySelectedGroupIndex = SelectedGroupIndex;
                SelectedGroupIndex = x.index;
            };
            GroupList.drawElementCallback = (Rect rect, int i, bool isActive, bool isFocused) =>
            {
                Groups[i].GroupName = GUI.TextField(new Rect(rect.x, rect.y + 3.0f, rect.width, rect.height), Groups[i].GroupName, GUIStyle.none);
                if (GUI.changed)
                {
                    Groups[i].gameObject.name = Groups[i].GroupName;
                }
                DropAreaGUI(Groups[i], rect);
            };
            GroupList.onReorderCallback = (x) =>
            {
                VersionUp();
            };
        }
        GroupScrollViewPos = GUILayout.BeginScrollView(GroupScrollViewPos, GUI.skin.verticalScrollbar);
        GUI.skin = DefaultSkin;
        GroupList.headerHeight = 0;
        GroupList.DoLayoutList();
        GUI.skin = Skin;
        GUILayout.Space(40.0f);
        GUILayout.EndScrollView();
        GUILayout.EndArea();
    }
    public void RenderEntries(PrefabAtlasWindow Window, Rect Size)
    {
        RenderHelpers.RenderBox(Size, ColorHelper._Black_20);
        Size.y -= 2;
        RenderHelpers.RenderBox(new Rect(Size.x, Size.y, Size.width, 2.0f), ColorHelper._Blue_BhvrTool);
        Size.y += 2;
        PrefabAtlas Data = Window._Data;
        GUI.skin = Skin;
        GUI.skin.verticalScrollbar = GUIStyle.none;
        if (Data == null || SelectedGroupIndex == -1 || SelectedGroupIndex >= Data.Groups.Count)
        {
            GUILayout.BeginArea(Size);
            LayoutHelpers.Horizontal(() =>
            {
                GUILayout.Space(5.0f);
                GUILayout.Label(new GUIContent("PREFABS: no group selected"));
            });
            GUILayout.EndArea();
            return;
        }

        //create the list
        if (PrefabList == null || SelectedGroupIndex != PreviouslySelectedGroupIndex)
        {
            List<GameObject> Prefbs = Data.Groups[SelectedGroupIndex].Prefabs;
            PreviouslySelectedGroupIndex = SelectedGroupIndex;
            PrefabList = new ReorderableList(Prefbs, typeof(GameObject), true, false, false, true);
            PrefabList.onRemoveCallback = (x) =>
            {
                int i = x.index;
                Prefbs.RemoveAt(i);
                VersionUp();
            };
            PrefabList.onSelectCallback = (x) =>
            {
                Selection.activeGameObject = Prefbs[x.index];
            };
            PrefabList.drawElementCallback = (Rect rect, int i, bool isActive, bool isFocused) =>
            {
                if (Prefbs[i] != null)
                {
                    GUI.Label(rect, new GUIContent(Prefbs[i].name), GUIStyle.none);
                }
            };
            PrefabList.onReorderCallback = (x) =>
            {
                VersionUp();
            };
        }
        //end create list
        GUILayout.BeginArea(Size);
        LayoutHelpers.Horizontal(() =>
        {
            GUILayout.Space(5.0f);
            GUILayout.Label(new GUIContent("PREFABS: " + Data.Groups[SelectedGroupIndex].GroupName));
        });
        EntryScrollViewPos = GUILayout.BeginScrollView(EntryScrollViewPos, GUI.skin.verticalScrollbar);
        GUI.skin = null;
        PrefabList.headerHeight = 0;
        PrefabList.DoLayoutList();
        GUI.skin = Skin;
        GUILayout.Space(40.0f); //fix for the layout not taking in consideration the add and remove buttons of the reordable list
        GUILayout.EndScrollView();
        GUILayout.EndArea();
    }
    public void RenderBunches(PrefabAtlasWindow Window, Rect Size)
    {
        GUI.skin = Skin;
        GUI.skin.verticalScrollbar = GUIStyle.none;
        PrefabAtlas Data = Window._Data;
        if (Data == null)
        {
            return;
        }
        List<PABunch> Bunches = Data.Bunches;
        GUILayout.BeginArea(Size);
        if (BunchList == null)
        {
            BunchList = new ReorderableList(Bunches, typeof(PABunch), true, false, true, true);
            BunchList.drawHeaderCallback = (x) =>
            {

            };
            BunchList.onAddCallback = (x) =>
            {
                //prefabs need to be instantiated and then replaced in order to fool unity systems
                GameObject instantiated = (GameObject)PrefabUtility.InstantiatePrefab(Data.gameObject);
                GameObject NewBunch = new GameObject("NewBunch");
                NewBunch.transform.parent = instantiated.transform;
                instantiated.GetComponent<PrefabAtlas>().Bunches.Add(NewBunch.AddComponent<PABunch>());
                PrefabUtility.ReplacePrefab(instantiated, Data.gameObject);
                GameObject.DestroyImmediate(instantiated);
                EditorUtility.SetDirty(Data.gameObject);
                VersionUp();
            };
            BunchList.onRemoveCallback = (x) =>
            {
                int i = x.index;
                if (EditorUtility.DisplayDialog("Deleting Bunch: " + Bunches[i]._ID,
                  "Are you sure you want to delete this Bunch? this action cant be reverted", "Yes", "Cancel"))
                {
                    GameObject instantiated = (GameObject)PrefabUtility.InstantiatePrefab(Data.gameObject);
                    PrefabAtlas temp = instantiated.GetComponent<PrefabAtlas>();
                    GameObject.DestroyImmediate(temp.Bunches[i].gameObject);
                    temp.Bunches.RemoveAt(i);
                    GameObject resulting = PrefabUtility.ReplacePrefab(instantiated, Data.gameObject);
                    EditorUtility.SetDirty(resulting);
                    GameObject.DestroyImmediate(instantiated);
                    Window.Focus();
                    SelectedGroupIndex = -1;
                    VersionUp();
                }
            };
            BunchList.onSelectCallback = (x) =>
            {
                SelectedBunchIndex = x.index;
                BunchesInspectorList = null;
            };
            BunchList.drawElementCallback = (Rect rect, int i, bool isActive, bool isFocused) =>
            {
                Bunches[i]._ID = GUI.TextField(new Rect(rect.x, rect.y + 3.0f, rect.width, rect.height), Bunches[i]._ID, GUIStyle.none);
                if (GUI.changed)
                {
                    Bunches[i].gameObject.name = Bunches[i]._ID;
                }
                //DropAreaGUI(Groups[i], rect);
            };
            BunchList.onReorderCallback = (x) =>
            {
                VersionUp();
            };
        }
        BunchesScrollViewPos = GUILayout.BeginScrollView(BunchesScrollViewPos, GUI.skin.verticalScrollbar);
        GUI.skin = DefaultSkin;
        BunchList.headerHeight = 0;
        BunchList.DoLayoutList();
        GUI.skin = Skin;
        GUILayout.Space(40.0f); //fix for the layout not taking in consideration the add and remove buttons of the reordable list
        GUILayout.EndScrollView();
        GUILayout.EndArea();
    }
    public void BunchesInspector(PrefabAtlasWindow Window, Rect Size)
    {
        PrefabAtlas Data = Window._Data;
        if (Data == null)
        {
            return;
        }
        List<PABunch> Bunches = Data.Bunches;
        if (SelectedBunchIndex != -1 && SelectedBunchIndex >= 0 && Bunches.Count > SelectedBunchIndex)
        {
            GUILayout.BeginArea(Size);
            PABunch CurrentBunch = Bunches[SelectedBunchIndex];
            SerializedObject Obj = new SerializedObject(CurrentBunch);
            Obj.Update();
            if (BunchesInspectorList == null)
            {

                //BunchesInspectorList = new ReorderableList(Obj, Obj.FindProperty("_SelectedItems"), true, true, true, true);
                BunchesInspectorList = new ReorderableList(CurrentBunch._SelectedItems, typeof(PAT), true, true, true, true);
                BunchesInspectorList.drawHeaderCallback = (x) =>
                {
                    GUI.Label(x, new GUIContent(CurrentBunch._ID));
                };

                BunchesInspectorList.onAddCallback = (x) =>
                {
                    CurrentBunch._SelectedItems.Add(new PAT());
                    VersionUp();
                };

                BunchesInspectorList.onRemoveCallback = (x) =>
                {
                    int i = x.index;
                    CurrentBunch._SelectedItems.RemoveAt(i);
                    Obj.Update();
                    Obj.ApplyModifiedProperties();
                };

                BunchesInspectorList.drawElementCallback = (Rect rect, int i, bool isActive, bool isFocused) =>
                {
                    Obj.Update();
                    GlobalPAT(Obj.FindProperty("_SelectedItems").GetArrayElementAtIndex(i), "", rect);
                    Obj.ApplyModifiedProperties();
                };

                BunchesInspectorList.onReorderCallback = (x) =>
                {
                    VersionUp();
                };
            }
            BunchesInspectorScrollViewPos = GUILayout.BeginScrollView(BunchesInspectorScrollViewPos, GUI.skin.verticalScrollbar);
            GUI.skin = DefaultSkin;
            //BunchesInspectorList.headerHeight = 0;
            BunchesInspectorList.DoLayoutList();
            Obj.ApplyModifiedProperties();
            GUI.skin = Skin;
            GUILayout.Space(40.0f); //fix for the layout not taking in consideration the add and remove buttons of the reordable list
            GUILayout.EndScrollView();
            GUILayout.EndArea();
        }

    }
    public void RenderGroupsOptions(PrefabAtlasWindow Window, Rect Size)
    {
        RenderHelpers.RenderBox(Size, ColorHelper._Black_20);
        Size.y -= 2;
        RenderHelpers.RenderBox(new Rect(Size.x, Size.y, Size.width, 2.0f), ColorHelper._Blue_BhvrTool);
       
        GUILayout.BeginArea(Size);
        GUILayout.Space(15.0f);
        if (SelectedGroupIndex != -1)
        {
            PrefabAtlas Data = Window._Data;
            if (GUILayout.Button(new GUIContent("Apply to group: "+ Data.Groups[SelectedGroupIndex].GroupName)))
            {

                //debug 
                GenericMenu Menu = new GenericMenu();
               
               
                //foreach (var a in AppDomain.CurrentDomain.GetAssemblies())
                {
                    Add(Menu, Assembly.GetAssembly(typeof(PAModHolder)), Data.Groups[SelectedGroupIndex]);
                    AddGroup(Menu, Assembly.GetAssembly(typeof(PAGroupModHolder)), Data.Groups[SelectedGroupIndex]);
                }

                Menu.ShowAsContext();

            }
        }
        GUILayout.EndArea();
    }
    private void Add(GenericMenu Menu, Assembly assembly, PrefabAtlasGroup Group)
    {
        var methods = assembly.GetTypes()
                      .SelectMany(t => t.GetMethods())
                      .Where(m => m.GetCustomAttributes(typeof(PAModHolder), false).Length > 0)
                      .ToArray();
        foreach (MethodInfo m in methods)
        {
            PAModHolder Att = null;
            object[] All = m.GetCustomAttributes(true);
            foreach(PAModHolder h in All)
            {
                Att = h;
            }
            if(Att == null)
            {
                //this should really never ever happen...
                Att = new PAModHolder();
            }
            Menu.AddItem(new GUIContent(Att._ID == string.Empty ? "Uncategorized/"+m.Name : Att._ID+"/"+m.Name), false, (x) =>
            {
                object[] Got = (object[])x;
                MethodInfo Final = (MethodInfo)Got[0];
                PrefabAtlasGroup FoundGround = (PrefabAtlasGroup)Got[1];
                if (EditorUtility.DisplayDialog("Apply Function", "Going to apply a function (" + Final.Name + ") to the group (" + FoundGround.GroupName + "), are you sure?", "Yes", "Cancel"))
                {
                    foreach (GameObject Print in FoundGround.Prefabs)
                    {
                        Final.Invoke(null, new object[] { Print });
                    }
                }
            }, new object[2] { m, Group });
        }
    }
    private void AddGroup(GenericMenu Menu, Assembly assembly, PrefabAtlasGroup Group)
    {
        var methods = assembly.GetTypes()
                      .SelectMany(t => t.GetMethods())
                      .Where(m => m.GetCustomAttributes(typeof(PAGroupModHolder), false).Length > 0)
                      .ToArray();
        foreach (MethodInfo m in methods)
        {
            PAGroupModHolder Att = null;
            object[] All = m.GetCustomAttributes(true);
            foreach (PAGroupModHolder h in All)
            {
                Att = h;
            }
            if (Att == null)
            {
                //this should really never ever happen...
                Att = new PAGroupModHolder();
            }
            Menu.AddItem(new GUIContent(Att._ID == string.Empty ? "Uncategorized/" + m.Name : Att._ID + "/" + m.Name), false, (x) =>
            {
                object[] Got = (object[])x;
                MethodInfo Final = (MethodInfo)Got[0];
                PrefabAtlasGroup FoundGround = (PrefabAtlasGroup)Got[1];
                if (EditorUtility.DisplayDialog("Apply Function", "Going to apply a function (" + Final.Name + ") to the group (" + FoundGround.GroupName + "), are you sure?", "Yes", "Cancel"))
                {
                    //foreach (GameObject Print in FoundGround.Prefabs)
                    {
                        Final.Invoke(null, new object[] { FoundGround });
                    }
                }
            }, new object[2] { m, Group });
        }
    }
    #region Globat pat for bunches
    public static void GlobalPAT(SerializedProperty prop, string ID, Rect position)
    {
        if (prop == null)
        {
            return;
        }
        //EditorGUILayout.LabelField("");
        //Rect position = GUILayoutUtility.GetLastRect();
        SerializedProperty Group = prop.FindPropertyRelative("enumType");
        PrefabAtlasGroup Current = PrefabAtlasEditor.GetEnumPrefab((PrefabAtlasGroup)Group.objectReferenceValue, position, "");
        Group.objectReferenceValue = Current;

        if (Current != null)
        {
            string name = Current.GroupName + "Tag";
            SerializedProperty Val = prop.FindPropertyRelative("HashVal");
            PrefabAtlasEditor.DrawEnumToInt(position, Val.intValue, GetType(name), new GUIContent(ID), Val, prop.FindPropertyRelative("SearchHelper"));
        }
        else
        {
            SerializedProperty Val = prop.FindPropertyRelative("HashVal");
            Val.intValue = 0;
        }
        if (GUI.changed)
        {
            prop.serializedObject.ApplyModifiedProperties();
        }
    }
    private static Type GetType(string typeName)
    {
        var type = Type.GetType(typeName);
        if (type != null) return type;
        foreach (var a in AppDomain.CurrentDomain.GetAssemblies())
        {
            type = a.GetType(typeName);
            if (type != null)
                return type;
        }
        return null;
    }
    #endregion
    private PrefabAtlas ScenePrefabAtlas = null;
    public void RenderInspector(PrefabAtlasWindow Window, Rect Size)
    {
        RenderHelpers.RenderBox(Size, ColorHelper._Black_20);
        PrefabAtlas Data = Window._Data;
        if (Data == null)
        {
            return;
        }
        Size.y -= 2;
        RenderHelpers.RenderBox(new Rect(Size.x, Size.y, Size.width, 2.0f), ColorHelper._Blue_BhvrTool);
        Size.y += 2;
        if (SceneObject == null)
        {
            SceneObject = GameObject.FindObjectOfType<PrefabAtlasSceneObject>();
        }
        #region Runtime statistics
        if (Application.isPlaying)
        {
            //LabelHelper.TextAt(new Rect(Size.x + 5.0f, Size.y - 4.0f, Size.width, HeaderHeight), HeaderFontSize, "STATISTICS", "Fonts/Titil", ColorHelper._White_200, TextAnchor.MiddleLeft);
            //try to get the prefab atlas from the Scene
            if (ScenePrefabAtlas == null)
            {
                ScenePrefabAtlas = GameObject.FindObjectOfType<PrefabAtlas>();
            }
            if (ScenePrefabAtlas == null)
            {
                RenderHelpers.RenderBorders(Size, ColorHelper._Blue_BhvrTool, 2.0f);
                return;
            }
            Dictionary<int, PrefabContainer> ElementsToInspect = ScenePrefabAtlas.AllPrefabContainers;
            Rect EntryLabel = new Rect(Size.x, Size.y + HeaderHeight, Size.width, EntryHeight);

            GUILayout.BeginArea(Size);
            GUILayout.Label(new GUIContent("STATISTICS"));
            SceneObjectScrollViewPos = GUILayout.BeginScrollView(SceneObjectScrollViewPos, GUIStyle.none);
            GUILayout.Space(5.0f);
            foreach (PrefabContainer container in ElementsToInspect.Values)
            {
                if (container == null)
                {
                    continue;
                }
                if (container.Alive <= 0)
                {
                    continue;
                }
                GUILayout.Label("");
                EntryLabel = GUILayoutUtility.GetLastRect();
                EntryLabel.xMin += 2.0f;
                EntryLabel.xMax -= 2.0f;
                RenderHelpers.RenderBox(EntryLabel, ColorHelper._Black_50);
                EntryLabel.xMin += 5.0f;
                GUI.Label(EntryLabel, new GUIContent(container.PrefabPrint.name + ": " + container.Alive.ToString() + "/" + container.Count.ToString()));
                GUILayout.Space(1.0f);
            }
            GUILayout.Label("");
            GUILayout.Label("");
            GUILayout.EndScrollView();
            GUILayout.EndArea();
            RenderHelpers.RenderBorders(Size, ColorHelper._Blue_BhvrTool, 2.0f);
            return;
        }
        #endregion

        GUI.skin = Skin;
        GUI.skin.verticalScrollbar = GUIStyle.none;
        GUILayout.BeginArea(Size);
        LayoutHelpers.Horizontal(() =>
        {
            GUILayout.Space(5.0f);
            GUILayout.Label(new GUIContent("SCENE OBJECT SETUP"));
        });
        GUILayout.EndArea();

        //LabelHelper.TextAt(new Rect(Size.x + 5.0f, Size.y - 4.0f, Size.width, HeaderHeight), HeaderFontSize, "SCENE OBJECT SETUP", "Fonts/Titil", ColorHelper._White_200, TextAnchor.MiddleLeft);
        if (SceneObject == null)
        {
            //offer the user to create one.
            Rect Button = new Rect(Size.x, Size.y + 60.0f, Size.width, EntryHeight);
            RenderHelpers.RenderBox(Button, ColorHelper._White_200);
            LabelHelper.TextAtCentered(Button, TextFontSize, "Create Scene Object", "");
            if (GUI.Button(Button, new GUIContent("", "The Scene object is the link between your game and the prefab atlas, in order to use the prefab atlas, an scene object must exist"), GUIStyle.none))
            {
                //create the scene object on the scene
                GameObject newObject = new GameObject("PrefabAtlasSceneObject");
                newObject.AddComponent<PrefabAtlasSceneObject>().TheAtlas = Data;
            }
        }
        else
        {
            SceneObject.TheAtlas = Data;
            //Display the options.
            UpkeepSceneObject(SceneObject, new Rect(Size.x, Size.y + HeaderHeight, Size.width, Size.height - HeaderHeight));
        }
    }
    private static GUIStyle MinimizeButtonStyle;
    #region Scene object functions
    private void UpkeepSceneObject(PrefabAtlasSceneObject Atlas, Rect Size)
    {
        
        //check integrity of the current atlas
        int count = Atlas.TheAtlas.Groups.Count;
        for (int i = 0; i < count; i++)
        {
            for (int a = i + 1; a < count; a++)
            {
                if (Atlas.TheAtlas.Groups[i].GroupName == Atlas.TheAtlas.Groups[a].GroupName)
                {
                    Rect MessageBox = new Rect(Size.x, Size.y + 30.0f, Size.width, 100.0f);
                    LabelHelper.TextAt(MessageBox, TextFontSize, "  There are 2 groups\n  with the same name\n  \"" + Atlas.TheAtlas.Groups[i].GroupName + "\"", "", ColorHelper.GetRGB(255.0f, 0.0f, 0.0f));
                    return;
                }
            }
        }
        for (int i = 0; i < count; i++)
        {
            List<GameObject> AllPrefabsInGroup = Atlas.TheAtlas.Groups[i].Prefabs;
            int prefabCount = AllPrefabsInGroup.Count;
            for (int a = 0; a < prefabCount; a++)
            {
                if (AllPrefabsInGroup[a] == null)
                {
                    AllPrefabsInGroup.RemoveAt(a);
                    a--;
                    prefabCount--;
                    VersionUp();
                }
            }
        }
        int currentGroupCount = Atlas.CurrentGroups.Count;
        UpdateScene(currentGroupCount, count, Atlas);
        GUILayout.BeginArea(Size);
        SceneObjectScrollViewPos = GUILayout.BeginScrollView(SceneObjectScrollViewPos, GUIStyle.none);
        for (int i = 0; i < count; i++)
        {
            if(i > Atlas.CurrentValues.Count)
            {
                Debug.Log("AAA");
                continue;
            }
            GUILayout.BeginVertical(new GUIContent(""), GUI.skin.box);
            GUILayout.BeginHorizontal();
            if (GUILayout.Button(new GUIContent("", "Activates or Deactivates the prefab container in this scene"), GUIStyle.none, GUILayout.MaxWidth(14.0f)))
            {
                Atlas.CurrentValues[i] = !Atlas.CurrentValues[i];
                VersionUp();
            }
            Rect F = GUILayoutUtility.GetLastRect();

            RenderHelpers.RenderBox(F, Atlas.CurrentValues[i] ? ColorHelper.GetRGB(0.0f, 255.0f, 0.0f) : ColorHelper.GetRGB(255.0f, 0.0f, 0.0f));
            GUILayout.Space(3.0f);
            GUILayout.Label(new GUIContent(Atlas.CurrentGroups[i]));

            if (MinimizeButtonStyle == null)
            {
                MinimizeButtonStyle = new GUIStyle();
                MinimizeButtonStyle.alignment = TextAnchor.UpperRight;
                MinimizeButtonStyle.normal.textColor = Color.white;
                MinimizeButtonStyle.fixedWidth = 50;
            }
            if (GUI.Button(GUILayoutUtility.GetLastRect(), new GUIContent(""), GUIStyle.none) || GUILayout.Button(new GUIContent(Atlas.MinimizedValues[i] ? "▼" : "▲"), MinimizeButtonStyle))
            {
                if (!Atlas.MinimizedValues[i])
                {
                    MinimizeAllEntries(Atlas);
                }
                else
                {
                    MinimizeAllEntries(Atlas);
                    Atlas.MinimizedValues[i] = false;
                    SceneObjectScrollViewPos.y = i * ((F.height * 2.0f) + 3.0f);
                }
                VersionUp();
            }
            GUILayout.EndHorizontal();

            if (!Atlas.MinimizedValues[i])
            {
                PrefabAtlasGroup CurrentGroup = GetPrefabGroup(Atlas.CurrentGroups[i], Atlas.TheAtlas);
                //draw every entry.
                if (CurrentGroup != null)
                {
                    //draw default values
                    GUILayout.BeginVertical(new GUIContent(""), GUI.skin.box);
                    GUILayout.BeginHorizontal();
                    GUILayout.Label(new GUIContent("DEFAULT VALUES"), GUI.skin.customStyles[2]);
                    if (GUILayout.Button(new GUIContent("Apply Defaults"), GUI.skin.box))
                    {
                        if (EditorUtility.DisplayDialog("Setting default values", "Are you sure you want to apply the default values to all the prefab definitions?", "Yes", "Cancel"))
                        {
                            for (int x = 0; x < Atlas.PreloadCounts[i].PreloadCounts.Count; x++)
                            {
                                Atlas.PreloadCounts[i].PreloadCounts[x] = Atlas.PreloadCounts[i].DefaultPreloadCount;
                                Atlas.PreloadCounts[i].MaxInstances[x] = Atlas.PreloadCounts[i].DefaultMaxInstances;
                                Atlas.PreloadCounts[i].Capacities[x] = Atlas.PreloadCounts[i].DefaultCapacity;
                            }
                        }
                        VersionUp();
                    }
                    GUILayout.EndHorizontal();
                    GUILayout.BeginHorizontal();
                    GUILayout.Label(new GUIContent("Preload", "The amount of pooled objects the scene object will start with"), GUI.skin.customStyles[0]);
                    Atlas.PreloadCounts[i].DefaultPreloadCount = EditorGUILayout.IntField(Atlas.PreloadCounts[i].DefaultPreloadCount);
                    GUILayout.EndHorizontal();
                    GUILayout.BeginHorizontal();
                    GUILayout.Label(new GUIContent("Capacity", "The amount of elements the pool can hold dormant at any moment, this is to limit how much memory we want to use"), GUI.skin.customStyles[0]);
                    Atlas.PreloadCounts[i].DefaultCapacity = EditorGUILayout.IntField(Atlas.PreloadCounts[i].DefaultCapacity);
                    GUILayout.EndHorizontal();
                    GUILayout.BeginHorizontal();
                    GUILayout.Label(new GUIContent("Max Instance", "The amount of max instances either inside the pool or outside the pool"), GUI.skin.customStyles[0]);
                    Atlas.PreloadCounts[i].DefaultMaxInstances = EditorGUILayout.IntField(Atlas.PreloadCounts[i].DefaultMaxInstances);
                    GUILayout.EndHorizontal();
                    GUILayout.EndVertical();
                    //fix for preload
                    if (Atlas.PreloadCounts[i].DefaultMaxInstances < -1)
                    {
                        Atlas.PreloadCounts[i].DefaultMaxInstances = -1;
                    }
                    //fix for capacity
                    if (Atlas.PreloadCounts[i].DefaultCapacity < -1)
                    {
                        Atlas.PreloadCounts[i].DefaultCapacity = -1;
                    }
                    //for for instancing
                    if (Atlas.PreloadCounts[i].DefaultMaxInstances < -1)
                    {
                        Atlas.PreloadCounts[i].DefaultMaxInstances = -1;
                    }
                    int GroupDefinitions = Atlas.PreloadCounts[i].PreloadCounts.Count;
                    //draw background for all the definitions
                    for (int a = 0; a < GroupDefinitions; a++)
                    {
                        GUILayout.BeginVertical(new GUIContent(""), GUI.skin.box);

                        GUILayout.Label(new GUIContent(Atlas.PreloadCounts[i].Prefabs[a].name));
                        GUILayout.BeginHorizontal();
                        GUILayout.Label(new GUIContent("Preload", "The amount of pooled objects the scene object will start with"), GUI.skin.customStyles[0]);
                        Atlas.PreloadCounts[i].PreloadCounts[a] = EditorGUILayout.IntField(Atlas.PreloadCounts[i].PreloadCounts[a]);
                        GUILayout.EndHorizontal();
                        GUILayout.BeginHorizontal();
                        GUILayout.Label(new GUIContent("Capacity", "The amount of elements the pool can hold dormant at any moment, this is to limit how much memory we want to use"), GUI.skin.customStyles[0]);
                        Atlas.PreloadCounts[i].Capacities[a] = EditorGUILayout.IntField(Atlas.PreloadCounts[i].Capacities[a]);
                        GUILayout.EndHorizontal();
                        GUILayout.BeginHorizontal();
                        GUILayout.Label(new GUIContent("Max Instance", "The amount of max instances either inside the pool or outside the pool"), GUI.skin.customStyles[0]);
                        Atlas.PreloadCounts[i].MaxInstances[a] = EditorGUILayout.IntField(Atlas.PreloadCounts[i].MaxInstances[a]);
                        GUILayout.EndHorizontal();
                        GUILayout.EndVertical();

                        //checkings
                        if (Atlas.PreloadCounts[i].PreloadCounts[a] < 0)
                        {
                            Atlas.PreloadCounts[i].PreloadCounts[a] = 0;
                        }
                        if (Atlas.PreloadCounts[i].MaxInstances[a] < -1)
                        {
                            Atlas.PreloadCounts[i].MaxInstances[a] = -1;
                        }
                        if (Atlas.PreloadCounts[i].Capacities[a] < -1)
                        {
                            Atlas.PreloadCounts[i].Capacities[a] = -1;
                        }
                        //Max instancing check
                        if (Atlas.PreloadCounts[i].Capacities[a] > Atlas.PreloadCounts[i].MaxInstances[a] && Atlas.PreloadCounts[i].MaxInstances[a] != -1)
                        {
                            Atlas.PreloadCounts[i].Capacities[a] = Atlas.PreloadCounts[i].MaxInstances[a];
                        }
                        //Capacity preloading check
                        if (Atlas.PreloadCounts[i].PreloadCounts[a] > Atlas.PreloadCounts[i].Capacities[a] && Atlas.PreloadCounts[i].Capacities[a] != -1)
                        {
                            Atlas.PreloadCounts[i].PreloadCounts[a] = Atlas.PreloadCounts[i].Capacities[a];
                        }
                        else if (Atlas.PreloadCounts[i].PreloadCounts[a] > Atlas.PreloadCounts[i].MaxInstances[a] && Atlas.PreloadCounts[i].MaxInstances[a] != -1)
                        {
                            Atlas.PreloadCounts[i].PreloadCounts[a] = Atlas.PreloadCounts[i].MaxInstances[a];
                        }
                    }
                }
            }
            GUILayout.EndVertical();
            GUILayout.Space(5.0f);
        }
        GUILayout.Space(100.0f);
        GUILayout.EndScrollView();
        GUILayout.EndArea();
    }
    static void MinimizeAllEntries(PrefabAtlasSceneObject Atlas)
    {
        int c = Atlas.MinimizedValues.Count;
        for (int i = 0; i < c; i++)
        {
            Atlas.MinimizedValues[i] = true;
        }
    }
    static PrefabAtlasGroup GetPrefabGroup(string Name, PrefabAtlas TheAtlas)
    {
        int count = TheAtlas.Groups.Count;
        for (int i = 0; i < count; i++)
        {
            if (TheAtlas.Groups[i].GroupName == Name)
            {
                return TheAtlas.Groups[i];
            }
        }
        return null;
    }
    private void UpdateScene(int currentGroupCount, int count, PrefabAtlasSceneObject Atlas)
    {
        if (Atlas._Version != Atlas.TheAtlas._Version)
        {
            Atlas._Version = Atlas.TheAtlas._Version;
        }
        else
        {
            return;
        }
        Dictionary<string, bool> CurrentMask = new Dictionary<string, bool>(currentGroupCount);
        Dictionary<string, bool> CurrentMinimized = new Dictionary<string, bool>(currentGroupCount);
        Dictionary<string, GroupCountsData> CurrentDefinitions = new Dictionary<string, GroupCountsData>(currentGroupCount);


        for (int i = 0; i < currentGroupCount; i++)
        {
            if (!CurrentMask.ContainsKey(Atlas.CurrentGroups[i]))
            {
                bool Result = Atlas.CurrentValues.Count > i ? Atlas.CurrentValues[i] : true;
                CurrentMask.Add(Atlas.CurrentGroups[i], Result);
            }
            if (!CurrentDefinitions.ContainsKey(Atlas.CurrentGroups[i]))
            {
                if (Atlas.PreloadCounts.Count > i)
                    CurrentDefinitions.Add(Atlas.CurrentGroups[i], Atlas.PreloadCounts[i]);
            }
            if (!CurrentMinimized.ContainsKey(Atlas.CurrentGroups[i]))
            {
                if (Atlas.MinimizedValues.Count > i)
                    CurrentMinimized.Add(Atlas.CurrentGroups[i], Atlas.MinimizedValues[i]);
            }

        }

        Atlas.CurrentGroups = new List<string>(count);
        Atlas.CurrentValues = new List<bool>(count);
        Atlas.PreloadCounts = new List<GroupCountsData>(count);
        Atlas.MinimizedValues = new List<bool>(count);

        for (int i = 0; i < count; i++)
        {
            Atlas.CurrentGroups.Add(Atlas.TheAtlas.Groups[i].GroupName);
            Atlas.CurrentValues.Add(true);
            Atlas.MinimizedValues.Add(true);
            Atlas.PreloadCounts.Add(new GroupCountsData(Atlas.TheAtlas.Groups[i].GroupName, Atlas.TheAtlas.Groups[i].Prefabs));
        }

        foreach (string val in CurrentMask.Keys)
        {
            int index = Atlas.CurrentGroups.IndexOf(val);
            if (index != -1)
            {
                Atlas.CurrentValues[index] = CurrentMask[val];
                Atlas.MinimizedValues[index] = CurrentMinimized.ContainsKey(val) ? CurrentMinimized[val] : false;
                GroupCountsData Tupe = CurrentDefinitions[val];
                Atlas.PreloadCounts[index].SetValues(Tupe.PreloadCounts, Tupe.Prefabs, Tupe.Capacities, Tupe.MaxInstances, Tupe.DefaultPreloadCount, Tupe.DefaultCapacity, Tupe.DefaultMaxInstances);
            }
        }
    }
    #endregion
    #endregion

    private void DropAreaGUI(PrefabAtlasGroup group, Rect DropArea)
    {
        if (Application.isPlaying)
        {
            return;
        }
        if (group == null) { return; }

        var evt = Event.current;
        var dropArea = DropArea;

        switch (evt.type)
        {
            case EventType.DragUpdated:
            case EventType.DragPerform:
                if (!dropArea.Contains(evt.mousePosition))
                    break;

                DragAndDrop.visualMode = DragAndDropVisualMode.Copy;

                if (evt.type == EventType.DragPerform)
                {
                    DragAndDrop.AcceptDrag();
                    VersionUp();
                    foreach (var draggedObject in DragAndDrop.objectReferences)
                    {
                        var AddingPrefab = draggedObject as GameObject;
                        if (!AddingPrefab || AddingPrefab.GetType() != typeof(GameObject))
                            continue;

                        //if (PrefabUtility.GetPrefabType(AddingPrefab) != PrefabType.Prefab)
                        if (PrefabUtility.GetPrefabAssetType(AddingPrefab) != PrefabAssetType.Regular)
                        {
                            //PrefabType
                            //if (PrefabUtility.GetPrefabType(AddingPrefab) == PrefabType.PrefabInstance)
                            if(PrefabUtility.GetPrefabInstanceStatus(AddingPrefab) == PrefabInstanceStatus.Connected)
                            {
                                AddingPrefab = PrefabUtility.GetCorrespondingObjectFromSource(AddingPrefab) as GameObject;
                            }
                            else
                            {
                                if (!Directory.Exists(Application.dataPath + "/PA_GeneratedPrefabs/" + group.GroupName))
                                {
                                    Directory.CreateDirectory(Application.dataPath + "/PA_GeneratedPrefabs/" + group.GroupName);
                                }
                                // AddingPrefab = PrefabUtility.CreatePrefab("Assets/PA_GeneratedPrefabs/" + group.GroupName + "/" + AddingPrefab.name + ".prefab", AddingPrefab, ReplacePrefabOptions.ConnectToPrefab);
                                AddingPrefab = PrefabUtility.SaveAsPrefabAsset(AddingPrefab, "Assets/PA_GeneratedPrefabs/" + group.GroupName + "/" + AddingPrefab.name + ".prefab"); //.CreatePrefab("Assets/PA_GeneratedPrefabs/" + group.GroupName + "/" + AddingPrefab.name + ".prefab", AddingPrefab, ReplacePrefabOptions.ConnectToPrefab);
                                Debug.Log("Creating prefab: " + AddingPrefab.name + " at " + Application.dataPath + "/PA_GeneratedPrefabs/" + group.GroupName);
                            }
                        }
                        if (AddingPrefab.GetComponent<PrefabAtlas>() != null)
                        {
                            Debug.LogError("You cant add a prefab atlas to a prefab atlas, thats just wrong...");
                            continue;
                        }

                        if (!group.Prefabs.Contains(AddingPrefab))
                        {
                            group.Prefabs.Add(AddingPrefab);
                        }
                    }

                    int prefabCount = group.Prefabs.Count;
                    if (group.Hashes == null)
                    {
                        group.Hashes = new List<int>(prefabCount);
                    }
                    group.Hashes.Clear();
                    for (int i = 0; i < prefabCount; i++)
                    {
                        group.Hashes.Add(group.Prefabs[i].name.GetHashCode());
                    }
                }
                evt.Use();
                break;

        }
    }
    void VersionUp()
    {
        if (SceneObject == null)
        {
            SceneObject = GameObject.FindObjectOfType<PrefabAtlasSceneObject>();
            if (SceneObject == null)
            {
                Debug.LogWarning("There is no Scene object to sync the changes, in order to preserve the changes please commit or create an scene object");
                //return;
                PrefabAtlas A = PrefabAtlasWindow.GetFirstPrefabOfType<PrefabAtlas>();
                if (A != null)
                {
                    if (A._Version + 1 == int.MaxValue)
                    {
                        A._Version = 1;
                    }
                    else
                    {
                        A._Version++;
                    }
                    EditorUtility.SetDirty(A.gameObject);
                }
            }
        }
        else
        {
            PrefabAtlas A = SceneObject.TheAtlas;
            if (A != null)
            {
                if (A._Version + 1 == int.MaxValue)
                {
                    A._Version = 1;
                }
                else
                {
                    A._Version++;
                }
                EditorUtility.SetDirty(A.gameObject);
            }
        }
    }
}