﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System.IO;
using System;
using System.Text;
using PrefabAtlasElements;
using System.Linq;

[CustomEditor(typeof(PrefabAtlas))]
public class PrefabAtlasEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        return;
    }

    #region helpers
   
    public static string FindTypeDirectory(Type theType)
    {
        string fileExtension = ".cs";
        DirectoryInfo TheDirectory = new DirectoryInfo(Application.dataPath);
        FileInfo[] TheFileInfo = TheDirectory.GetFiles("*" + fileExtension, SearchOption.AllDirectories);
        int i = 0; int FileInfoLength = TheFileInfo.Length;
        FileInfo TempFileInfo; string tempFilePath;
        for (; i < FileInfoLength; i++)
        {
            TempFileInfo = TheFileInfo[i];
            if (TempFileInfo == null)
                continue;

            tempFilePath = TempFileInfo.FullName;
            tempFilePath = tempFilePath.Replace(@"\", "/");

            char[] delimiters = { '.', '/' };
            string[] FindAction = tempFilePath.Split(delimiters);
            int countSplits = FindAction.Length;
            string ClassName = FindAction[countSplits - 2];
            Type cachedType = GetType(ClassName);
            if (cachedType != null)
            {
                if (cachedType == theType)
                {
                    tempFilePath = tempFilePath.Replace(ClassName + ".cs", "");
                    return tempFilePath;
                }
            }
        }

        return string.Empty;
    }

    private static Type GetType(string typeName)
    {
        var type = Type.GetType(typeName);
        if (type != null) return type;
        foreach (var a in AppDomain.CurrentDomain.GetAssemblies())
        {
            type = a.GetType(typeName);
            if (type != null)
                return type;
        }
        return null;
    }

    #endregion

    #region property drawer
    public static void SkimSearch(SerializedProperty SearchSkimString, Rect Whole)
    {
        GUI.Label(Whole, new GUIContent("SEARCH:"));
        float SearchSkimWidth = 0.66f;
        float PasteButton = 0.05f;
        float SearchSkimXOffset = 0.22f;
        Rect SkimSeacrhRect = new Rect(Whole.xMin + Whole.width * SearchSkimXOffset, Whole.yMin, Whole.width * SearchSkimWidth, Whole.height);
        SearchSkimString.stringValue = GUI.TextField(SkimSeacrhRect, SearchSkimString.stringValue);
        if (GUI.Button(new Rect(SkimSeacrhRect.x + SkimSeacrhRect.width, SkimSeacrhRect.y, Whole.width * PasteButton, Whole.height), "C"))
        {
            GUIUtility.systemCopyBuffer = SearchSkimString.stringValue;
        }
        if (GUI.Button(new Rect(SkimSeacrhRect.x + SkimSeacrhRect.width + Whole.width * PasteButton, SkimSeacrhRect.y, Whole.width * PasteButton, Whole.height), "P"))
        {
            SearchSkimString.stringValue = GUIUtility.systemCopyBuffer;
        }
        if (GUI.changed)
        {
            SearchSkimString.serializedObject.ApplyModifiedProperties();
        }
    }
    public static void NewEnumToInt(Rect EntireBox, int EnumValue, System.Type EnumType, SerializedProperty Property, string SearchSkimString)
    {
        string name = string.Empty;
        if (!Enum.IsDefined(EnumType, EnumValue))
        {
            var Values = Enum.GetValues(EnumType);
            int index = 0;
            int ValCount = Values.Length;
            for (int z = 0; z < ValCount; z++)
            {
                name = Values.GetValue(z).ToString();
                if (name == "None")
                {
                    EnumValue = (int)Values.GetValue(index);
                    break;
                }
                index++;
            }
        }
        else
        {
            name = Enum.Parse(EnumType, EnumValue.ToString(), true).ToString();
        }
        string SearchSkimp = SearchSkimString;

        Rect LabelBox = new Rect(EntireBox.x, EntireBox.y, EntireBox.width * 0.2f, EntireBox.height);
        GUI.Label(LabelBox, new GUIContent("PREFAB:"));
        Rect ButtonBox = new Rect(EntireBox.x + EntireBox.width * 0.22f, EntireBox.y,EntireBox.width * 0.66f, EntireBox.height);
       
        if (GUI.Button(ButtonBox, new GUIContent(name, name)))
        {
            var Values = Enum.GetValues(EnumType);
            string[] Parsed = new string[Values.Length];
            int ind = 0;
            foreach (Enum valEnu in Values)
            {
                Parsed[ind] = valEnu.ToString();
                ind++;
            }
            List<string> Sorted = Parsed.ToList();


            Sorted.Sort((x, y) => string.Compare(x, y));
            Sorted.Remove("None");
            if (SearchSkimp != "")
            {
                SearchSkimp = SearchSkimp.ToLower();
                int LenghtSkim = SearchSkimp.Length;
                for (int i = 0; i < Sorted.Count; i++)
                {
                    if (Sorted[i].Length < LenghtSkim)
                    {
                        Sorted.RemoveAt(i);
                        i--;
                    }
                    else
                    {
                        string SortedLowered = Sorted[i].ToLower();
                        for (int a = 0; a < LenghtSkim; a++)
                        {
                            if (SortedLowered[a] != SearchSkimp[a])
                            {
                                Sorted.RemoveAt(i);
                                i--;
                                break;
                            }
                        }
                    }
                }
            }
            Sorted.Insert(0, "None");
            GenericMenu M = new GenericMenu();
            for (int i = 0; i < Sorted.Count; i++)
            {
                string nam = Sorted[i];
                M.AddItem(new GUIContent(nam), false, (x) =>
                {
                    string decoded = (string)x;
                    //Debug.Log(decoded);
                    Property.intValue = (int)Enum.Parse(EnumType, decoded, true);
                    Property.serializedObject.ApplyModifiedProperties();
                }, nam);
            }
            M.ShowAsContext();
        }
    }
    public static void DrawEnumToInt(Rect aPosition, int aMask, System.Type aType, GUIContent aLabel, SerializedProperty prop, SerializedProperty SearchSkimString)
    {
        if (aType == null)
        {
            prop.intValue = 0;
            return;
        }
        if (aLabel.text == "")
        {
            EditorGUI.LabelField(new Rect(aPosition.xMin + aPosition.width * 0.36f, aPosition.yMin, aPosition.width * 0.2f, aPosition.height), new GUIContent("ID:"));
        }
        else
        {
            EditorGUI.LabelField(new Rect(aPosition.xMin + aPosition.width * 0.46f, aPosition.yMin, aPosition.width * 0.1f, aPosition.height), new GUIContent("ID:"));
        }

        string name = string.Empty;
        if (!Enum.IsDefined(aType, aMask))
        {
            var Values = Enum.GetValues(aType);
            int index = 0;
            int ValCount = Values.Length;
            for(int z = 0; z < ValCount; z++)
            {
                name = Values.GetValue(z).ToString();
                if(name == "None")
                {
                    aMask = (int)Values.GetValue(index);
                    break;
                }
                index++;
            }
        }
        else
        {
            name = Enum.Parse(aType, aMask.ToString(), true).ToString();
        }

        //search
        float SearchSkimWidth = 0.10f;
        float PasteButton = 0.05f;
        float SearchSkimXOffset = 0.8f;
        if(aLabel.text == string.Empty)
        {
            SearchSkimWidth = 0.2f;
            SearchSkimXOffset = 0.75f;
        }
        Rect SkimSeacrhRect = new Rect(aPosition.xMin + aPosition.width * SearchSkimXOffset, aPosition.yMin, aPosition.width * SearchSkimWidth, aPosition.height);
        SearchSkimString.stringValue = GUI.TextField(SkimSeacrhRect, SearchSkimString.stringValue);
        if (GUI.Button(new Rect(SkimSeacrhRect.x + SkimSeacrhRect.width, SkimSeacrhRect.y, aPosition.width*PasteButton, aPosition.height), "C"))
        {
            GUIUtility.systemCopyBuffer = SearchSkimString.stringValue;
        }
        if (GUI.Button(new Rect(SkimSeacrhRect.x + SkimSeacrhRect.width + aPosition.width*PasteButton, SkimSeacrhRect.y, aPosition.width * PasteButton, aPosition.height), "P"))
        {
            SearchSkimString.stringValue = GUIUtility.systemCopyBuffer;
        }
        if (GUI.changed)
        {
            SearchSkimString.serializedObject.ApplyModifiedProperties();
        }
        string SearchSkimp = SearchSkimString.stringValue;
        float ButtonSize = 0.57f;
        float ButtonWidth = 0.22f;
        if(aLabel.text == "")
        {
            ButtonSize = 0.42f;
            ButtonWidth = 0.33f;
        }
        if (GUI.Button(new Rect(aPosition.xMin + aPosition.width * ButtonSize, aPosition.yMin, aPosition.width * ButtonWidth, aPosition.height), new GUIContent(name, name)))
        {
            var Values = Enum.GetValues(aType);
            string[] Parsed = new string[Values.Length];
            int ind = 0;
            foreach(Enum valEnu in Values)
            {
                Parsed[ind] = valEnu.ToString();
                ind++;
            }
            List<string> Sorted = Parsed.ToList();//.Sort((x, y) =>  string.Compare(x, y));
         

            Sorted.Sort((x, y) => string.Compare(x, y));
            Sorted.Remove("None");
            if (SearchSkimp != "")
            {
                SearchSkimp = SearchSkimp.ToLower();
                int LenghtSkim = SearchSkimp.Length;
                for (int i = 0; i < Sorted.Count; i++)
                {
                    if(Sorted[i].Length < LenghtSkim)
                    {
                        Sorted.RemoveAt(i);
                        i--;
                    }
                    else
                    {
                        string SortedLowered = Sorted[i].ToLower();
                        for(int a = 0; a < LenghtSkim; a++)
                        {
                            if(SortedLowered[a] != SearchSkimp[a])
                            {
                                Sorted.RemoveAt(i);
                                i--;
                                break;
                            }
                        }
                    }
                }
            }
            Sorted.Insert(0, "None");
            GenericMenu M = new GenericMenu();
            for(int i  = 0; i < Sorted.Count; i++)
            {
                string nam = Sorted[i];
                M.AddItem(new GUIContent(nam), false, (x) =>
                {
                    string decoded = (string)x;
                    //Debug.Log(decoded);
                    prop.intValue = (int)Enum.Parse(aType, decoded, true);
                    prop.serializedObject.ApplyModifiedProperties();
                }, nam);
            }
            M.ShowAsContext();
        }
    }
    private static PrefabAtlas TheAtlas = null;
    public static PrefabAtlasGroup GetEnumPrefab(PrefabAtlasGroup Indexer, Rect position, string PropName)
    {
        if (TheAtlas == null)
        {
            //return null;
            PrefabAtlas Results = PrefabAtlasSceneObjectEditor.GetPrefabAtlas();
            if (Results == null)
            {
                GUI.Label(position,new GUIContent("PAT: There is no prefab atlas in this project."));
                return null;
            }
            else
            {
                PrefabAtlas Got = Results;
                TheAtlas = Got;
            }
        }

        List<GUIContent> Contents = new List<GUIContent>();
        List<PrefabAtlasGroup> Groups = TheAtlas.Groups;
        int count = Groups.Count;
        for (int i = 0; i < count; i++)
        {
            PrefabAtlasGroup current = Groups[i];
            Contents.Add(new GUIContent(current.GroupName));
        }

        int selected = -1;
        int Picked = -1;
        if (Indexer != null)
        {
            selected = Groups.IndexOf(Indexer);
        }
        if (PropName == "")
        {
            EditorGUI.LabelField(new Rect(position.xMin , position.yMin, position.width * 0.31f, position.height), new GUIContent("G:"));
            Picked = EditorGUI.Popup(new Rect(position.xMin + position.width * 0.06f, position.yMin, position.width * 0.3f, position.height), selected, Contents.ToArray());
        }
        else
        {
            EditorGUI.LabelField(new Rect(position.xMin, position.yMin, position.width * 0.19f, position.height), new GUIContent(PropName));
            EditorGUI.LabelField(new Rect(position.xMin + position.width * 0.2f, position.yMin, position.width * 0.11f, position.height), new GUIContent("G:"));
            Picked = EditorGUI.Popup(new Rect(position.xMin + position.width * 0.26f, position.yMin, position.width * 0.2f, position.height), selected, Contents.ToArray());
        }
       
        if (Picked != -1)
        {
            return Groups[Picked];
        }
        else
        {
            return Indexer;
        }
    }

    public static PrefabAtlasGroup GetEnumPrefabDirect(PrefabAtlasGroup Indexer, Rect position, string PropName)
    {
        if (TheAtlas == null)
        {
            //return null;
            PrefabAtlas Results = PrefabAtlasSceneObjectEditor.GetPrefabAtlas();
            if (Results == null)
            {
                GUI.Label(position, new GUIContent("PAT: There is no prefab atlas in this project."));
                return null;
            }
            else
            {
                PrefabAtlas Got = Results;
                TheAtlas = Got;
            }
        }

        List<GUIContent> Contents = new List<GUIContent>();
        List<PrefabAtlasGroup> Groups = TheAtlas.Groups;
        int count = Groups.Count;
        for (int i = 0; i < count; i++)
        {
            PrefabAtlasGroup current = Groups[i];
            Contents.Add(new GUIContent(current.GroupName));
        }

        int selected = -1;
        int Picked = -1;
        if (Indexer != null)
        {
            selected = Groups.IndexOf(Indexer);
        }

        EditorGUI.LabelField(new Rect(position.xMin, position.yMin, position.width * 0.2f, position.height), new GUIContent(PropName));
        Picked = EditorGUI.Popup(new Rect(position.xMin + position.width * 0.22f, position.yMin, position.width * 0.76f, position.height), selected, Contents.ToArray());
        

        if (Picked != -1)
        {
            return Groups[Picked];
        }
        else
        {
            return Indexer;
        }
    }

    #endregion
}


[CustomPropertyDrawer(typeof(PAT))]
public class EnumToIntPropertyDrawer : PropertyDrawer
{
    const float H = 80.0f;
    public override void OnGUI(Rect position, SerializedProperty prop, GUIContent label)
    {
        SerializedProperty Group = prop.FindPropertyRelative("enumType");

        Rect EntireRect = position;
        GUI.Box(EntireRect, new GUIContent(""));
        Rect LabelRect = new Rect(EntireRect.x, EntireRect.y, EntireRect.width, H * 0.22f);
        Rect SkimSearch = new Rect(EntireRect.x, EntireRect.y + H * 0.5f, EntireRect.width, H * 0.22f);
        Rect GroupRect = new Rect(EntireRect.x, EntireRect.y + H*0.25f, EntireRect.width, H*0.22f);
        Rect PrefabRect = new Rect(EntireRect.x, EntireRect.y + H * 0.75f, EntireRect.width, H * 0.22f);

        GUI.Label(LabelRect, new GUIContent(prop.displayName));
        PrefabAtlasGroup CachedGroup = (PrefabAtlasGroup)Group.objectReferenceValue;
      
        PrefabAtlasGroup Current = PrefabAtlasEditor.GetEnumPrefabDirect((PrefabAtlasGroup)Group.objectReferenceValue, GroupRect, "GROUP:");
        Group.objectReferenceValue = Current;

        if (Current != null)
        {
            string name = Current.GroupName + "Tag";
            SerializedProperty Val = prop.FindPropertyRelative("HashVal");
            SerializedProperty Skim = prop.FindPropertyRelative("SearchHelper");
            PrefabAtlasEditor.SkimSearch(Skim, SkimSearch);
            PrefabAtlasEditor.NewEnumToInt(PrefabRect, Val.intValue, GetType(name), Val, Skim.stringValue);
            //PrefabAtlasEditor.DrawEnumToInt(position, Val.intValue, GetType(name), label, Val, );
        }
        else
        {
            SerializedProperty Val = prop.FindPropertyRelative("HashVal");
            Val.intValue = 0;
        }

    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return H;
    }


    private static Type GetType(string typeName)
    {
        var type = Type.GetType(typeName);
        if (type != null) return type;
        foreach (var a in AppDomain.CurrentDomain.GetAssemblies())
        {
            type = a.GetType(typeName);
            if (type != null)
                return type;
        }
        return null;
    }
}







