﻿using UnityEngine;
using UnityEditor;
using PrefabAtlasElements;

[InitializeOnLoad]
public class PASOExec
{
    const int ReaderExecOrder = -10000;
    static PASOExec()
    {
        var temp = new GameObject();

        var reader = temp.AddComponent<PrefabAtlasSceneObject>();
        MonoScript readerScript = MonoScript.FromMonoBehaviour(reader);
        if (MonoImporter.GetExecutionOrder(readerScript) != ReaderExecOrder)
        {
            MonoImporter.SetExecutionOrder(readerScript, ReaderExecOrder);
        }

        Object.DestroyImmediate(temp);
    }

}

