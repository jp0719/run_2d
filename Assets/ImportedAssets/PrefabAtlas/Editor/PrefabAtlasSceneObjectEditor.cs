﻿using System;
using System.Collections.Generic;
using UnityEditor;
using System.IO;
using System.Linq;
using UnityEngine;
using PrefabAtlasElements;

[CustomEditor(typeof(PrefabAtlasSceneObject))]
public class PrefabAtlasSceneObjectEditor : Editor
{
    public override void OnInspectorGUI()
    {
        //DrawDefaultInspector();
        return;
    }

    /// <summary>
    /// Used to get assets of a certain type and file extension from entire project
    /// </summary>
    /// <param name="type">The type to retrieve. eg typeof(GameObject).</param>
    /// <param name="fileExtension">The file extention the type uses eg ".prefab".</param>
    /// <returns>An Object array of assets.</returns>
    public static System.Object[] GetAssetsOfType(System.Type type, string fileExtension)
    {
        List<System.Object> tempObjects = new List<System.Object>();
        DirectoryInfo directory = new DirectoryInfo(Application.dataPath);
        FileInfo[] goFileInfo = directory.GetFiles("*" + fileExtension, SearchOption.AllDirectories);

        int i = 0; int goFileInfoLength = goFileInfo.Length;
        FileInfo tempGoFileInfo; string tempFilePath;
        System.Object tempGO;
        for (; i < goFileInfoLength; i++)
        {
            tempGoFileInfo = goFileInfo[i];
            if (tempGoFileInfo == null)
                continue;

            tempFilePath = tempGoFileInfo.FullName;
            tempFilePath = tempFilePath.Replace(@"\", "/").Replace(Application.dataPath, "Assets");
            tempGO = AssetDatabase.LoadAssetAtPath(tempFilePath, typeof(System.Object)) as System.Object;
            if (tempGO == null)
            {
                continue;
            }
            else if (tempGO.GetType() != type)
            {
                continue;
            }

            tempObjects.Add(tempGO);
        }

        return tempObjects.ToArray();
    }

    /// <summary>
    /// Used to get assets of a certain type and file extension from entire project
    /// </summary>
    /// <param name="type">The type to retrieve. eg typeof(GameObject).</param>
    /// <param name="fileExtension">The file extention the type uses eg ".prefab".</param>
    /// <returns>An Object array of assets.</returns>
    public static T GetFirstPrefabOfType<T>() where T : MonoBehaviour
    {
        
        DirectoryInfo directory = new DirectoryInfo(Application.dataPath);
        
        FileInfo[] goFileInfo = directory.GetFiles("*" + ".prefab", SearchOption.AllDirectories);

        int i = 0; int goFileInfoLength = goFileInfo.Length;
        FileInfo tempGoFileInfo; string tempFilePath;
        GameObject tempGO;
        for (; i < goFileInfoLength; i++)
        {
            tempGoFileInfo = goFileInfo[i];
            if (tempGoFileInfo == null)
                continue;

            tempFilePath = tempGoFileInfo.FullName;
            tempFilePath = tempFilePath.Replace(@"\", "/").Replace(Application.dataPath, "Assets");
            tempGO = AssetDatabase.LoadAssetAtPath(tempFilePath, typeof(System.Object)) as GameObject;
            if (tempGO == null)
            {
                continue;
            }
            T Compo = tempGO.GetComponent<T>();
            if (Compo != null)
            {
                return Compo;
            }
            Compo = tempGO.GetComponentInChildren<T>();
            if (Compo != null)
            {
                return Compo;
            }

        }
        return null;

    }

    const string PATDir = "PrefabAtlas";
    public static PrefabAtlas GetPrefabAtlas()
    {
        DirectoryInfo directory = new DirectoryInfo(Application.dataPath);
        DirectoryInfo[] all = directory.GetDirectories("*", SearchOption.AllDirectories);
        int count = all.Length;
        DirectoryInfo FoundDir = null;
        for(int i = 0; i < count; i++)
        {
            if (all[i].Name == PATDir)
            {
                FoundDir = all[i];
                break;
            }
        }
        if(FoundDir== null)
        {
            return null;
        }
        FileInfo[] goFileInfo = FoundDir.GetFiles("PrefabAtlas.prefab", SearchOption.AllDirectories);
        if(goFileInfo.Length > 0)
        {
            FileInfo Curr = goFileInfo[0];
            string fn = Curr.FullName;
            fn = fn.Replace(@"\", "/").Replace(Application.dataPath, "Assets");
            GameObject tempGO = AssetDatabase.LoadAssetAtPath(fn, typeof(System.Object)) as GameObject;
            if (tempGO != null)
            {
                PrefabAtlas Got = tempGO.GetComponent<PrefabAtlas>();
                if (Got != null)
                {
                    return Got;
                }
            }
        }

        return null;
    }
}

